#!/usr/bin/env python
# coding: utf-8

# # pip

# Python possui uma gigantesca quantidade de módulos (pacotes) disponíveis para serem instalados. A forma padrão de se instalar módulos em Python é através de uma ferramenta chamada `pip` (*package installer for Python*). Com `pip`, podemos instalar qualquer pacote de uma base chamada *Python Package Index* ([PyPI](https://pypi.org)).
# 
# `pip` possui diversas funções: instalar, desinstalar, listar, entre outras. Você pode conhecer todas na [documentação oficial](https://pip.pypa.io/en/stable/cli/).

# ## Verificando a Instalação

# As novas versões de Python normalmente já possuem esta ferramenta instalada. Você pode verificar se possui com os seguintes comandos:

# - Windows:

# In[1]:


py -m pip --version


# - Linux/UNIX e MacOS:

# In[1]:


python --m pip --version


# ## Atualização

# Podemos atualizar a versão de seu `pip`.

# - Windows:

# In[ ]:


py -m pip install --upgrade pip


# - Linux/UNIX e MacOS:

# In[ ]:


python -m pip install --upgrade pip


# ## Instalando pip

# Se você não possuir `pip`, é possível instalar como abaixo.

# - Windows:

# In[ ]:


py -m ensurepip --upgrade


# - Linux/UNIX e MacOS:

# In[ ]:


python -m ensurepip --upgrade


# Se ainda assim não conseguir instalar, você pode ler mais no [site oficial](https://pip.pypa.io/en/stable/installation/).

# ## Instalando Módulos

# Vamos falar especificamente sobre módulos em outro capítulo, por agora basta que saibamos como instalar um módulo em Python. Exemplificaremos instalando o módulo [Matplotlib](https://matplotlib.org/stable/index.html) — um módulo de gráficos de Python.
# 
# Instalamos módulos com o comando `pip install nome_do_modulo` ou `pip install -U nome_do_modulo`. A opção `-U` significa "atualizar todos os pacotes para a versão mais nova". Você pode ler sobre o comando `install` no [site oficial](https://pip.pypa.io/en/stable/cli/pip_install/).

# In[ ]:


python -m pip install -U matplotlib


# ```{note}
# Se estiver utilizando Jupyter, o comando será este abaixo:
# 
#         import sys
# 
#         !{sys.executable} -m pip install matplotlib
# 
# Você pode ler sobre o motivo de ser diferente neste [post](https://jakevdp.github.io/blog/2017/12/05/installing-python-packages-from-jupyter/).
# ```
