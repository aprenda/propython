#!/usr/bin/env python
# coding: utf-8

# ## Conversão de Tipos

# Em Python há conversão explícita de dados: convertemos dados através de funções específicas para cada tipo.
# 
# No capítulo passado vimos uma conversão implícita dos dados para strings em [f-strings](#variaveis).

# ### Conversão para Inteiros

# Função de conversão para inteiros: `int()`

# In[1]:


# Convertendo float para int:
int(12.56)


# ### Conversão para float

# Função de conversão para float: `float()`

# In[2]:


# Convertendo int para float:
float(12)


# ### Conversão para Booleano

# Já estamos utilizando esta função desde o início do capítulo. Esta é: `bool()`

# In[3]:


# Convertendo int para bool:
bool(12)


# ### Conversão para strings

# Função de conversão para str: `str()`

# In[4]:


# Convertendo int para str:
str(12)


# ### Conversão entre Dados

# O tipo de dado tem de ser coercível para o outro tipo desejado. Podemos converter todos números para strings, por exemplo:

# In[5]:


# Convertendo números para string:
str(12)


# In[6]:


# Convertendo números para string:
str(-52.0006)


# In[7]:


# Convertendo strings para números:
int("12")


# In[8]:


# Convertendo strings para números:
float("12")


# Mas nem todas strings são coercíveis para números:

# In[9]:


# Tentando converter esta string para número:
int("Texto não é conversível para númerais.")

# Tentando converter esta string para número:
float("Texto não é conversível para númerais.")

