#!/usr/bin/env python
# coding: utf-8

# # Criando Tuplas

# Para criarmos tuplas, o procedimento é igual às listas, com exceção de que utilizamos parênteses ao invés de chaves. Esta forma é chamada de *sintaxe literal*, como já falamos. Abaixo exemplificamos:

# In[1]:


numeros = (3, 14, 15, 93)


# Podemos agora printar esta nova variável:

# In[2]:


print(numeros)


# Também podemos criar tuplas através da função construtora: `tuple()`. Ao usarmos esta forma, colocamos um iterável na função, então podemos colocar uma lista, como fizemos antes.

# ```{note}
# Tuplas também são iteráveis (na verdade, todas as estruturas de dados em Python são), como veremos no capítulo sobre iteração. Contudo, por motivos de simplicidade, quando necessitarmos de um objeto iterável, usaremos as `[]` das listas.
# ```

# In[3]:


vogais = tuple(["a", "e", "i", "o", "u"])


# In[4]:


print(vogais)


# Se criarmos tuplas vazias, seus valores booleanos serão `False`, com vemos abaixo:

# In[5]:


tupla_vazia = ()

print(bool(tupla_vazia))


# In[6]:


tupla_vazia = tuple()

print(bool(tupla_vazia))


# Tuplas são imutáveis, então, após sua criação, não há como modificar uma tupla. Tentemos modificar nossa tupla:
# 
# # Tuples are immutable, that is, they cannot be changed
# # You can't update or delete a value. It's unchanging way of storing ordered data
# 
# 
# # x=(1,2,3)
# 
# # print(3 in x) # Is 3 in 'x'? True
# 
# # x[0]="change me!" # TypeError: 'tuple' object does not support item assignment
# 
# 
# # alphabet=('a','b','c','d')
# 
# # print(type(alphabet))
# 
# 
# # Why use a tuple?
# 
# # - They are faster than lists
# # - It can make your code safer from bugs
# # - Use tuples as valid keys in a dictionary
# # - Some methods return tuples to the user (like .items())
# 
# 
# # Tuples are commonly used for UNCHANGING data:
# 
# # months=('January',
# # 'February',
# # 'March',
# # 'April',
# # 'May',
# # 'June',
# # 'July',
# # 'August',
# # 'September',
# # 'October',
# # 'November',
# # 'December')
# 
# # print(months)
# 
# # We can create a tuple using '()' or 'tuple()'
# 
# # t1=() # Empty tuple
# # t1=tuple() # Empty tuple
# 
# 
# # Tuples with one item is constructed by following a value with a comma (it is
# # not sufficient to enclose a single value in parentheses):
# 
# # t1=(1,)
# 
# 
# 
# # We access tuples just like we access lists:
# 
# # print(months[0])
# 
# # print(months[3])
# 
# # print(months[10])
# 
# # print(months[11])
# 
# 
# 
# # Like said before, tuples can be used as keys in dictionaries:
# 
# # locations={
# #     (35.6895,39.6917): "Tokyo Office",
# #     (40.7128,74.0060): "New York Office",
# #     (37.7749,122.4194): "San Francisco Office"
# # }
# 
# # print(locations)
# 
# # print(locations[(35.6895,39.6917)])
# 
# 
# 
# 
# # Like also said before, some methods return tuples:
# 
# # cat=dict(cat='biscuit',age=0.5,favorite_toy='my chapstick')
# 
# # print(cat.items())
# 
# # 'cat.items()' will return a list, but each item within there is a tuple. The for this
# # to be a tuple instead of a lists is that it's not going to change. When you call '.items()',
# # it's returning the current state. So, there's no reason for make a list because we not
# # going to update these values, we might update the actual dictionary.
# 
# ## Tuple Methods ##
# 
# # 1) Count:
# 
# # Return the number of times of a value appears in a tuple:
# 
# # x=(1,2,3,3,3)
# 
# # print(x.count(1)) # '1' appears one time in the tuple
# 
# # print(x.count(3)) # '3' appears three times in the tuple
# 
# 
# 
# # 2) Index:
# 
# # Returns the index at which a value is found in a tuple:
# 
# # x.index(1) # What is the index of '1' inside the tuple? Returns '0', that is, it's the first element
# 
# # x.index(5) # ValueError: tuple.index(x): x not in the tuple ('5' it's not in the tuple)
# 
# # x.index(3) # Returns '2', because only the first matching index is returned
# 
# 
# 
# 
# ## Nested Tuples ##
# 
# # We can have nested tuples:
# 
# # nums=(1,2,3,(4,5,6),7,8)
# 
# # print(len(nums)) # 6
# 
# # print(nums[0]) # 1
# 
# # print(nums[3]) # (4,5,6)
# 
# # print(nums[3][0]) # 4
# 
# # print(nums[3][2]) # 6
# 
# 
# 
# 
# # Slicing Tuples ##
# 
# # We can slice tuples list list too:
# 
# # print(nums[:]) # Begginning to the end
# 
# # print(nums[0:4]) # Begginning to the third element, because it's exclusive
# 
# # print(nums[0:4:2]) # Begginning to the third element, step = 2
