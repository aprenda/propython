#!/usr/bin/env python
# coding: utf-8

# # Métodos de Listas

# Há diversos métodos próprios de listas:
# 
# dir(list)

# ## Método `append()`

# Com 'append()', colocamos um novo elemento no fim de uma lista
# 
# l = [1, 2]
# 
# print(l)
# 
# l.append("risos")
# 
# print(l)
# 
# 
# Exemplo com 'for' (e um exemplo mais grosseiro que poderia ser feito
# com compreensão de listas):
# 
# l = []
# 
# print(l)
# 
# for numero in range(10):
#     l.append(numero)
# 
# print(l)

# ## Método `clear()`

# Com 'clear()', excluímos todos os elementos da lista:
# 
# l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# 
# print(l)
# 
# l.clear()
# 
# print(l)

# ## Método `pop()`

# Com 'pop()', removemos um elemento em um dado index e o retornamos.
# Útil para criar novas variáveis. Por default, o último elemento é 
# removido.
# 
# l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# 
# print(l)
# 
# l.pop() Removendo o último elemento
# 
# print(l)
# 
# l.pop(6) Removendo o elemento no index 6
# 
# print(l)

# ## Método `remove()`

# Com 'remove()', removemos (SEM RETORNAR!) a primeira ocorrência de
# um elemento
# 
# l = [1, 1, 1, 1, 2, 3, 4, 4, 4, 5, 6]
# 
# print(l)
# 
# l.remove(1)
# 
# print(l)
# 
# l.remove(4)
# 
# print(l)
# 
# l.remove(5)
# 
# print(l)
