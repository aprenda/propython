#!/usr/bin/env python
# coding: utf-8

# (tupla_aninhamento)=
# # Aninhamento de Tuplas

# Podemos criar tuplas aninhadas do mesmo modo que fizemos com listas. Vamos criar uma:

# In[1]:


tup_anin = (
    ("Setembro", "Outubro", "Novembro"),
    (0, 1, 2, 3, 4, 5, 6, 7, 8, 9),
    (True, False)
)


# Criamos uma tupla que possui como elementos três outras tuplas: uma com strings; outra com cúmeros; e a última com booleanos.

# In[2]:


print(tup_anin)


# Assim como indexação e slicing, estruturas aninhadas são acessadas da mesma forma como [já vimos](lista-anin). Vamos acessar nossa tupla aninhada:

# In[3]:


# Acessando a tupla de meses:
print(tup_anin[0])


# In[4]:


# Acessando a tupla numérica:
print(tup_anin[1])


# In[5]:


# Acessando a tupla booleana:
print(tup_anin[2])


# E os elementos destes objetos aninhadas são acessados com uma indexação adicional:

# In[6]:


# Acessando os elementos da tupla de meses:
print(tup_anin[0][0])
print(tup_anin[0][1])
print(tup_anin[0][2])


# In[7]:


# Acessando os lementos da tupla numérica:
print(tup_anin[1][0])
print(tup_anin[1][1])
print(tup_anin[1][2])
print(tup_anin[1][3])
print(tup_anin[1][4])
print(tup_anin[1][5])
print(tup_anin[1][6])
print(tup_anin[1][7])
print(tup_anin[1][8])
print(tup_anin[1][9])


# In[8]:


# Acessando os elementos da tupla booleana:
print(tup_anin[2][0])
print(tup_anin[2][1])


# E, obviamente, como são tuplas, não podemos modificar nenhum elemento:

# In[9]:


tup_anin[0] = ("Janeiro", "Fevereiro")


# Nem mesmo um elemento aninhado, pois também são tuplas:

# In[13]:


tup_anin[0][0] = "Janeiro"

