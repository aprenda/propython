#!/usr/bin/env python
# coding: utf-8

# # Designação

# A sintaxe da designação de uma variável em Python é:

# In[1]:


nome_var = valor


# Onde:
# 
# - `nome_var`: nome da variável
# - `=`: operador para designação (*assignment operator*)
# - `valor`: valor ligado à variável
# 
# Exemplo:

# In[2]:


x = 100


# Designamos o valor `100` para a variável `x`.
# 
# Em Python, variáveis são apenas nomes — nomes que se referem a valores. Imagine, por exemplo, que a string `"Python"` está em um lugar específico da memória do computador. O que fazemos ao criar uma variável é *ligar*/*conectar* um nome qualquer que desejarmos a este valor, no caso, `"Python"`. Toda vez que nos referirmos a este nome, estamos nos referindo a um valor específico na memória do computador. Assim, no exemplo acima, o nome `x` (a variável `x`) refere-se ao valor `100` na memória do computador.
# 
# Ao nomearmos uma variável, devemos ter uma melhor semântica nas variáveis, ou seja, as variáveis devem ter nomes compreensíveis e claros em seu propósito:

# In[3]:


# Uma variável guardando o número de canetas
numero_canetas = 12


# E podemos ter vários tipos de valores nas variáveis, um tema que veremos em detalhes no próximo [capítulo](#tipos_dados).
# 
# > x = "string"    
# > y = False    
# > z = 12.56
# 
# Também podemos designar várias variáveis de uma única vez:

# In[4]:


x = y = z = 4000


# (mult-desig)=
# ## Múltiplas Designações

# Podemos também criar várias variáveis a vários valores diferentes utilizando vírgulas para separá-los:

# In[5]:


tres, variaveis, diferentes = 5, 10, 15

print(tres, variaveis, diferentes)


# O que aconteceu:
# 
# Python ligou os três nomes diferentes (`tres`, `variaveis` e `diferentes`) a três valores diferentes (`5`, `10` e `15`), utilizando a respectiva ordem definida pelo usuário. Assim:
# 
# > tres = 5    
# > variaveis = 10    
# > diferentes = 15
# 
# Na realidade, o que está acontencendo é um *tuple unpacking*, mas falaremos disto em [outro capítulo](tup_virg).
