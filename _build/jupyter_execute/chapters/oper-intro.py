#!/usr/bin/env python
# coding: utf-8

# # Operadores

# Operadores são símbolos que uma linguagem de programação utiliza para realizar ações específicas. Tais ações dependem da linguagem: diferentes linguagens utilizam operadores diferentes para algumas ações específicas da linguagem e podem até mesmo utilizar operadores de senso comum para outros fins. Veremos a partir deste capítulo que Python também utiliza alguns operadores para vários fins diferentes.
# 
# Antes de começarmos, vamos definir alguns conceitos que serão necessários:
# 
# - **Operando**: operandos são os objetos sendo manipulados pelo operador. Por exemplo, numa adição simples:
# 
#         1 + 2
# 
# O operador é o símbolo `+` e os operandos são `1` e `2`, sendo estes especificados como **operando esquerdo** e **operando direito**, respectivamente.  
# 
# - **Operador unário**: operador que utiliza apenas um operando, geralmente o operando direito. Ex:
# 
#         -8
#         
# Onde `-` é o operador e `8` é o operando direito (à direita do operador).
# 
# - **Operador binário**: operador que utiliza dois operandos. Ex:
# 
#         1 + 2
#         
# Onde `+` é o operador e `1` e `2` são os operandos esquerdo (à esquerda do operador) e direito (à direita do operador), respectivamente.
# 
# ```{note}
# A não ser quando explicitamente especificado, todos os operadores debatidos neste capítulo são binários.
# ```
