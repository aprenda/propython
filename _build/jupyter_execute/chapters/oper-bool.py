#!/usr/bin/env python
# coding: utf-8

# # Operadores Booleanos

# Este operadores são conectivos entre diferentes operadores e retornam os valores `True` ou `False`. Estes operadores funcionam de modo similar a uma [tabela-verdade](https://en.wikipedia.org/wiki/Truth_table). Entretanto, os operadores de conjunção (`and`) e disjunção (`or`) não retornam o valor booleano da expressão, mas sim, o valor do operando em questão! Vejamos o que isto significa.

# ## Operador `and`

# O operador `and` (em lógica: conjunção) avalia primeiramente o operando esquerdo: se este for falso, seu valor é retornado. Se for verdadeiro, é necessário avaliar o operando direito também, sendo retornado seu valor.
# 
# A razão de avaliarmos primeiro um dos operandos é: logicamente, na tabela-verdade, a conjunção é verdadeira somente se ambos lados da expressão forem verdadeiros. Se um deles for falso, o resultado é falso de qualquer maneira. Assim, se um dos operandos for falso, não é necessário avaliar o outro operando, sendo a operação mais rápida.
# 
# ```{admonition} Importante!
# O **valor** do último operando avaliado é retornado! Não necessariamente o resultado será sempre `True` ou `False`.
# ```
# 
# Vamos ver alguns exemplos.

# 1\. Ambos operandos são booleanos e o resultado desta expressão é `False`, pois o operando da esquerda é `False`.

# In[1]:


print(False and False)


# 2\. Ambos operandos são booleanos e o resultado desta expressão é `False`, pois o operando da direita é `False`.

# In[2]:


print(True and False)


# 3\. Ambos operandos são booleanos e o resultado desta expressão é `True`, pois o operando da direita é `True`.

# In[3]:


print(True and True)


# 4\. Ambos operandos são numéricos e o resultado desta expressão é `0`, pois o operando da esquerda é `False`.

# In[4]:


print(0 and 0)


# ```{admonition} Lembrete
# Os números `0` e `1` são avaliados com `False` e `True`, respectivamente.
# ```

# 5\. Ambos operandos são numéricos e o resultado desta expressão é `0`, pois o operando da esquerda é `False`.

# In[5]:


print(0 and 1)


# 6\. Ambos operandos são numéricos e o resultado desta expressão é `1`, pois o operando da direita é `True`.

# In[6]:


print(1 and 1)


# 7\. Ambos operandos são strings e o resultado desta expressão é `""` (uma string vazia), pois o operando da esquerda é `False`.

# ```{admonition} Lembrete
# Todos objetos vazios são avaliados como `False`.
# ```

# In[7]:


print("" and "texto") # "", pois o operando esquerdo é falso (string 
                      # vazia)


# ```{admonition} Resumo
# 1. Avalie o operando esquerdo.
#     1. Se `False`, retorne o valor do operando esquerdo.
#     1. Se `True`, avalie o operando direito.
# 1. Retorne o valor do operando direito, de qualquer forma.
# ```

# ## Operador `or`

# O operador `or` (em lógica: disjunção) avalia primeiramente o operando esquerdo: se este for verdadeiro, seu valor é retornado. Se for falso, é necessário avaliar o operando direito também, sendo retornado seu valor.
# 
# A razão de avaliarmos primeiro um dos operandos, aqui, na disjunção, é similar: logicamente, na tabela-verdade, a disjunção é verdadeira quando qualquer um dos lados da expressão for verdadeiro. Se um deles for verdadeiro, o resultado é verdadeiro de qualquer maneira. Assim, se um dos operandos for verdadeiro, não é necessário avaliar o outro operando, sendo a operação mais rápida.
# 
# ```{admonition} Importante!
# Novamente: o **valor** do último operando avaliado é retornado! Não necessariamente o resultado será sempre `True` ou `False`.
# ```
# 
# Vejamos alguns exemplos.

# 1\. Ambos operandos são booleanos e o resultado desta expressão é `False`, pois o operando da direita é `False`.

# In[8]:


print(False or False)


# 2\. Ambos operandos são booleanos e o resultado desta expressão é `True`, pois o operando da esquerda é `True`.

# In[9]:


print(True or False)


# 3\. Ambos operandos são booleanos e o resultado desta expressão é `True`, pois o operando da esquerda é `True`.

# In[10]:


print(True or True)


# 4\. Ambos operandos são numéricos e o resultado desta expressão é `0`, pois o operando da direita é `False`.

# In[11]:


print(0 or 0)


# 5\. Ambos operandos são numéricos e o resultado desta expressão é `1`, pois o operando da direita é `True`.

# In[12]:


print(0 or 1)


# 6\. Ambos operandos são numéricos e o resultado desta expressão é `1`, pois o operando da esquerda é `True`.

# In[13]:


print(1 or 1)


# 7\. Ambos operandos são strings e o resultado desta expressão é `"texto"`, pois o operando da direita é `True`.

# In[14]:


print("" or "texto")


# ```{admonition} Resumo
# 1. Avalie o operando esquerdo.
#     1. Se `True`, retorne o valor do operando esquerdo.
#     1. Se `False`, avalie o operando direito.
# 1. Retorne o valor do operando direito, de qualquer forma.
# ```

# ## Operador `not`

# The operator not yields True if its argument is false, False otherwise.

# O operador `not` (em lógica: negação), assim como na tabela-verdade, negará o valor booleano do operando. Se o valor booleano do operando for `True`, sua negação será `False` e vice-versa.

# In[15]:


print(not True)


# In[16]:


print(not False)


# In[17]:


print(not 0)


# In[18]:


print(not 1)


# In[19]:


print(not 28)


# In[20]:


print(not "True")


# In[21]:


print(not "")

