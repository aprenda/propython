#!/usr/bin/env python
# coding: utf-8

# (string_comp)=
# # Strings

# ## Indexação e Slicing

# A primeira nova característica que veremos é: **strings podem ser indexadas**. Agora que sabemos o que é indexação e slicing, facilmente podemos aplicar tais conceitos em outros tipos de objetos.
# 
# ```{note}
# Indexação e slicing podem ser aplicados a diferentes tipos de objetos em Python. A sintaxe e modo de aplicação em cada um destes objetos não muda — é exatamente o mesmo modo como vimos em listas.
# ```
# 
# Para acessar um elemento de uma string, basta fazer uma indexação simples. Exemplo:

# In[1]:


print("Python"[0])


# Acima, pegamos o primeiro elemento da string `"Python"`, que é exatamente a letra `"P"`. Se a string estiver ligada à uma variável, fazemos exatamente da mesma forma:

# In[2]:


texto = "Python"

print(texto[0])


# Já sabemos, desde as listas, que podemos ustilziar variáveis com indexação.
# 
# Se strings são indexáveis, então podemos fazer um slicing destes objetos. Assim como indexação, o slicing é exatamente o mesmo como em listas. Exemplo:

# In[3]:


print(texto[0:3])


# Acima, pegamos apenas os elementos nas posições `0` até `2` da string `"Python"`.
# 
# Quando fazemos slicing de uma string, estamos pegando um subconjunto da string; um pedaço desta. Tecnicamente, estamos acessando uma **substring**. Abaixo, pegamos a substring `"thon"`:

# In[4]:


print(texto[2:])


# E somente um exemplo mostrando o uso do parâmentro `intervalo` de slicing:

# In[5]:


print(texto[::2])


# Acima, pegamos os elementos nas posições pares da string.

# ## Mutabilidade

# Uma importante características de string é que este é um tipo de objeto **imutável**, como já havíamos mencionado no capítulo de [strings](dados-str). Aqui, podemos verificar tal afirmação. Continuando com nossa string `"Python"`, tentemos substituir o elemento `y` por `i`:

# In[6]:


# "y" está no index 2:
"Python"[2]


# In[7]:


# Tentando modificar:
"Python"[2] = "i"


# Um erro ocorreu! Vejamos o que Python nos diz sobre:
# 
# > TypeError: 'str' object does not support item assignment
# 
# O que significa: este tipo de objeto não aceita uma designação de elementos, ou seja, não é possível modificar um objeto do tipo string (`'str'`).
# 
# Quando necessitamos de uma nova string, temos de criar uma nova string e substituir a antiga, se necessário.
