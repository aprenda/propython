#!/usr/bin/env python
# coding: utf-8

# # Métodos de Tuplas

# Abordaremos agora métodos de tuplas. Será uma seção bem curta, pois há apenas dois métodos para este tipo de estrutura. Vejamos todas as funções e atributos de uma tupla:

# In[1]:


dir(tuple)


# Retirando os métodos especiais, um objeto `tuple` possui apenas dois métodos: `count()` e `index()`. Vamos retornar ao nosso conjunto de capitais, mas agora em uma tupla:

# In[2]:


capitais = (
    "Rio Branco", "Boa Vista", "Macapá", "Manaus", "Boa Vista",
    "Belém", "Boa Vista", "Porto Velho", "Boa Vista"
)


# In[3]:


print(capitais)


# Nossa tupla `capitais` contém várias capitais da região norte do Brasil, com elementos repetidos.
# 
# Agora vejamos os métodos de tuplas.

# ## Método `count()`

# Com o método `count()`, podemos contar quantas vezes algum elemento se repete na tupla, da mesma forma como na lista. Sua sintaxe é:

# In[4]:


tuple.count(elemento)


# Deste modo, fornecemos o elemento que queremos contar e nos é retornado a quantidade de ocorrências na tupla. Vamos contar quantas vezes Boa Vista ocorre em nossa tupla:

# In[5]:


print(capitais.count("Boa Vista"))


# Sabemos que, de fato, há quatro strings `"Boa Vista"` na lista `capitais`. E quanto a Manaus?

# In[6]:


print(capitais.count("Manaus"))


# Há apenas uma string `"Manaus"` na tupla. E se colocássemos um elemento inexistente?

# In[7]:


print(capitais.count("Cuiabá"))


# Não nos é retornado erro algum, apenas que não há nenhum elemento `"Cuiabá"` na tupla.

# ## Método `index()`

# Com o método `index()`, é retornado o index da **primeira ocorrência** de um elemento na tupla. Sua sintaxe é:

# In[8]:


tuple.index(elemento)


# Vamos descobrir a posição de alguns elementos:

# In[9]:


print(capitais.index("Belém"))


# A string `"Belém"` está na posição `5`.
# 
# Nossa tupllista tem quatro elementos iguais: `Boa Vista`. Vamos descobrir em qual posição ele encontra-se.

# In[10]:


print(capitais.index("Boa Vista"))


# Há quatro strings `"Boa Vista"` na tupla, mas apenas a posição `1` foi retornada. Isto ocorre porque o método `index()` retorna apenas o index do **primeiro elemento encontrado**. Poderiam haver vinte strings iguais, mas `index()` retornará apenas a posição da primeira encontrada.
# 
# E se procurássemos por um elemento que não está na lista?

# In[11]:


print(capitais.index("Goiânia"))


# Um erro é retornado, pois a string `"Goiânia"` não está na tupla `capitais`.
