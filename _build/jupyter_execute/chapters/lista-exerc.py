#!/usr/bin/env python
# coding: utf-8

# # Exercícios

# ```{admonition} **Tente Sozinho!**
# Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
# ```
# 
# ```{admonition} **Pesquise!**
# Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
# ```

# ## Questão 1

# Defina a estrutura de dados lista e cite suas principais características.

# ```{admonition} Resposta
# :class: dropdown
# 
# Listas são estruturas que agrupam um conjunto de elementos em um único objeto e caracterizam-se por serem ordenadas, mutáveis e iteráveis.
# ```

# ## Questão 2

# As listas abaixo não estão construídas corretamente. Arrume o que for necessário.

# In[1]:


lst1 = [
    "amarelo", "vermelho", "azul",
    "laranja", "roxo", "verde"
)


# In[2]:


lst2 = [
    "amarelo", "vermelho", "azul"
    "laranja", "roxo", "verde"
]


# In[3]:


lst3 = [
    104.3, 99.5, 94.9
    3.14, 2.61, 6.02
]


# In[4]:


lst4 = list(1,2,3,4)


# ```{admonition} Resposta
# :class: dropdown
# 
# - `lst1`: erro no operador de construção. Havia um `)` no lugar de `]`
# 
#         lst1 = [
#             "amarelo", "vermelho", "azul",
#             "laranja", "roxo", "verde"
#         ]
# - `lst2`: os elementos de uma lista são separados por vírgulas. O erro estava na ausência de uma vírgula após a string "azul". Deste modo, teremos duas strings separadas e não uma única.
# 
#         lst2 = [
#             "amarelo", "vermelho", "azul",
#             "laranja", "roxo", "verde"
#         ]
# - `lst3`: assim como o anterior, o erro estava na ausência de uma vírgula após o número `94.9`. Contudo, neste caso, Python nos retorna um erro pois não concatena os números como fez com as strings.
# 
#         lst3 = [
#             104.3, 99.5, 94.9,
#             3.14, 2.61, 6.02
#         ]
# - `lst4`: a função construtora `list()` aceita apenas iteráveis como argumento. Assim, temos de colocar todos os elementos dentro de um único objeto iterável para então colocar este objeto na função.
# 
#         lst4 = list([1,2,3,4])
# ```

# ## Questão 3

# Na lista abaixo, faça o seguinte:
# 
# - Troque `Joao` por `João`;
# - Troque `Olivia` por `Olívia`;
# - Troque `lucas` por `Lucas`.

# In[5]:


nomes = ["Joao", "Olivia", "Maria", "lucas"]


# ```{admonition} Resposta
# :class: dropdown
# - Trocando `Joao`:
# 
#         nomes[0] = "João"
# 
# - Trocando `Olivia`:
# 
#         nomes[1] = "Olívia"
#         
# - Trocando `lucas` (duas opções):
# 
#         nomes[3] = "Lucas"
#         nomes[-1] = "Lucas"
# ```

# ## Questão 4

# A lista abaixo representa uma lista de usuários qualquer. A partir desta lista, e utilizando slicing, crie uma nova lista `autorizados` sem o usuário `john117`.

# In[6]:


usuarios = [
    "naked_snake", "solid_snake", "liquid_snake",
     "solidus_snake", "venom_snake", "john117"
]


# ```{admonition} Resposta
# :class: dropdown
# Qualquer das formas abaixo está correta:
# 
#         autorizados = usuarios[0:-1]
#         autorizados = usuarios[:-1]
#         autorizados = usuarios[0:5]
#         autorizados = usuarios[:5]
# ```

# ## Questão 5

# Ainda com nossa lista `usuarios` (abaixo):
# 
# 1. Mude o usuário `naked_snake` para `bigboss`;
# 1. Remova o usuário `john117`;
# 1. Crie uma nova lista `pai` com os usuários `bigboss` e `venom_snake`;
# 1. Crie uma nova lista `filhos` com os usuários `solid_snake`, `liquid_snake` e `solidus_snake`.
# 
# Para os passos 3 e 4, utilize slicing.

# In[7]:


usuarios = [
    "naked_snake", "solid_snake", "liquid_snake",
     "solidus_snake", "venom_snake", "john117"
]


# ```{admonition} Resposta
# :class: dropdown
# 1. Mude o usuário `naked_snake` para `bigboss`:
#         
#         usuarios[0] = "bigboss"
#         print(usuarios)
#         
# 1. Remova o usuário `john117`:
#         
#         usuarios.remove("john117")
#         print(usuarios)
# 
# 1. Crie uma nova lista `pai` com os usuários `bigboss` e `venom_snake`:
# 
#         pai = usuarios[::4]
#         print(pai)
# 
# 1. Crie uma nova lista `filhos` com os usuários `solid_snake`, `liquid_snake` e `solidus_snake`:
# 
#         filhos = usuarios[1:4]
#         print(filhos)
# ```

# ## Questão 6

# Vimos como remover elementos de uma lista através dos métodos `remove()` e `pop()`. Contudo, há outro modo de removermos um elemento de uma lista: usando o statement `del`. Pesquise sobre este comando.
# 
# Abaixo, temos a lista `empresas`. Usando `del`:
# 
# 1. Retire a string `Facebook`;
# 1. Retire as strings `Whatsapp` e `Instagram`;
# 1. Diferencie `remove()`, `pop()` e `del`.

# In[8]:


empresas = [
    "Facebook", "Google",  "Twitter" , "Whatsapp", "Instagram"
]


# ```{admonition} Resposta
# :class: dropdown
# 
# 1. Retire a string `Facebook`:
# 
#         del empresas[0]
#         print(empresas)
# 
# 1. Retire as strings `Whatsapp` e `Instagram`:
# 
#         del empresas[2:4]
#         print(empresas)
# 
# 1. Diferencie `remove()`, `pop()` e `del`:
# 
# - `remove()`: remove a primeira ocorrência de um elemento; não usa index.
# - `pop()`: remove um elemento através de seu index e o retorna.
# - `del`: remove um elemento através de seu index sem o retornar. Na verdade, `del` é um comando de deleção; ele apagará qualquer objeto. Aplicado às indexações em listas, ele apagará elementos da lista que estejam nestas posições.
# ```

# ## Questão 7

# Temos abaixo uma nova lista aninhada `animais`.

# In[9]:


mamiferos = ["gato", "macaco", "ornitorrinco"]

aves = ["ema", "águia", "seriema"]

artropodes = ["chilopoda", "libélula", "lagosta"]

animais = [mamiferos, aves, artropodes]


# A partir desta lista, faça e responda:
# 
# 1. Com a função construtora `list()`, crie uma lista `animais_copia` de `animais`;
# 1. `animais_copia` possui os mesmos elementos que `animais`? Verifique;
# 1. `animais_copia` e `animais` são os mesmos objetos (na memória)? Verifique;
# 1. Os elementos de `animais_copia` e `animais` são os mesmos (na memória)? Verifique;
# 1. Em `animais_copia`, mude a string `ornitorrinco` para `anta` e printe `animais_copia`;
# 1. Em `animais_copia`, mude a string `águia` para `papagaio` e printe `animais_copia`;
# 1. Em `animais_copia`, mude a string `chilopoda` para `lacraia` e printe `animais_copia`;
# 1. Verifique as listas `animais_copia` e `animais`. Os elementos de `animais` mudaram após a modificação de `animais_copia`? Que tipo de cópia foi feita no passo 1 com a função `list()`?

# ```{admonition} Resposta
# :class: dropdown
# 1. Com a função construtora `list()`, crie uma lista `animais_copia` de `animais`:
# 
#         animais_copia = list(animais)
# 
# 1. `animais_copia` possui os mesmos elementos que `animais`? Verifique;
# 
#         # Sim, possui:
#         print(animais)
#         print(animais_copia)
# 
# 1. `animais_copia` e `animais` são os mesmos objetos (na memória)? Verifique;
# 
#         # São objetos independentes (diferentes):
#         print(f"ID animais: {id(animais)}")
#         print(f"ID animais_copia: {id(animais_copia)}")
# 
# 1. Os elementos de `animais_copia` e `animais` são os mesmos (na memória)? Verifique;
#         
#         # Sim, pois os elementos aninhados referem-se aos mesmos locais da memória:
#         
#         print(f"ID mamiferos: {id(animais[0])}")
#         print(f"ID mamiferos_copia: {id(animais_copia[0])}")
# 
#         print(f"ID aves: {id(animais[1])}")
#         print(f"ID aves_copia: {id(animais_copia[1])}")
# 
#         print(f"ID artropodes: {id(animais[2])}")
#         print(f"ID artropodes_copia: {id(animais_copia[2])}")
# 
# 1. Em `animais_copia`, mude a string `ornitorrinco` para `anta` e printe `animais_copia`;
# 
#         animais_copia[0][2] = "anta"
#         print(animais_copia)
# 
# 1. Em `animais_copia`, mude a string `águia` para `papagaio` e printe `animais_copia`;
# 
#         animais_copia[1][1] = "papagaio"
#         print(animais_copia)
# 
# 1. Em `animais_copia`, mude a string `chilopoda` para `lacraia` e printe `animais_copia`;
# 
#         animais_copia[2][0] = "lacraia"
#         print(animais_copia)
# 
# 1. Verifique as listas `animais_copia` e `animais`. Os elementos de `animais` mudaram após a modificação de `animais_copia`? Que tipo de cópia foi feita no passo 1 com a função `list()`?
# 
#         # Todos os elementos originais foram modificados.
#         # list() faz uma cópia shallow
#         
#         print(animais)
#         print(animais_copia)
# ```

# ## Questão 8

# Abaixo, temos uma lista numérica `numeros`. Com ela:
# 
# 1. Crie uma nova lista `copia` utilizando slicing total. Qual tipo de cópia estamos fazendo?;
# 1. Verifique se os elementos de ambas listas são os mesmos;
# 1. Em `copia`, modifique o elemento `104.3` para a string `Python`;
# 1. Verifique ambas listas novamente. Os elementos originais mudaram? Por que?

# In[10]:


numeros = [
    104.3, 99.5, 98.9, 102.9, 94.9,
    97.1, 98.3, 2.71, 3.14, 6.02
]


# ```{admonition} Resposta
# :class: dropdown
# 1. Crie uma nova lista `copia` utilizando slicing total. Qual tipo de cópia estamos fazendo?;
# 
#         # Esta é uma cópia shallow:
#         copia = numeros[:]
#         
# 1. Verifique se os elementos de ambas listas são os mesmos;
# 
#         # Sim, são os mesmos:
#         print(numeros)
#         print(copia)
#         
# 1. Em `copia`, modifique o elemento `104.3` para a string `Python`;
# 
#         copia[0] = "Python"
#         
# 1. Verifique ambas listas novamente. Os elementos originais mudaram? Por que?
#         
#         print(numeros)
#         print(copia)
# 
# No passo 4, vimos que a lista original não foi afetada. Isto porque, quando falamos de cópias e as diferenciamos em *shallow* ou *deep*, só percebemos a diferença em objetos compostos. Caso contrário, não diferença. Aqui, não estamos lidando com objetos compostos, mas com listas que possuem **literais** (valores constantes) como elementos. No caso, os literais são números (*Floating point literals*) e string (*string literals*).
# ```

# (numeros_perg)=
# ## Questão 9

# No código abaixo, temos a lista `numeros`. 

# In[11]:


numeros = [
    104.3, 99.5, 98.9, 102.9, 94.9,
    97.1, 98.3, 111, 222, 333
]


# Um programador fez o seguinte código:

# In[12]:


numeros[-1:-4:-1] = [6.02, 3.14, 2.71]


# Explique o código acima. O que este código está fazendo?

# ```{admonition} Resposta
# :class: dropdown
# 
# Estamos fazendo um slicing com indexação negativa. Vamos por partes. Primeiro: ao executarmos
# 
#         numeros[::-1]
# 
# Estamos invertendo a ordem toda a lista. Após, colocamos o index inicial de `-1` e o index final de `-4`:
# 
#         numeros[-1:-4:-1]
# 
# Ou seja, fazemos um slicing negativo do index `-1` ao index `-4` (exclusivo). Com isto, selecionamos os três elementos que desejamos (os três últimos) e os substituímos, respectivamente, pelos elementos da outra lista com o código:
# 
#         numeros[-1:-4:-1] = [6.02, 3.14, 2.71]
# ```

# ## Questão 10

# Ainda sobre a questão [anterior](numeros-perg), responda: por que conseguimos modificar a lista? Se o código

# In[13]:


numeros[-1:-4:-1]


# Retorna uma lista, o que o seguinte código está fazendo, tecnicamente?

# In[14]:


numeros[-1:-4:-1] = [6.02, 3.14, 2.71]


# ```{admonition} Resposta
# :class: dropdown
# 
# Com o slicing, estamos pegando um subconjunto da lista `numeros`. Por isto, os elementos do slicing são apenas referências aos respectivos elementos da lista original. Com a designação
# 
#         numeros[-1:-4:-1] = [6.02, 3.14, 2.71]
#         
# Estamos trocando os respectivos elementos de ambas listas. Como estamos lidando com as referências, modificamos a lista original. Podemos verificar que são os mesmos elementos executando:
# 
#         a = numeros[-1:-4:-1]
#         print(id(a[0]))
#         print(id(numeros[-1]))
# ```

# (sort_perg)=
# ## Questão 11

# Há um último método de listas chamado `sort()`. Veja a ajuda deste método, pesquise sobre ele e faça:
# 
# 1. Ordene a lista `numeros` em ordem crescente e printe `numeros`;
# 1. Ordene a lista `numeros` em ordem decrescente e printe `numeros`.

# In[15]:


numeros = [
    104.3, 99.5, 98.9, 102.9, 94.9,
    97.1, 98.3, 2.71, 3.14, 6.02
]


# ```{admonition} Resposta
# :class: dropdown
# 
# 1. Ordene a lista `numeros` em ordem crescente e printe `numeros`:
# 
#         numeros.sort()
#         print(numeros)
#         
# 1. Ordene a lista `numeros` em ordem decrescente e printe `numeros`.
# 
#         numeros.sort(reverse=True)
#         print(numeros)
# ```

# ## Questão 12

# Abaixo, temos a lista de strings `letras`.
# 
# 1. Ordene esta lista, mas agora use a função `sorted()`.
# 1. Atualize a lista `letras`.
# 
# ```{admonition} Atenção
# `sorted()` é uma função, não um método! Ela está disponível diretamente em Python.
# ```

# In[16]:


letras = [
    "O", "w", "Z", "s", "r", "S", "Y", "y",
    "p", "F", "g", "h", "R", "W", "m", "t",
    "f", "J", "H", "q", "C", "b", "I", "e",
    "x", "V", "j", "N", "i", "c", "B", "X",
    "u", "P", "z", "T", "M", "Q", "v", "a",
    "d", "E", "G", "K", "L", "A", "n", "U",
    "D", "l", "k", "o"
]


# ```{admonition} Resposta
# :class: dropdown
# 
# 1. Ordene esta lista, mas agora use a função `sorted()`:
# 
#         sorted(letras)
#         
# 1. Atualize a lista `letras`:
# 
#         letras = sorted(letras)
#         print(letras)
# 
# A função `sorted()` não age *in-place*. Ela retorna uma nova lista modificada (ordenada), deixando a lista original intacta. Assim, para atualizarmos `letras`, precisamos remover a ligação deste nome para a lista antiga e ligá-la novamente à lista ordenada que é retornada por `sorted()`. É isto que fazemos na segunda tarefa.
# ```

# ## Questão 13

# Vimos todos os métodos de listas em Python. Estes métodos possuem a característica de serem *in-place*.
# 
# 1. Pesquise e defina algoritmos *in-place*.
# 1. Explique, por exemplo, como o método `append()` age *in-place*.
# 1. Se `append()` não funcionasse *in-place*, como faríamos para atualizar uma lista? Exemplifique com uma lista chamada `lst`.

# ```{admonition} Resposta
# :class: dropdown
# 
# **Pesquise e defina algoritmos in-place**.
# 
# Algoritmos *in-place* (funções, métodos, etc.) são aqueles que atualizam o objeto original sem retornar ou criar um novo. Estes algoritmos modificam um objeto mutável e, como nenhum novo objeto novo é criado, geralmente sua execução é mais rápida.
# 
# **Explique, por exemplo, como o método `append()` age in-place**.
# 
# Ao executarmos, por exemplo, o seguinte código:
# 
#         lst.append('a')
# 
# Python aumenta o índice da lista `lst` em uma unidade e insere a string `"a"` na sua última posição. Tudo isto sem necessidade de criar e retornar outra lista que possua os mesmos elementos de `lst`, mas com a string `"a"` adicionada.
# 
# **Se `append()` não funcionasse in-place, como faríamos para atualizar uma lista? Exemplifique com uma lista chamada `lst`**
# 
# Neste caso, teríamos que atualizar manualmente a lista. Se `append()` não é *in-place*, significa que este método retornaria uma nova lista com um elemento adicionado. Assim, basta que façamos uma nova designação de variável para atualizar `lst`, isto é, o nome `lst` será ligado à nova lista retornada e atualizada, como abaixo:
# 
#         lst = lst.append("a")
# ```

# ## Questão 14

# Veja o código abaixo e tente entender o que está sendo feito. Depois, execute-o para confirmar se estava correto. Explique o que este código está fazendo.

# In[17]:


[1,2,3] * 3


# ```{admonition} Resposta
# :class: dropdown
# 
# O operador `*` funciona aqui tal como em strings: executa uma repetição. Se temos uma lista e um inteiro como operandos, os elementos da lista serão repetidos e retornados em uma única lista.
# ```

# ## Questão 15

# Veja o código abaixo e tente entender o que está sendo feito. Depois, execute-o para confirmar se estava correto. Explique o que este código está fazendo.

# In[18]:


[1,2,3] + [4, 5, 6]


# ```{admonition} Resposta
# :class: dropdown
# 
# O operador `+` funciona aqui como um concatenador. Se temos duas listas como operandos, é retornada uma nova lista unindo todos os elementos de ambos operandos.
# ```
