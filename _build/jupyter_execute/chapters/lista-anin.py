#!/usr/bin/env python
# coding: utf-8

# # Listas Aninhadas

# Uma vez que listas aceitam qualquer tipo de estrutura em Python, podemos então ter listas dentro de listas — as chamadas **listas aninhadas**. Em outras palavras: listas que possuem listas como elementos. Vamos criar algumas.

# In[1]:


aninhado = [
    [0, 1, 2, 3], # Lista aninhada 1
    [True, False], # Lista aninhada 2
    ["str1", "str2", "str3"] # Lista aninhada 3
]

print(aninhado)


# Na lista `aninhado` encontram-se três outras listas, cada uma contendo seus próprios dados.
# 
# Como podemos acessar estes elementos? Da mesma forma que acessamos listas simples: utilizando os índices dos elementos desejados e indexando cada lista com o operador `[]`.
# 
# Primeiramente vamos acessar a primeira lista aninhada — a lista de números inteiros. Esta lista está no index zero:

# In[2]:


print(aninhado[0])


# Acessamos a lista corretamente. Podemos acessar todas as outras listas aninhadas da mesma forma:

# In[3]:


# Segunda lista aninhada - booleanos (index 1):
print(aninhado[1])


# In[4]:


# Terceira lista aninhada - strings (index 2):
print(aninhado[2])


# Tais listas aninhadas são elementos de uma lista, então são tratadas como tal. Se tentarmos acessar com um index inexistente, teremos um erro:

# In[5]:


print(aninhado[3])


# Como qualquer outro elemento de uma lista, também podemos indexar negativamente:

# In[6]:


print(aninhado[-1])


# ## Acessando Elementos Aninhados

# Podemos acessar os elementos próprios de cada lista aninhada adicionando mais um operador `[]` para indicar o aninhamento de index. A sintaxe torna-se, então:

# In[7]:


lista[][]


# Assim, estamos fazendo uma dupla indexação:

# In[8]:


lista[primeira_indexacao][segunda_indexacao]


# - Na primeira indexação, acessamos **a lista aninhada**.
# - Na segunda indexação, acessamos **os elementos da lista aninhada** selecionada.
# 
# Vamos acessar os elementos da lista de inteiros (index zero):

# In[9]:


# Primeiro elemento da lista de inteiros:
print(aninhado[0][0])


# In[10]:


# Segundo elemento da lista de inteiros:
print(aninhado[0][1])


# In[11]:


# Terceiro elemento da lista de inteiros:
print(aninhado[0][2])


# In[12]:


# Quarto elemento da lista de inteiros:
print(aninhado[0][3])


# Da mesma forma, acessamos os elementos das outras listas aninhadas:

# In[13]:


# Acessando os elementos da lista de booleanos:

print(aninhado[1][0]) # Primeiro elemento
print(aninhado[1][1]) # Segundo elemento


# In[14]:


# Acessando os elementos da lista de strings:

print(aninhado[2][0]) # Primeiro elemento
print(aninhado[2][1]) # Segundo elemento
print(aninhado[2][2]) # Terceiro elemento


# ## Generalização

# Podemos, assim, generalizar para qualquer quantidade de aninhamento que houver: basta adicionar mais operadores de indexação `[]` para cada aninhamento existente. Por exemplo, com três aninhamentos, a sintaxe seria:

# In[15]:


lista[primeira_indexacao][segunda_indexacao][terceira_indexacao]


# Com quatro aninhamentos:

# In[16]:


lista[primeira_indexacao][segunda_indexacao][terceira_indexacao][quarta_indexacao]


# Obviamente, quanto mais aninhamento, mais difícil é o código, menos legível ele se torna e mais pesado seu processamento. Se necessita de vários aninhamentos, provavelmente há outra forma melhor de se fazer o que deseja.

# (copias)=
# ## Cópias

# Aproveitaremos nosso conhecimento sobre listas para falarmos sobre um tópico importante: cópias de objetos compostos (*compound objects*), isto é, objetos que contêm outros objetos (como as listas aninhadas).
# 
# Nesta seção, vamos precisar de uma nova lista para trabalharmos e de importar um módulo de Python. Módulos são códigos que importamos para usar em nosso programa atual. Abordaremos especificamente este assunto em outro capítulo, por agora basta saber como importar um módulo. Aqui, importaremos o módulo `copy`, que já é disponibilizado com o Python. Para podermos usá-lo, executamos o código:

# In[17]:


from copy import copy, deepcopy


# E já podemos utilizá-lo, como faremos em breve. Agora, criemos uma lista aninhada:

# In[18]:


# Lista 1:
mamiferos = ["gato", "macaco", "ornitorrinco"]

# Lista 2:
aves = ["ema", "seriema", "arara"]

# Lista aninhada:
animais = [mamiferos, aves]

print(animais)


# Em nossa lista aninhada, temos duas listas: `mamiferos` e `aves`. E agora vamos criar uma nova variável a partir da variável `animais` e vejamos seus locais na memória:

# In[19]:


copia_animais = animais


# In[20]:


print(f"ID: {id(copia_animais)}\nID: {id(animais)}")


# O código:
# 
# ```python
# copia_animais = animais
# ```
# 
# Na verdade não cria uma lista diferente, isto é, uma cópia independente da lista `animais`. De fato, a designação com o operador `=` não copia objeto algum, mas sim, cria ligações entre os objetos em questão.
# 
# Em nosso caso, então, estamos apenas criando uma nova referência para a lista `animais`: agora, as variáveis `copia_animais` e `animais` referem-se à mesma lista na memória do computador. Fato verificado pelas mesmas id's acima.
# 
# Podemos verificar também que as listas aninhadas são também as mesmas:

# In[21]:


# ID's das listas de mamíferos:
print(f"ID: {id(copia_animais[0])}\nID: {id(animais[0])}")


# In[22]:


# ID's das listas de aves:
print(f"ID: {id(copia_animais[1])}\nID: {id(animais[1])}")


# Diversas vezes, quando lidando com objetos mutáveis, desejamos criar cópias destes objetos para que possamos mudar a cópia sem afetar o original. Quando tratamos de cópias de objetos, existem em Python dois tipos de cópias que podem ser feitas: *shallow copy* e *deep copy*.
# 
# ```{note}
# A diferenciação entre cópias *shallow* e *deep* é apenas importante quando estamos lidando com objetos compostos e com elementos mutáveis. Se os elementos forem imutáveis, não veremos diferença.
# ```

# ### Shallow copy

# Tecnicamente, uma *shallow copy* cria um novo objeto (uma nova lista), mas coloca neste objeto **referências aos mesmos objetos originais**. Então, cria uma nova lista com os mesmos objetos.
# 
# Já tivemos contato com *shallow copies*, mesmo sem saber: quando falamos de [slicing total](slic-total), vimos que a sintaxe
# 
# ```python
# lista[:]
# ```
# 
# Retornava todos os elementos da lista em questão. Vamos designar, com esta sintaxe, uma nova variável `copia_animais`:

# In[23]:


# Copiando a lista:
copia_animais = animais[:]


# E vejamos seus conteúdos:

# In[24]:


print(animais)


# In[25]:


print(copia_animais)


# Os conteúdos parecem ser os mesmos. Vejamos se de fato são as mesmas listas através de seus locais na memória:

# In[26]:


print(id(animais))
print(id(copia_animais))


# São listas independentes! E suas listas aninhadas?

# In[27]:


print(id(animais[0]))
print(id(copia_animais[0]))


# In[28]:


print(id(animais[1]))
print(id(copia_animais[1]))


# As listas aninhadas, em ambas `animais` e `copia_animais` são as mesmas. O que fizemos aqui?
# 
# Com o slicing total `[:]`, fizemos uma cópia *shallow* da lista `animais`, isto é, criamos um novo objeto (uma nova lista) e chamamos de `copia_animais`. Dentro deste novo objeto, colocamos as referências das listas aninhadas originais, por isto elas são as mesmas da lista original. Para verificar isto, modificaremos a lista original `animais`:

# In[29]:


# Lista original:
print(animais[0])


# In[30]:


# Cópia:
print(copia_animais[0])


# In[31]:


# Modificando a lista original:
animais[0][2] = "quati"


# E vejamos ambas listas novamente:

# In[32]:


# Lista original:
print(animais[0])


# In[33]:


# Cópia:
print(copia_animais[0])


# De fato, ambas listas mudaram, pois os objetos aninhados em `copia_animais` são apenas referências aos objetos aninhados originais.
# 
# No início desta seção, importamos o módulo `copy`. Este módulo possui duas funções para cópias de objetos. Uma destas funções é `copy()`. Com esta, fazemos uma cópia *shallow* de um objeto, assim como acabamos de fazer. Somente para fins de ilustração, façamos uma outra cópia *shallow* da lista `animais`:

# In[34]:


copia2_animais = copy(animais)


# E podemos ver que esta lista é uma cópia da última versão da nossa lista `animais`:

# In[35]:


print(copia2_animais)


# Além de ser uma nova lista:

# In[36]:


print(id(animais))
print(id(copia_animais))
print(id(copia2_animais))


# E que as listas aninhadas são as mesmas:

# In[37]:


print(id(animais[0]))
print(id(copia_animais[0]))
print(id(copia2_animais[0]))


# In[38]:


print(id(animais[1]))
print(id(copia_animais[1]))
print(id(copia2_animais[1]))


# ### Deep copy

# Uma *deep copy* cria um novo objeto (uma nova lista), mas **cria cópias independentes dos objetos originais e as coloca no novo objeto**. Então, cria uma nova lista com cópias verdadeiras dos elementos originais.
# 
# Primeiramente, reinicimeos nossa lista original:

# In[39]:


# Reiniciando nossa lista:

mamiferos = ["gato", "macaco", "ornitorrinco"]

aves = ["ema", "seriema", "arara"]

animais = [mamiferos, aves]

print(animais)


# Para fazermos uma *deepcopy*, precisamos da outra função do módulo `copy`. Esta função é chamada `deepcopy()` e faz uma cópia verdadeira de todo o objeto e seus elementos. Vamos utilizá-la:

# In[40]:


# Copiando a lista:
copia_animais = deepcopy(animais)


# Podemos ver que seus elementos são os mesmos:

# In[41]:


print(animais)


# In[42]:


print(copia_animais)


# E vejamos se são objetos diferentes:

# In[43]:


print(f"ID: {id(copia_animais)}\nID: {id(animais)}")


# De fato, são objetos independentes. E quanto aos seus elementos (as listas aninhadas)?

# In[44]:


print(id(animais[0]))
print(id(copia_animais[0]))


# In[45]:


print(id(animais[1]))
print(id(copia_animais[1]))


# Todos objetos estão em locais diferentes da memória, então, teoricamente, são todos objetos independentes. Vamos modificar a lista `animais`:

# In[46]:


# Lista original:
print(animais[0])


# In[47]:


# Cópia:
print(copia_animais[0])


# In[48]:


# Modificando a lista original:
animais[0][2] = "quati"


# E vejamos ambas listas novamente:

# In[49]:


# Lista original:
print(animais[0])


# In[50]:


# Cópia:
print(copia_animais[0])


# De fato, conseguimos criar uma *deep copy*. Através desta, fizemos uma cópia verdadeira da lista `animais`, onde todos elementos são independentes dos originais. Podemos então tratar ambas listas aninhadas como listas independentes — uma não alterará a outra.

# ```{admonition} Resumo
# - **Designação**: Não cria cópia alguma, apenas liga amis uma variável ao mesmo local na memória.
# - **Shallow copy**: Cria um novo objeto e coloca as referências dos elementos originais neste objeto; "clona" os elementos originais.
# - **Deep copy**: Cria um novo objeto e faz cópias verdadeiras de seus elementos. Todos elementos são tratados como elementos independentes.
# ```
