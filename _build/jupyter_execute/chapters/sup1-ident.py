#!/usr/bin/env python
# coding: utf-8

# (oper_ident)=
# # Comparação de Identidade

# Com as comparações de identidade (*identity comparisons*), queremos verificar se **dois objetos são o mesmo**. Não estamos verificando se eles possuem os mesmos elementos, mas sim, se ambos são o mesmo objeto; um único local na memória. Estas comparações são realizadas com dois operadores: `is` e `is not`, os quais verificam a identidade de um objeto. A identidade de um objeto é verificada por um função já conhecida nossa: `id()`, a qual utilizamos desde o início do curso.
# 
# Nesta seção faremos algo diferente: ao invés de apresentarmos os operadores e fazermos exemplos com cada um deles, ambos serão apresentados e faremos um único exemplo com ambos. Isto porque refaremos um exemplo de um modo mais simples: o exemplo das [cópias](copias). Basicamente, tudo o que fizemos naquela seção foi verificar se dois objetos são o mesmo. Agora, faremos esta seção novamente, mas utilizando os operadores de comparação de identidade.

# ## Operador `is`

# O operador `is` retorna `True` se ambos operandos forem o mesmo objeto e `False` se não forem. Usamos o operador em uma expressão, como:

# In[1]:


obj1 is obj2


# Onde `obj1` e `obj2` são os objetos que queremos saber se são o mesmo (na memória).

# ## Operador `is not`

# O operador `is not` retorna `True` se ambos operandos forem objetos diferentes (diferentes locais da memória) e `False` se forem o mesmo. O operador `is not` tem o valor booleano inverso do operador `is`. Usamos o operador em uma expressão, como:

# In[2]:


obj1 is not obj2


# Onde `obj1` e `obj2` são os objetos que queremos saber se diferentes (na memória).

# ## Cópias com Operadores de Identidade

# Para fins de reprodutibilidade, pegaremos as mesmas listas aninhadas:

# In[3]:


# Lista 1:
mamiferos = ["gato", "macaco", "ornitorrinco"]

# Lista 2:
aves = ["ema", "seriema", "arara"]

# Lista aninhada:
animais = [mamiferos, aves]

print(animais)


# E importaremos as funções do módulo `copy`:

# In[4]:


from copy import deepcopy


# Como já detalhamos o processo na [seção original](copias), podemos partir diretamente para a criação da variável e das cópias. Primeiro, vamos criar uma nova variável a partir de `animais` e vejamos seus locais na memória.

# In[5]:


copia_animais = animais


# In[6]:


# São o mesmo objeto (id's iguais):
print(f"ID: {id(copia_animais)}\nID: {id(animais)}")


# Como sabemos agora, o operador `is` verifica a identidade de objetos e retorna `True` se os locais da memória (os id's) forem os mesmos, então podemos utilizá-lo para tal:

# In[7]:


print(copia_animais is animais)


# E o operador `is not`:

# In[8]:


print(copia_animais is not animais)


# De fato, o operador `is` confirma que ambos objetos `copia_animais` e `animais` são o mesmo (estão no mesmo local da memória). Além disso, o operador `is not` retorna o valor booleano inverso.
# 
# Relembrando: o operador `=` **não faz cópias**! Ele apenas faz uma ligação de um nome novo àquele local da memória.
# 
# ```{note}
# A partir deste momento, sempre que formos verificar a identidade de objetos, utilizamos os operadores `is` e `is not`. Deixaremos para usar a função `id()` somente para casos específicos, se houver necessidade.
# ```
# 
# Agora as cópias *shallow* e *deep*:

# In[9]:


# Shallow copy:
animais_shallow = animais[:]

# Deep copy:
animais_deep = deepcopy(animais)


# Comparemos primeiramente a lista *shallow* com a original:

# In[10]:


print(animais_shallow is animais)


# Como o resultado é `False`, sabemos que as listas `animais_shallow` e `animais` são objetos independentes. Vejamos seus elementos:

# In[11]:


print(animais_shallow[0] is animais[0])


# In[12]:


print(animais_shallow[1] is animais[1])


# As listas aninhadas são as mesmas.
# 
# Como estudamos na seção original, é exatamente isto que a cópia *shallow* faz: ela cria um novo objeto e, dentro deste, insere referências aos objetos originais.
# 
# Agora vejamos a cópia *deep*:

# In[13]:


print(animais_deep is animais)


# Como o resultado é `False`, sabemos que as listas `animais_deep` e `animais` também são objetos independentes. Vejamos seus elementos:

# In[14]:


print(animais_deep[0] is animais[0])


# In[15]:


print(animais_deep[1] is animais[1])


# De fato, é o que esperávamos: a cópia *deep* criou uma nova lista e, dentro dela, fez cópias verdadeiras dos elementos da lista original. Verificamos isto quando todos os resultados do operador `is` aplicados acima são `False`.
