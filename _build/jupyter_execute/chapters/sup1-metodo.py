#!/usr/bin/env python
# coding: utf-8

# # Métodos de Strings

# Uma seção muito importante, agora veremos os métodos mais importantes de strings.

# ## Método `count()`

# O método `count()` retorna o número de ocorrências de um elemento na string. Sua sintaxe é:

# In[1]:


"string".count(elemento)


# Exemplos:

# In[2]:


# Quantos "e" estão na string:
"Paralelepípedo".count("e")


# In[3]:


# Quantos "l" estão na string:
"Paralelepípedo".count("l")


# In[4]:


# Quantos "p" estão na string:
"Paralelepípedo".count("p")


# In[5]:


# Quantos "P" estão na string:
"Paralelepípedo".count("P")


# ```{note}
# Lembre-se que Python é *case sensitive*! Ele diferencia maiúsculas de minúsculas.
# ```

# ## Método `format()`

# ```{note}
# Este método está aqui somente para fins de completude, você talvez possa ver esta forma de formatação em vários códigos mais antigos. Atualmente, se você quiser formatar strings, use [f-strings](f-string).
# ```

# O método `format()` retorna uma operação de formatação da string, assim como fizemos em f-strings. A diferença é a sintaxe. Com o método `format()`, colocamos apenas os operadores `{}` na string nos locais onde queremos fazer a interpolação de strings e dentro do método colocamos os valores de fato. Com f-strings, fazemos tudo de uma única vez. Exemplo:

# In[6]:


"Esta é outra {} de interpolação de {}".format("forma", "strings")


# Acima, a string com os operadores `{}`:

# In[7]:


"Esta é outra {} de interpolação de {}"


# Apartir desta, executamos o método `format()` e, neste, fornecemos as strings para interpolação, ou seja, para "colocar" dentro da string inicial. No caso, as strings são `"forma"` e `"strings"`. O método, fica, então:

# In[8]:


format("forma", "strings")


# Por fim, o método torna-se:

# In[9]:


"Esta é outra {} de interpolação de {}".format("forma", "strings")


# Se houvessem mais strings para interpolação, haveriam mais `{}` dentro da string e mais strings dentro do método. Outros exemplos:

# In[10]:


"Estou estudando {}!".format("Python")


# In[11]:


"{}, 1 + 2 = {}".format("Sim", 1+2)


# ## Método `isalpha()`

# O método `isalpha()` retorna `True` se:
# 
# 1. A string não é vazia;
# 1. Se todos elementos da string estão no alfabeto.
# 
# Se uma destas condições não for verficada, retorna `False`. Sua sintaxe é:

# In[12]:


"string".isalpha()


# Exemplos:

# In[13]:


# Verdadeiro, pois é uma string somente com letras:
"Paralelepípedo".isalpha()


# In[14]:


# Falso, pois a string possui números:
"Paralelepípedo143".isalpha()


# In[15]:


# Falso, pois a string possui um espaço em branco:
"Parale lepípedo".isalpha()


# In[16]:


# Falso, pois é uma string vazia:
"".isalpha()


# ## Método `isnumeric()`

# O método `isnumeric()` retorna `True` se:
# 
# 1. A string não é vazia;
# 1. Se todos elementos da string são numéricos.
# 
# Se uma destas condições não for verficada, retorna `False`. Sua sintaxe é:

# In[17]:


"string".isnumeric()


# Exemplos:

# In[18]:


# Verdadeiro, pois é uma string somente com números:
"1234".isnumeric()


# In[19]:


# Falso, pois a string possui uma letra:
"1a234".isnumeric()


# In[20]:


# Falso, pois a string possui um espaço em branco:
"12 34".isnumeric()


# In[21]:


# Falso, pois é uma string vazia:
"".isnumeric()


# ## Método `isalnum()`

# O método `isalnum()` retorna `True` se:
# 
# 1. A string não é vazia;
# 1. Se todos elementos da string são alfanuméricos.
# 
# Se uma destas condições não for verficada, retorna `False`. Um elemento é **alfanumérico*** se este é: uma letra do alfabeto (maiúscula ou minúscula) ou um número.
# 
# ```{note}
# O método `isalnum()` retornará `True` se qualquer um dos métodos `isalpha()`, `isnumeric()`, `isdecimal()` ou `isdigit()` retornar `True`.
# 
# > Pesquise sobre os métodos `isdecimal()` e `isdigit()` para conhecê-los!
# ```
# 
# Sua sintaxe é:

# In[22]:


"string".isalnum()


# Exemplos:

# In[23]:


# Verdadeiro, pois todos elementos são alfanuméricos:
"Paralelepípedo".isalnum()


# In[24]:


# Verdadeiro, pois todos elementos são alfanuméricos:
"Paralelepípedo143".isalnum()


# In[25]:


# Falso, pois há um espaço em branco:
"Parale lepípedo".isalnum()


# In[26]:


# Verdadeiro, pois todos elementos são alfanuméricos:
"1234".isalnum()


# In[27]:


# Verdadeiro, pois todos elementos são alfanuméricos:
"1a234".isalnum()


# In[28]:


# Falso, pois há um espaço em branco:
"12 34".isalnum()


# In[29]:


# Falso, pois é uma string vazia:
"".isalnum()


# ## Método `islower()`

# O método `islower()` retorna `True` se:
# 
# 1. A string não é vazia;
# 1. Se todos elementos **afabéticos** da string são letras minúsculas.
# 
# Se uma destas condições não for verficada, retorna `False`. Sua sintaxe é:

# In[30]:


"string".islower()


# Exemplos:

# In[31]:


# Falso, pois há uma letra maiúscula:
"Paralelepípedo".islower()


# In[32]:


# Verdadeiro, pois só há letras minúsculas:
"paralelepípedo".islower()


# In[33]:


# Verdadeiro, pois todas as letras são minúsculas:
"paralelepípedo143".islower()


# In[34]:


# Verdadeiro, pois todas as letras são minúsculas:
"parale lepípedo".islower()


# In[35]:


# Falso, pois não há letras:
"1234".islower()


# In[36]:


# Falso, pois não há letras, somente espaço em branco:
" ".islower()


# In[37]:


# Falso, pois é uma string vazia:
"".islower()


# ## Método `isupper()`

# O método `isupper()` retorna `True` se:
# 
# 1. A string não é vazia;
# 1. Se todos elementos **afabéticos** da string são letras maiúsculas.
# 
# Se uma destas condições não for verficada, retorna `False`. Sua sintaxe é:

# In[38]:


"string".isupper()


# Exemplos:

# In[39]:


# Falso, pois há letra minúsculas:
"Banana".isupper()


# In[40]:


# Verdadeiro, pois só há letras maiúsculas:
"BANANA".isupper()


# In[41]:


# Verdadeiro, pois todas as letras maiúsculas:
"BANANA143".isupper()


# In[42]:


# Verdadeiro, pois todas as letras são maiúsculas:
"BAN ANA".isupper()


# In[43]:


# Falso, pois não há letras:
"1234".isupper()


# In[44]:


# Falso, pois não há letras, somente espaço em branco:
" ".isupper()


# In[45]:


# Falso, pois é uma string vazia:
"".isupper()


# ## Método `istitle()`

# O método `istitle()` retorna `True` se:
# 
# 1. A string não é vazia;
# 1. Se o primeiro elemento da string é maiúsculo e todos os outros elementos são minúsculos, ou seja, apenas a primeira letra maiúscula.
# 
# Se uma destas condições não for verficada, retorna `False`. Sua sintaxe é:

# In[46]:


"string".istitle()


# Exemplos:

# In[47]:


# Verdadeiro, pois somente o "B" é maiúsculo:
"Banana".istitle()


# In[48]:


# Verdadeiro, pois somente o "B" é maiúsculo dentre as letras:
"Banana2".istitle()


# In[49]:


# Falso, pois todas as letras são maiúsculas:
"BANANA".istitle()


# In[50]:


# Falso, pois há um espaço em branco:
"Ba ana".istitle()


# In[51]:


# Falso, pois não há letras:
"1234".isupper()


# In[52]:


# Falso, pois é uma string vazia:
"".isupper()


# ## Método `lower()`

# O método `lower()` retorna uma cópia da string original com todos os caracteres alfabéticos em minúsculo. Sua sintaxe é:

# In[53]:


"string".lower()


# Exemplos:

# In[54]:


"Banana".lower()


# In[55]:


"BANANA".lower()


# In[56]:


"Ban Ana".lower()


# ## Método `upper()`

# O método `upper()` retorna uma cópia da string original com todos os caracteres alfabéticos em maiúsculo. Sua sintaxe é:

# In[57]:


"string".upper()


# Exemplos:

# In[58]:


"Banana".upper()


# In[59]:


"banana".upper()


# In[60]:


"Ban Ana".upper()


# ## Método `capitalize()`

# O método `capitalize()` retorna uma cópia da string original na qual a primeira letra de toda a string é maiúscula e todas as outras são minúsculas. Sua sintaxe é:

# In[61]:


"string".capitalize()


# Exemplos:

# In[62]:


"uma frase".capitalize()


# In[63]:


"OLÁ Mundo!".capitalize()


# In[64]:


"UmA FrasE bem GRANDE".capitalize()


# ## Método `title()`

# O método `title()` retorna uma string onde todas as palavras possuem apenas a primeira letra maiúscula. Sua sintaxe é:

# In[65]:


"string".title()


# Exemplos:

# In[66]:


"uma frase".title()


# In[67]:


"Olá mundo!".title()


# In[68]:


"UmA FrasE bem GRANDE".title()


# ## Método `strip()`

# O método `strip()` retorna uma cópia da string original sem os espaços em branco que econtram-se no ínicio e no fim da string. Sua sintaxe é:

# In[69]:


"string".strip()


# Exemplos:

# In[70]:


"  Espaço em branco no início".strip()


# In[71]:


"Espaço em branco no fim  ".strip()


# In[72]:


"  Espaço em branco no início e no fim  ".strip()


# ## Método `join()`

# O método `join()` pega todos os elementos de um objeto iterável, os concatena com e retorna uma única string com estes elementos. O separador dos elementos concatenados é a string da qual o método está sendo executado. Sua sintaxe é:

# In[73]:


"string".join(iterável)


# Exemplo: Vamos pegar os números `0` até `9` e concatenátlos em uma única string, onde o separador é o hífen (`-`).

# In[74]:


"-".join("0123456789")


# Acima, temos uam string `"-"` que representa o separador. A partir desta, executamos o método `join()` com a string `0123456789`. O método `join()` concatena estas strings e e utiliza `-` como separador.
# 
# ```{note}
# Strings também são objetos iteráveis!
# ```
# 
# Outro exemplo:

# In[75]:


", ".join(["Fulano", "Beltrano", "Ciclano"])


# Acima, nosso separador é `", "`: uma vírgula com um espaço em branco. A string será então a concatenação deste separador com cada palavra da lsita fornecida como iterável.

# ## Método `replace()`

# O método `replace()` retorna uma cópia da string original com todas as ocorrências de uma substring substituídas por outra. Sua sintaxe é:

# In[76]:


"string".replace(old, new)


# Onde:
# 
# - `old`: substring que deseja substituir.
# - `new`: substring que deseja colocar na string.
# 
# Exemplos:

# In[77]:


"Ban ana".replace(" ", "")


# Acima, substituímos um espaço em branco (`" "`) por uma string vazia (`""`), resultando em uma única palavra (`"Banana"`).

# In[78]:


"Banana2".replace("2", "")


# Acima, substituímos a substring `"2"` por uma string vazia (`""`), resultando na "remoção" do número: `"Banana"`.
