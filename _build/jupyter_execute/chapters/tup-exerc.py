#!/usr/bin/env python
# coding: utf-8

# # Exercícios

# ```{admonition} **Tente Sozinho!**
# Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
# ```
# 
# ```{admonition} **Pesquise!**
# Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
# ```

# ## Questão 1

# Defina tuplas.

# ```{admonition} Resposta
# :class: dropdown
# Tuplas são estruturas de dados ordenadas e imutáveis utilizadas geralmente para agrupar dados homogêneos ou para agrupar dados heterogêneos (quando necessário).
# ```

# ## Questão 2

# Diferencie tuplas e listas. Você saberia dizer quando tuplas são mais adequadas do que listas?

# ```{admonition} Resposta
# :class: dropdown
# Tuplas possuem sintaxe diferente (usam parênteses) e são imutáveis. Contudo, uma outra grande diferença entre estas estruturas de dados é de extrema importância.
# 
# Listas são idealmente para elementos homogêneos; elementos com um mesmo sentido. Por exemplo, uma lista de usuários online: todos os elementos são usuários, mas, de acordo com o sentido desta lista, usuários podem ser adicionados (se estiverem online) ou removidos (se estiverem offline). Por isto uma lista seria ideal, pois é mutável e todos os elementos significam a mesma coisa. Tuplas não podem ser mudadas: uma vez que uma tupla de usuários fosse criada, ela não poderia ser modificada.
# 
# Tuplas podem ser utilizadas para armazenar objetos homogêneos, tal qual uma lista, mas que não devem ser modificados. Contudo, tuplas podem demonstrar uma estrutura com seus elementos — o que listas não conseguem ter. Qual estrutura? Por exemplo, você está trabalhando com uma interface que acessa a base de dados do IBGE e fornece uma cidade para uma certa função de IDH. Esta função retorna uma tupla:
# 
#         ("Goiânia", 0.799, 2010)
# 
# Esta tupla contém tem a seguinte estrutura:
# 
#         (cidade_fornecida, IDH, ano_da_pesquisa)
# 
# Esta tupla contém uma estrutura bem definida: todos os resultados apresentarão este resultado, independente de qual cidade seja. Ademais, os elementos podem todos serem do mesmo tipo. Neste exemplo, se as cidades fossem registradas com ID de números e o IDH fosse classificado categoricamente como `1` (baixo), `2` (médio) e `3` (alto), a tupla poderia ser:
# 
#         (526, 3, 2010)
# 
# A qual significaria a mesma coisa, só que codificada.
# 
# Esta é, de fato, a grande diferença de tuplas: a estrutura que seus elementos podem criar. Faremos vários exemplos disto quando começarmos a criar funções.
# ```

# ## Questão 3

# Crie uma tupla com um único elemento: a string `"Python"`. Chame esta tupla de `linguagem`.

# ```{admonition} Resposta
# :class: dropdown
# Podemos fazer de duas formas: Sem parênteses e com parênteses. As duas são demonstradas abaixo:
# 
#         linguagem = "Python",
#         linguagem = ("Python",)
# 
# E podemos verificar que são tuplas:
# 
#         print(type(linguagem))
# ```

# ## Questão 4

# Qual a diferença entre as três variáveis abaixo?

# In[1]:


var1 = (1)

var2 = 1,

var3 = (1,)


# ```{admonition} Resposta
# :class: dropdown
# As variáveis `var2` e `var3` são tuplas, enquanto `var1` é um número inteiro. Podemos verificar isto executando:
# 
#         print(f"Var1 é um {type(var1)}: {var1}")
#         print(f"Var2 é um {type(var2)}: {var2}")
#         print(f"Var3 é um {type(var3)}: {var3}")
# 
# O que cria as tuplas de fato são as vírgulas, enquanto os parênteses são opcionais! A sintaxe da `var1`:
# 
#         var1 = (1)
# 
# Não cria uma tupla. Quando queremos criar tuplas com um único elemento, **sempre** tem de haver uma vírgula após o elemento para simbolizar que estamos criando uma tupla. As variáveis `var2` e `var3` possuem esta vírgula.
# ```

# ## Questão 5

# O que está sendo feito no código abaixo? Explique.

# In[2]:


(1, 2, 3) + (4, 5, 6)


# ```{admonition} Resposta
# :class: dropdown
# Assim como em listas, o operador `+`, quando tem tuplas em ambos operandos, realiza a operação de concatenação. Por isto é retornado uma única tupla com os elementos de ambas.
# 
# > **Nota**: tecnicamente, o operador `+` sempre realizará concatenação quando em ambos operandos encontrarem-se objetos do tipo sequẽncia (tuplas são sequências).
# ```

# ## Questão 6

# O que está sendo feito no código abaixo? Explique.

# In[3]:


(1, 2, 3) * 4


# ```{admonition} Resposta
# :class: dropdown
# Assim como em listas, o operador `*`, quando tem uma tupla e um número inteiro como operandos, realiza a operação de repetição. Por isto é retornado uma única tupla com os elementos repetidos. Neste caso, os elementos da tupla são repetidos quatro vezes.
# 
# > **Nota**: tecnicamente, o operador `*` sempre realizará repetição quando seus operandos forem uma sequência (tuplas são sequências) e um número inteiro.
# ```

# ## Questão 7

# O código abaixo está incompleto. Complete-o de forma com que as variáveis `a-j` sejam criadas com o mesmo valor: `True`

# In[4]:


a, b, c, d, e, f, g, h, i, j =


# ```{admonition} Resposta
# :class: dropdown
# Poderíamos fazer:
# 
#         a = b = c = d = e = f = g = h = i = j = True
# 
# O qual possui o mesmo efeito, mas temos que completar o código dado. Com este código, temos o lado esquerdo do operador `=` designando dez variáveis (`a-j`). Sendo assim, temos de ter valores `True` suficientes para cada variável. Podemos conseguir uma tupla de `True` facilmente:
# 
#         (True,) * 10
# 
# Deste modo, o código completo é:
# 
#         a, b, c, d, e, f, g, h, i, j = (True,) * 10
# 
# E podemos confirmar com:
# 
#         print(a, b, c, d, e, f, g, h, i, j)
# ```

# ## Questão 8

# Na questão anterior, qual processo está sendo executado para que a operação fosse possível? Explique o que está sendo feito no caso do exercício anterior.

# ```{admonition} Resposta
# :class: dropdown
# Estamos utilizando *tuple unpacking* para conseguir fazer esta operação. O que está sendo feito é bem simples: ao criar a tupla:
# 
#         (True,) * 10
# 
# Cada elemento desta, ou seja, cada valor `True`, é distribuído ordenadamente e ligado à cada nome no operando esquerdo do operador `=`.
# ```

# ## Questão 9

# Abaixo, temos um código que identifica alguns produtos de diferentes lojas hipotéticas de informática. Pense no que está sendo feito no código, execute-o e faça o print de cada loja específica para ver quais produtos são vendidos.

# In[5]:


loja1, loja2, loja3 = (
    ["Fone de ouvido", "Cabo HDMI", "Conversor VGA-HDMI", "Carregador"],
    ["Película", "Cabo HDMI", "Conversor VGA-HDMI", "Carregador", "Mochila"]
)


# ```{admonition} Resposta
# :class: dropdown
# O código está errado. O erro encontra-se na quantodade diferente de elementos entre os operandos do oeprador `=`. No operando esquerdo, temos três nomes, então Python espera três valores dentro da tupla no operando direito. COntudo, há somente dois elementos na tupla: duas listas. É exatamente isto que o erro diz:
# 
# > ValueError: not enough values to unpack (expected 3, got 2)
# 
# Para corrigir isto temos duas opções:
# 
# 1. Criar uma terceira lista de produtos na tupla;
# 1. Retirar o nome `loja3`.
# 
# Como não sabemos o que `loja3` vende, vamos optar pela segunda opção. Assim, a resposta é:
# 
#         loja1, loja2 = (
#             ["Fone de ouvido", "Cabo HDMI", "Conversor VGA-HDMI", "Carregador"],
#             ["Película", "Cabo HDMI", "Conversor VGA-HDMI", "Carregador", "Mochila"]
#         )
# 
#         print(loja1)
#         print(loja2)
# ```

# ## Questão 10

# Temos uma nova tupla aninhada abaixo.
# 
# 1. Qual a diferença para a tupla aninhada que vimos [anteriormente](tupla_aninhamento)?
# 1. No primeiro elemento da tupla, tente modificar seus elementos para: `"Janeiro"`, `"Fevereiro` e `"Março"`.
# 1. Foi possível modificar? Por que?

# In[6]:


tup_anin = (
    ["Setembro", "Outubro", "Novembro"],
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [True, False]
)


# ```{admonition} Resposta
# :class: dropdown
# Contudo, como tuplas aceitam diferentes elementos, podemos aninhar com outros elementos também. Neste caso, aninhamos com listas.
# 
# Os elementos da tupla não podem ser modificados:
# 
#         tup_anin[0] = ("Janeiro", "Fevereiro", "Março")
# 
# Mas os elementos aninhados **podem** ser modificados, pois são listas:
# 
#         tup_anin[0][0] = "Janeiro"
#         tup_anin[0][1] = "Fevereiro"
#         tup_anin[0][2] = "Março"
#         
#         print(tup_anin)
# 
# Ao modificar os elementos da lista, não modificamos os elementos da tupla! A tupla continua possuindo três listas como elementos.
# ```

# ## Questão 11

# Ordene a tupla abaixo.

# In[7]:


tup = (5, 1, 9, 0, 7, 3, 6, 4, 2, 8)


# ```{admonition} Resposta
# :class: dropdown
# Não há como ordernar esta tupla in-place! Tuplas são imutáveis, então teremos obrigatoriamente criar um novo objeto, ordernar este objeto e fazer uma tupla nova a partir dele. Faremos isto com uma lista.
# 
#         # Transformando a tupla em lista:
#         lst = list(tup)
# 
#         # Ordenando a lista:
#         lst.sort()
# 
# Agora podemos transformar a lista ordenada `lst` em tupla novamente, com a função `tuple()`, a qual fará uma coerção do tipo `list` para o tipo `tuple`:
# 
#         tup_ordenada = tuple(lst)
# 
#         # Tupla ordenada:
#         print(tup_ordenada)
# ```

# ## Questão 12

# A tupla abaixo contém diversas redes sociais. Contudo, há alguns erros.
# 
# 1. Remova `"Gmail"`;
# 1. Letterboxd está escrito errado. Arrume para: `Letterboxd`
# 
# O resultado final deve ser uma tupla.

# In[8]:


redes = ("Letterboxsd", "Instagram", "Gmail", "Twitter", "TikTok")


# ```{admonition} Resposta
# :class: dropdown
# Para modificar esta tupla, teremos que fazer o mesmo processo que na questão anterior.
# 
#         # Transformando em lista:
#         redes_lst = list(redes)
# 
#         # Removendo Gmail:
#         redes_lst.remove("Gmail")
# 
#         # Arrumando a grafia (duas opções abaixo):
#         redes_lst[0] = "Letterboxd"
#         redes_lst[0] = redes_lst[0].replace("s", "")
# 
#         # Tranformando em tupla novamente:
#         redes = tuple(redes_lst)
# 
#         print(redes)
# ```
