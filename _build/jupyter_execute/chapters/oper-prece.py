#!/usr/bin/env python
# coding: utf-8

# # Precedência de Operadores

# Em programação, temos há os conceitos de **precedência** e **associatividade**.
# 
# A precedência de operadores é definida como a ordem na qual os operadores são executados. Por exemplo, na seguinte expressão:
# 
# ```python
# 2 + 5 * 8
# ```
# 
# A precedência de operadores de Python define que o operador `*` será executado antes do operador `+`. Desta forma, a operação de multiplicação será feita primeiramente e, após, a adição será feita. As operações aritméticas geralmente seguem a mesma ordem que na matemática.
# 
# Se todos os operadores em uma expressão possuem precedências diferentes, não há problema. Se alguns possuírem a mesma precedência, a associatividade entra em cena. Associatividade é a ordem na qual os operadores são avaliados quando estes possuem a mesma precedência. Em Python, a ordem avaliação de todos os operadores (com uma exceção) é a mesma: da esquerda para a direita. Por exemplo, na expressão:
# 
# ```python
# 8 + 2 * 10 / 2
# ```
# 
# O operador `+` possui menor precedência que os outros, por isto esta operação será avaliada por último. Os operadores `*` e `/` possuem a mesma precedência, então a ordem de avaliação em Python dita que a multiplicação será executada primeiro, pois o operador `*` vem antes do operador `/` (está à esquerda). Desta forma, a execução do código é:
# 
# 1. `8 + 2 * 10 / 2`
# 1. `8 + 20 / 2`
# 1. `8 + 10`
# 1. `18`
# 
# Entretanto, há um detalhe quando lidando com precedência e associatividade: a ordem de avaliação muda se houver expressões em parênteses (agrupamentos). Assim como na matemática, as expressões em parênteses têm a maior precedência, ou seja, são avaliadas primeiro — mesmo que o operador entre parênteses possua precedência menor. Vejamos este caso na mesma expressão anterior, agora com uma expressão entre parênteses:
# 
# ```python
# (8 + 2) * 10 / 2
# ```
# 
# Neste caso, a operação de adição será a primeira a ser executado, pois está dentro dos parênteses. Desta forma, o código é executado da seguinte maneira:
# 
# 1. `(8 + 2) * 10 / 2`
# 1. `10 * 10 / 2`
# 1. `100 / 2`
# 1. `50`
# 
# A tabela abaixo mostra a precedência dos operadores vistos neste capítulo.
# 
# ```{note}
# A tabela abaixo não está completa. Ainda há vários operadores que veremos adiante. Atualizaremos à medida com que os encontrarmos.
# ```

# |Operador|Precedência|Obs|
# |:--|:--|:--|
# |`(expressão)`|Maior precedência|-|
# |`**`|-|Exceção: sua ordem de avaliação é da direita para a esquerda|
# |`-X`|-|Operador `-` unário|
# |`*`, `/`, `//`, `%`|-|-|
# |`+`, `-`|-|Operadores aritméticos `+` e `-` binários|
# |`<`, `<=`, `>`, `>=`, `!=`, `==`|-|Comparações possuem mesma precedência|
# |`not`|-|-|
# |`and`|-|-|
# |`or`|Menor precedência|-|
