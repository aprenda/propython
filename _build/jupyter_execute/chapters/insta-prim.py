#!/usr/bin/env python
# coding: utf-8

# # Primeiro Contato

# Agora que já instalamos Python, vamos ter nosso primeiro contato com esta linguagem de programação. Como de praxe, o primeiro comando (ou conjunto de comandos) que se faz ao aprender uma nova linguagem é o famigerado "Olá mundo!" (*Hello World!*). Isto se dá, provavelmente, por ser algo simples e que nos dá um vislumbre da sintaxe da linguagem em questão.
# 
# Em Python, este código é extremamente simples. Necessitamos apenas da função `print()` e de colocar algo dentro dos parênteses. No caso, este algo é a frase `"Olá, Mundo!"`, com aspas mesmo. Abaixo executamos o código:

# In[1]:


print("Olá, Mundo!")


# É visivelmente perceptível que Python é uma linguagem enxuta e pouco verborrágica em comparação com outras linguagens, como:

# - C:
# 
# ```c
# #include<stdio.h>
# 
# int main(void) {
#     printf("Olá, Mundo!\n");
#     return 0;
# }
# ```

# - C++:
# 
# ```c++
# #include <iostream>
# 
# using namespace std;
# 
# int main() {
#    cout << "Olá, Mundo!" << endl;
#    return 0;
# }
# ```

# - Java:
# 
# ```java
# public class java {
#     public static void main(String[] args) {
#         System.out.println("Olá, Mundo!");
#     }
# }
# ```

# ## Comentários

# Antes de avançarmos nosso estudo de Python, este é o momento ideal para conhecermos uma ferramenta extremamente útil em qualquer linguagem de programação e que utilizaremos a todo momento: os comentários.
# 
# Comentários são partes do código-fonte que não são lidas pelo computador, isto é, ao construir o programa, o computador ignora tais partes do código e segue em frente. Os comentários são antecedidos por algum símbolo, como, por exemplo, `– –` ou `%` — o símbolo em específico é dependente da linguagem de programação. Em geral, os dois símbolos mais utilizados para representar comentários são `#` e `//`. Especificamente em Python, utilizamos o jogo da velha (`#`) para inserirmos um comentário no código.
# 
# **Comentários são feitos para o usuário.** Novamente, o computador ignora todas estas entradas. O objetivo de um comentário é explicar uma linha/parte do código objetivamente para que qualquer pessoa que esteja lendo o código possa saber o que está sendo feito ali. Isto auxilia enormemente todas as outras pessoas que terão contato com seu programa e a você mesmo, quando voltar ao seu código tempos depois de tê-lo feito.
# 
# Façamos nosso primeiro comentário em Python: utilizando nosso print anterior, basta começar o comentário com `#`, como abaixo.

# In[2]:


# Meu primeiro comando em Python: um print
print("Olá, Mundo!")


# Na primeira linha, iniciamos o comentário com `#` e, logo após, especificamos o que estamos fazendo na linha abaixo com `Meu primeiro comando em Python: um print`. Qualquer coisa após `#` será ignorado pelo Python.
# 
# Ao executarmos este comando, percebemos que é idêntico ao nosso primeiro print: não houve erros nem prints adicionais. Contudo, agora nosso código contém uma explicação do que estamos fazendo. Python ignora o comentário, **o qual é válido somente para sua linha**, e executa a próxima linha — o print.
# 
# Em Python, existem comentários multi-linhas, nos quais podemos escrever textos de algumas linhas e explicar algo com mais profundidade, se necessário. Veremos estes mais à frente.
