#!/usr/bin/env python
# coding: utf-8

# # Tipos Númericos

# Python reconhece três tipos numéricos: `int`, `float` e `complex`.
# 
# Números em Python podem ser criados por **constantes literais**, ou seja, valores inseridos pelo programador diretamente no 
# código. São constantes pois não podem ser mudadas de forma dinâmica: o programador tem de mudá-las manualmente, uma a uma. 
# Normalmente são chamadas apenas de **literais** e, neste caso, seriam **literais numéricas**, pois são números inseridos 
# diretamente no código-fonte.
# 
# Outra forma de se criar números em Python é através do retorno de funções e como resultado da utilização de operadores — ambos 
# casos que veremos adiante.

# (dados_inteiros)=
# ## Int

# São os números pertencentes ao conjunto dos números inteiros. Em outras palavras, os valores inteiros — tipo `int` — são os valores sem casas decimais.
# 
# Exemplos:
# 
# > 1    
# > 102547    
# > -50    
# > -215487
# 
# Podemos ver que os mesmos pertencem à classe `int`:

# In[1]:


print(type(1))


# In[2]:


print(type(2))


# In[3]:


print(type(-50))


# In[4]:


print(type(-100))


# ```{note}
# Os tipos booleanos (`True` e `False`) tecnicamente são um subtipo dos números inteiros, comportando-se como `1` e `0`. Podemos confirmar isto com a função `isinstance()`, com a qual verificamos se um objeto é do mesmo tipo que outro e retorna `True` se forem. Executando:
# 
#         isinstance(True, int)
#         isinstance(False, int)
# 
# Estamos querendo saber se `True` e `False` são do tipo `int`. Temos o resultado:
# 
#         True
#         True
#         
# Confirmando que de fato são considerados internamente como inteiros. Leia mais sobre isto nas [referências](dados-leia).
# ```

# ## Float

# São os números pertencentes ao conjunto dos números reais. De forma geral, são números com casas decimais. Todos números que conterem pontos e números decimais serão floats.
# 
# Exemplos:
# 
# > 12.02    
# > 10.0    
# > -567.5
# > -4.684321354
# 
# Números na forma de notação científica — escritos com a letra `e` — também são floats. Vemos isto no próximo exemplo, o qual demonstra a notação científica para o número 4000 $\left(4 \times 10^{3} \right)$:

# In[5]:


4e3


# Podemos ver que estes números pertencem à classe `float`:

# In[6]:


print(type(12.02))


# In[7]:


print(type(10.0))


# In[8]:


print(type(-567.5))


# In[9]:


print(type(-4.684321354))


# In[10]:


print(type(4e3))


# ## Complex

# São os números pertencentes ao conjunto dos números complexos. Números complexos possuem duas partes: imaginária e real. Matematicamente, são representados na forma:
# 
# $a+ib$
# 
# Onde:
# 
# - $a, b$ = números reais $\left(\mathbb{R} \right)$
# - $i$ = número imaginário, sendo seu valor: $\sqrt{-1}$
# 
# Geralmente, números imaginários são representados pela letra $i$ ou $j$. Em Python, adicionamos a letra $j$ (minúscula ou maiúscula) a uma literal numérica para criar um número imaginário e somamos a um número real para termos um número complexo. 
# 
# Exemplos:

# In[11]:


4 + 3j


# In[12]:


5.6 + 0J


# Outra forma de se criar números complexos é com a função `complex()`, na qual colocamos os valores real e imaginário, respectivamente:

# In[13]:


complex(4, 3)


# In[14]:


complex(5.6, 0)


# In[15]:


# O default do número imaginário é zero:
complex(5.6)


# Podemos ver que todos pertencem à classe `complex`:

# In[16]:


print(type(4 + 3j))


# In[17]:


print(type(5.6 + 0J))


# ## Valores Booleanos de Tipos Numéricos

# Qualquer número diferente de zero possuirá valor booleano verdadeiro:

# In[18]:


print(bool(2))


# In[19]:


print(bool(-14))


# In[20]:


print(bool(-5.689))


# In[21]:


print(bool(-0.01))


# In[22]:


print(bool(0.01))


# O número zero sempre será falso:

# In[23]:


print(bool(0))


# In[24]:


print(bool(0.0))

