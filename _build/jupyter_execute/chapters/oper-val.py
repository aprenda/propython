#!/usr/bin/env python
# coding: utf-8

# # Comparação de Valores

# A finalidade de todos os operadores desta seção é comparar **os valores** entre seus operandos.

# ## Operador `>`

# Retorna `True` se o operando da esquerda for maior que o operando da direita. Caso contrário, retorna `False`.

# In[1]:


print(1 > 2)


# In[2]:


print(-100 > -200)


# ## Operador `>=`

# Retorna `True` se o operando da esquerda for maior ou igual que o operando da direita. Caso contrário, retorna `False`.

# In[3]:


print(1 >= 2)


# In[4]:


print(2 >= 2)


# In[5]:


print(1 >= 1.0)


# In[6]:


print(-300 >= -200)


# ## Operador `<`

# Retorna `True` se o operando da esquerda for menor que o operando da direita. Caso contrário, retorna `False`.

# In[7]:


print(1 < 2)


# In[8]:


print(-100 < -200)


# ## Operador `<=`

# Retorna `True` se o operando da esquerda for menor ou igual que o operando da direita. Caso contrário, retorna `False`.

# In[9]:


print(4 <= 2)


# In[10]:


print(2 <= 2)


# In[11]:


print(1 <= 1.0)


# In[12]:


print(-100 <= -200)


# ## Operador `==`

# Retorna `True` se ambos operandos possuírem o mesmo valor. Caso contrário, retorna `False`.

# In[13]:


print(1 == 2)


# In[14]:


print(1 == 1)


# ## Operador `!=`

# Retorna `True` se ambos operandos possuírem valores diferentes. Caso contrário, retorna `False`.

# In[15]:


print(1 != 2)


# In[16]:


print(1 != 1)

