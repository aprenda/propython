#!/usr/bin/env python
# coding: utf-8

# # Zen of Python

# Desde o início de seu desenvolvimento, Guido van Rossum tinha alguns princípios sobre como o design de Python deveria ser feito. Após anos seguindo este conjunto de regras, Tim Peters — um dos desenvolvedores de Python — escreveu tais princípios em uma série de 20 aforismos e os intitulou *The Zen of Python*. É possível ver isto no próprio Python, com o código demonstrado abaixo. Você pode ler mais a fundo sobre a filosofia de Python no link da seção [](conhecendo5.md). 

# In[1]:


import this

