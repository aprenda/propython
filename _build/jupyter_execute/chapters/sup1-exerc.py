#!/usr/bin/env python
# coding: utf-8

# # Exercícios

# ```{admonition} **Tente Sozinho!**
# Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
# ```
# 
# ```{admonition} **Pesquise!**
# Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
# ```

# (sequencia_perg)=
# ## Questão 1

# Por que strings são objetos indexáveis?

# ```{admonition} Resposta
# :class: dropdown
# Em Python, tecnicamente, string é uma **sequência** — um tipo de objeto básico de Python. Sequências podem ser mutáveis ou imutáveis, mas todas são indexáveis. Então, se um objeto pode ser indexado, provavelmente ele é um tipo de sequência. Outros objetos que são sequências: listas, tuplas e range. Estes dois últimos ainda veremos adiante.
# ```

# ## Questão 2

# Vimos diversos métodos de strings. Este métodos são *in-place*? Por que?

# ```{admonition} Resposta
# :class: dropdown
# 
# Strings são objetos imutáveis. Seus métodos ou retornam um valor booleano ou retornam uma nova string. Nenhum modifica a string original.
# ```

# ## Questão 3

# Na string abaixo, troque todos os caracteres em maiúsculo para minúsculo e vice-versa.

# In[1]:


texto = "ESTA string TEM palavras MAIÚSCULAS e MINÚSCULAS"


# ```{admonition} Resposta
# :class: dropdown
# 
# O método de strings `swapcase()` faz exatamente esta função. Basta aplicá-lo à string:
# 
#         texto.swapcase()
# ```

# ## Questão 4

# A partir da string abaixo, crie uma lista onde cada palavra da string é um elemento separado da lista.

# In[2]:


texto = "Aprendendo strings"


# ```{admonition} Resposta
# :class: dropdown
# 
# O método de strings `split()` faz exatamente esta função. Basta aplicá-lo à string:
# 
#         texto.split()
# ```

# ## Questão 5

# A partir da string abaixo, crie uma lista onde cada número da string é um elemento separado da lista.

# In[3]:


texto = "0,1,2,3,4,5,6,7,8,9"


# ```{admonition} Resposta
# :class: dropdown
# 
# O método `split()` possui o argumento `sep` para podermos dividir a string de acordo com algum elemento desta. Como, na string, os elementos estão separados por vírgulas, vamos usar a vírgula como separador:
# 
#         texto.split(sep=",")
# ```

# ## Questão 6

# Crie uma variável com a string `"martelo"`, inverta a ordem de seus caracteres (escreva a palavra de trás para a frente) e guarde em uma variável chamada `inverso`. Por fim, printe `inverso`.

# ```{admonition} Resposta
# :class: dropdown
# 
#         var = "martelo"
# 
#         # Invertendo a string com sclicing
#         inverso = var[::-1]
# 
#         print(inverso)
# ```

# ## Questão 7

# Dada a string abaixo, verifique se as seguintes frutas: `"maçã"`, `"coco"`, `"caju"` e `"pêra"`.

# In[4]:


"uvalaranjatamarindococobananamorangocajumelancia"


# ```{admonition} Resposta
# :class: dropdown
# Vamos colocar a string em uma variável, para facilitar:
# 
#         texto = "uvalaranjatamarindococobananamorangocajumelancia"
# 
# Agora podemos ver com `in` ou `not in` se a string contém as frutas. Façamos com `in`:
# 
#         "maçã" in texto
#         "coco" in texto
#         "caju" in texto
#         "pêra" in texto
# ```

# ## Questão 8

# Usando a mesma string, descubra qual index das frutas: `"tamarindo"`, `"coco"`, `"caju"` e `"morango"`. Em cada caso, comprove com um print indexando a fruta em questão.

# In[5]:


"uvalaranjatamarindococobananamorangocajumelancia"


# ```{admonition} Resposta
# :class: dropdown
# Há dois métodos que podemos utilizar: `find()` e `index()`. Aqui, utilizaremos `index()`, pois este método retorna um erro se a substring não for achada. Colocando a string na variável:
# 
#         texto = "uvalaranjatamarindococobananamorangocajumelancia"
# 
# A primeira substring é `"tamarindo"`. Com `index()`, sabemos a posição inicial da fruta:
# 
#         print(texto.index("tamarindo"))
# 
# Que é o index `10`. Como `"tamarindo"` tem nove letras, e sendo a primeira letra no index `10`, então toda a palavra tem de ir do index `10` ao index `18`. Confirmando:
#         
#         print(texto[10:19])
# 
# De fato, conseguimos acessar a substring facilmente. O processo para as outras é idêntico.
# 
# - Coco:
# 
#         print(texto.index("coco")) # index 19-22
#         print(texto[19:23])
#         
# - Caju:
# 
#         print(texto.index("caju")) # index 36-39
#         print(texto[36:40])
#         
# - Morango:
# 
#         print(texto.index("morango")) # index 29-35
#         print(texto[29:36])
# ```

# ## Questão 9

# A partir da string abaixo, crie novas strings com as palavras: `"python"`, `"c++"` e `"java"`. Após criadas, coloque, em cada uma, sua primeira letra em maiúsculo.

# In[6]:


"j#pfyct1h6o+n4ca6r+1vh+6op1´~]v-6a6153+dfg+hcay"


# ```{admonition} Resposta
# :class: dropdown
# Para responder esta questão, basta fazermos um slicing da string de diferentes formas. A maior dificuldade é achar a primeira letra da palavra e o intervalo de slicing. Abaixo está cada um dos slicings.
# 
#         # Variável:
#         texto = "j#pfyct1h6o+n4ca6r+1vh+6op1´~]v-6a6153+dfg+hcay"
# 
#         # Python:
#         str1 = texto[2:13:2]
#         str1 = str1.capitalize()
#         print(str1)
# 
#         # C++:
#         str2 = texto[14:25:4]
#         str2 = str2.capitalize()
#         print(str2)
# 
#         # Java:
#         str3 = texto[::15]
#         str3 = str3.capitalize()
#         print(str3)
# ```

# ## Questão 10

# A string abaixo contém números de telefone. Transforme esta string em uma lista na qual cada elemento é um telefone diferente. Após criar a lista, transforme cada telefone no tipo numérico inteiro.

# In[7]:


"+55062745318955+55079284639751+55034672338925+55048469872216"


# ```{admonition} Resposta
# :class: dropdown
#         # Variável:
#         texto = "+55062745318955+55079284639751+55034672338925+55048469872216"
#         
#         # Separando:
#         telefones = texto.split("+55")
#         print(telefones)
# 
#         # Na separação, ocorre uma string vazia.
#         # Retirando a string vazia:
#         telefones.remove("")
#         print(telefones)
# 
#         # Transformando em número:
#         telefones[0] = int(telefones[0])
#         telefones[1] = int(telefones[1])
#         telefones[2] = int(telefones[2])
#         telefones[3] = int(telefones[3])
#         print(telefones)
# ```

# ## Questão 11

# Transforme a string abaixo em uma lista na qual cada elemento é um único número. Após criar a lista, transforme todos os elementos em números inteiros.

# In[8]:


"0123456789"


# ```{admonition} Resposta
# :class: dropdown
#         # Variável:
#         texto = "0123456789"
# 
#         # Concatenando a string "-" com a variável texto:
#         texto = "-".join(texto)
#         print(texto)
# 
#         # Transformando em lista:
#         lst = texto.split("-")
#         print(lst)
# 
#         # Transformando cada string em inteiro:
#         lst[0] = int(lst[0])
#         lst[1] = int(lst[1])
#         lst[2] = int(lst[2])
#         lst[3] = int(lst[3])
#         lst[4] = int(lst[4])
#         lst[5] = int(lst[5])
#         lst[6] = int(lst[6])
#         lst[7] = int(lst[7])
#         lst[8] = int(lst[8])
#         lst[9] = int(lst[9])
#         print(lst)
# ```
