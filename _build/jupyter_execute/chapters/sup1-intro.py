#!/usr/bin/env python
# coding: utf-8

# # Suplemento

# No capítulo sobre [listas](lista-intro), fomos introduzidos à primeira estrutura de dados em Python e, com ela, aos conceitos de mutabilidade, ordenação, indexação e métodos de objetos. Contudo, não só apenas as estruturas de dados possuem estas características. Veremos que as strings também são indexáveis e possuem métodos. Com estes conceitos, iremos revisitar alguns pontos já vistos e complementar nossos conhecimentos anteriores sobre strings.
# 
# ```{note}
# Os tipos de dados `int` e `float` também possuem métodos! Entretanto, seus métodos basicamente lidam com bits, bytes e suas representações. Por isto, não falaremos deles. Para nós, indexações e métodos de strings são muito mais utilizados e importantes.
# ```
# 
# Adicionalmente, veremos dois tipos novos de operadores. No capítulo sobre operadores, vimos os operadores de [comparação de valores](oper-val). Em Python, existem mais dois tipos de operadores de comparação: comparação de filiação e comparação de identidade. Aqui, depois de aprendermos um pouco sobre as estruturas de dados, é o local ideal para vermos estes operadores.
