#!/usr/bin/env python
# coding: utf-8

# # Operadores Aritméticos

# Os operadores aritméticos são os mais simples e provavelmente são aqueles com os quais você já está familiarizado da própria matemática, com exceção de alguns poucos.
# 
# ```{note}
# Vários operadores possuem mais de uma funcionalidade em Python. Nesta seção, apresentaremos apenas seu significado matemático. Os outros usos dos operadores serão debatidos em capítulos posteriores, à medida com que forem necessários.
# ```

# ## Operador `+`

# O operador `+` realiza a soma de seus operandos. Quando utilizado com tipos numéricos, realiza uma a operação de adição. Exemplos abaixo:

# In[1]:


print(1 + 2)


# In[2]:


print(1.056 + 2.56)


# In[3]:


print(3.14 + 6.02)


# In[4]:


print(806 + 987)


# ## Operador `-`

# O operador `-` realiza a diferença de seus operandos. Quando utilizado com tipos numéricos, realiza uma a operação de subtração. Exemplos abaixo:

# In[5]:


print(1 - 2)


# In[6]:


print(3.14 - 6.2)


# In[7]:


print(2.56 - 1.056)


# In[8]:


print(987 - 806)


# ### Unário

# O uso que acabamos de ver do operador `-` é do tipo binário, pois utiliza ambos operandos para realizar a subtração. Contudo, o operador `-` também possui um uso unário. O operador `-` unário realiza a negação de seu operando numérico.
# 
# ```{note}
# **Negação** (como em negação lógica) significa o oposto de uma operação ou sentença; seu contrário.
# ```
# 
# Por exemplo, com este operador, o número
# 
#         8
# 
# torna-se seu simétrico:
# 
#         -8
# 
# Outros exemplos:
# 
# - `256` e `-256`
# - `567.9` e `-567.9`
# - `1.02` e `-1.02`
# - `333` e `-333`

# ## Operador `*`

# O operador `*` realiza o produto de seus operandos. Quando utilizado com tipos numéricos, realiza uma a operação de multiplicação. Exemplos abaixo:

# In[9]:


print(1 * 2)


# In[10]:


print(3.14 * 6.2)


# In[11]:


print(2.56 * 1.056)


# In[12]:


print(987 * 806)


# ## Operador `**`

# O operador de potenciação `**` (*power operator*), quando utilizado com dois operandos, eleva o operando da esquerda à potência do operando da direita. Exemplos:

# In[13]:


# Dois ao cubo:
2**3


# In[14]:


# Três elevado a seis:
3**6


# Se tivermos potências inteiras negativas, o resultado será transformado em `float`, como em:

# In[15]:


10**-3


# O que é o correto, uma vez que:
# 
# $10^{-3}=\frac{1}{10^{3}}=\frac{1}{1000}=0.001$

# (oper_div)=
# ## Operador `/`

# O operador `/` realiza a divisão de seus operandos: a operação de divisão. Com este operador, o resultado sempre será um `float`, mesmo que o resultado da divisão seja inteira. Exemplos abaixo:

# In[16]:


10 / 5


# In[17]:


4.2 / 4


# In[18]:


6.14 / 1.6


# In[19]:


16 / 4


# Uma divisão por zero resulta em erro:

# In[20]:


5.2 / 0


# ## Operador `//`

# O operador `//` também realiza a divisão de seus operandos, mas faz uma **divisão de inteiros** (*floor division*). Uma divisão de inteiros retorna somente a parte inteira do resultado. Por isto, com este operador, o resultado será um `int` se a divisão for inteira e, caso contrário, será um `float` contendo somente a parte inteira. Exemplos abaixo:

# In[21]:


# resultado: int
10 // 5


# In[22]:


# resultado: float (somente parte inteira)
4.2 // 4


# In[23]:


# resultado: float (somente parte inteira)
6.14 // 1.6


# In[24]:


# resultado: int
16 // 4


# Uma divisão por zero também resulta em erro:

# In[25]:


5.2 // 0


# ## Operador `%`

# O operador `%` realiza a divisão entre seus operandos e retorna **o resto** da divisão. Por exemplo, seja $r$ o resto de uma divisão. Se fizer mos:
# 
# $\frac{4}{2} \longrightarrow r=0$
# 
# O resto é zero, pois é uma divisão exata. Já na divisão:
# 
# $\frac{4}{3} \longrightarrow r=1$
# 
# O resto é `1`, pois a divisão não é exata. Executando estas divisões em Python:

# In[26]:


# O resto da divisão 4/2:
4 % 2


# In[27]:


# O resto da divisão 4/3:
4 % 3


# ```{admonition} Importante!
# Com este operador, não estamos interessados no resultado divisão! Queremos saber apenas qual é o resto divisão.
# ```
