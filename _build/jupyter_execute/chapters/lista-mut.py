#!/usr/bin/env python
# coding: utf-8

# # Modificando Listas

# Como sabemos, uma das características de listas é a mutabilidade. Há diversas funções que podemos utilizar para modificar uma lista. Nesta seção, veremos que é possível modificar uma lista e exemplificaremos uma forma básica de tal. As diversas funções de modificação serão debatidas [em breve](lista-metodo).
# 
# Vejamos na prática como modificar uma lista após sua criação. Primeiro, criemos uma lista nova:

# In[1]:


numeros = [
    104.3, 99.5, 98.9, 102.9, 94.9,
    97.1, 98.3, 111, 222, 333
]

print(numeros)


# ## Indexação Normal

# Nossa lista `numeros` deveria ser uma lista de floats. Porém, ela contém floats e três números inteiros. Vamos arrumar esta lista para que todos seus elementos sejam do mesmo tipo.
# 
# Modificamos uma lista em Python ao indexarmos o(s) elemento(s) desejados e designarmos um novo valor, ou seja, **selecionamos uma valor  na lista e o substituímos**. Façamos passo-a-passo:

# In[2]:


# Substituindo o valor 2.71 no index 7:
numeros[7] = 2.71

# Valor modificado no index 7:
print(numeros)


# In[3]:


# Substituindo o valor 3.14 no index 8:
numeros[8] = 3.14

# Valor modificado no index 8:
print(numeros)


# In[4]:


# Substituindo o valor 6.02 no index 9:
numeros[9] = 6.02

# Valor modificado no index 9:
print(numeros)


# Modificamos nossa lista! Agora ela é composta por apenas floats. Contudo, podemos também modificar uma lista de outras formas, como, por exemplo, indexação negativa.

# ## Indexação Negativa

# A ideia de modificar um objeto com indexação negativa é a mesma da indexação normal. Vamos reiniciar nossa lista e modificá-la novamente:

# In[5]:


numeros = [
    104.3, 99.5, 98.9, 102.9, 94.9,
    97.1, 98.3, 111, 222, 333
]

print(numeros)


# In[6]:


# Substituindo o valor 6.02 no index -1:
numeros[-1] = 6.02

# Valor modificado no index -1:
print(numeros)


# In[7]:


# Substituindo o valor 6.02 no index -2:
numeros[-2] = 3.14

# Valor modificado no index -2:
print(numeros)


# In[8]:


# Substituindo o valor 6.02 no index -3:
numeros[-3] = 2.71

# Valor modificado no index -3:
print(numeros)


# ## Modificando com Slicing

# Vamos agora modificar vários elementos de uma única vez com um slicing da lista. Novamente, vamos reiniciar a lista:

# In[9]:


numeros = [
    104.3, 99.5, 98.9, 102.9, 94.9,
    97.1, 98.3, 111, 222, 333
]

print(numeros)


# A ideia sempre é a mesma: selecionar os elementos e modificá-los. Com slicing, a única diferença é que selecionamos vários elementos de uma única vez. Façamos: vamos selecionar os index `7:10` de uma única vez e modificar os elementos nestas posições:

# In[10]:


numeros[7:10] = [2.71, 3.14, 6.02]

print(numeros)


# ```{admonition} Questão
# Você consegue identificar qual é o resultado do seguinte código? Pense e responda esta [questão](numeros_perg).
# 
# ```python
# numeros[-1:-4:-1]
# ```
