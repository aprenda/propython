#!/usr/bin/env python
# coding: utf-8

# # Comparação de Filiação

# O intuito das comparações de filiação ou pertencimento (*membership tests*) é verificar se um dado objeto contém um elemento em específico. Estas comparações são realizadas com dois operadores: `in` e `not in`. Vejamos cada um destes agora.

# ## Operador `in`

# Utilizamos o operador `in` para verificar se um elemento faz parte de um objeto. Este operador retorna `True` se o elemento fizer parte do objeto e `False` se não fizer. Usamos o operador em uma expressão, como:

# In[1]:


elemento in objeto


# Onde:
# 
# - `elemento`: o elemento que queremos verificar.
# - `objeto`: o objeto que queremos saber se contém o elemento.
# 
# Vejamos alguns exemplos. Primeiro vamos criar um objeto para utilizarmos:

# In[2]:


doces = [
    "bolo", "chocolate", "leite condensado",
    "torta", "churros", "brigadeiro"
]

print(doces)


# Temos uma lista de strings com alguns doces. Usando o operador `in`, vamos conferir se esta lista contém certos doces:

# In[3]:


print("pudim" in doces)


# O resultado retornado pela expressão é `False`, pois a string `"pudim"` não encontra-se na lista `doces`. Vejamos com outro doce:

# In[4]:


print("brigadeiro" in doces)


# O resultado retornado é `True`, pois a string `"brigadeiro"` de fato encontra-se na lista `doces`. São operações bem simples.

# ## Operador `not in`

# Este operador é o contrário do anterior. Utilizamos o operador `not in` para verificar se um elemento **não** faz parte de um objeto. Este operador retorna `True` se o elemento não fizer parte do objeto e `False` se fizer. Este operador possui o valor lógico contrário do operador `in`.
# 
# Usamos o operador em uma expressão, como:

# In[5]:


elemento not in objeto


# Onde:
# 
# - `elemento`: o elemento que queremos verificar.
# - `objeto`: o objeto que queremos saber se contém o elemento.
# 
# Vejamos alguns exemplos. Continuando com nossa lista `doces`:

# In[6]:


print("pudim" not in doces)


# O resultado retornado pela expressão é `True`, pois a string `"pudim"` não encontra-se na lista `doces`. Vejamos com outro doce:

# In[7]:


print("brigadeiro" not in doces)


# O resultado retornado é `False`, pois a string `"brigadeiro"` de fato encontra-se na lista `doces`.
# 
# Como verificamos, o oeprador `not in` de fato é o valor contrário do operador `in`.
