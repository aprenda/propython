#!/usr/bin/env python
# coding: utf-8

# # Exercícios

# ```{admonition} **Tente Sozinho!**
# Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
# ```
# 
# ```{admonition} **Pesquise!**
# Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
# ```

# ## Questão 1

# Explique o que está ocorrendo na operação abaixo:

# In[1]:


"Duas" + "Strings"


# ```{admonition} Resposta
# :class: dropdown
# 
# Quandos os operandos do operador `+` são strings, este operador executa uma concatenação das strings. Por isto o resultado da operação é uma única string unindo ambas.
# ```

# ## Questão 2

# Explique o que está ocorrendo na operação abaixo:

# In[2]:


"a" * 3


# ```{admonition} Resposta
# :class: dropdown
# Quando o operador `*` é utilizado com uma string e um inteiro, a operação executada é repetição da string. Neste caso, estamos repetindo a string `"a"` três vezes.
# ```

# ## Questão 3

# As operações abaixo são feitas com tipos numéricos diferentes: `int` e `float`. Explique o que Python faz para que a operação seja possível.

# In[3]:


1 + 2.5


# In[4]:


1 - 2.5


# ```{admonition} Resposta
# :class: dropdown
# Quando há tipos numéricos diferentes em operadores aritméticos, Python faz uma conversão aritmética:
# 
# - Se algum dos operandos for um número complexo, o outro será convertido em complexo;
# - Se um dos operandos for `float`, o outro será convertido para `float`;
# - Se ambos operandos forem inteiros, não há conversão.
# ```

# ## Questão 4

# Explique por que o resultado do código abaixo é `152`.

# In[5]:


--152


# ```{admonition} Resposta
# :class: dropdown
# O operador sendo utilizado aqui é o `-` unário: ele realiza a negação de seu operando.
# 
# Primeiramente, temos o número `152`. Com o operador, temos seu simétrico: `-152`. Ao colocarmos outro `-` na frente de `-152`, estamos negando esta expressão, ou seja, voltando para `152`.
# ```

# ## Questão 5

# O que está sendo feito nas operações abaixo?

# In[6]:


-1**2


# In[7]:


(-1)**2


# In[8]:


-10**2


# ```{admonition} Resposta
# :class: dropdown
# Na operação:
# 
#         -1**2
# 
# O operador `**` tem maior precedência sobre o operador unário `-`. Assim, `1**2` ($1^{2}$) é executada, resultando em `1` e, após, o operador `-` é éxecutado, resultando em `-1`.
# 
# No caso de:
# 
#         (-1)**2
# 
# Temos uma expressão em parênteses, a qual será executada primeiro. Por isto, a ação executada é $-1^{2}$, resultando em `1`.
# 
# A última operação:
# 
#         -10**2
# 
# É o mesmo da primeira: avaliamos primeiro `10**2` ($10^{2}$), resultando em `100` e, após, o operador `-` é éxecutado, resultando em `-100`.
# ```

# (str_comp_perg)=
# ## Questão 6

# Avalie as expressões abaixo e explique-as.

# In[9]:


"python" > "Python"


# ```{admonition} Resposta
# :class: dropdown
# 
# A comparação de strings é feita através de seu número no código [Unicode](https://en.wikipedia.org/wiki/List_of_Unicode_characters#Basic_Latin). Você pode facilmente saber qual o código Unicode de uma string em Python com a função `ord()`. Neste caso, executaríamos:
# 
#         print(ord("p"))
#         print(ord("P"))
# 
# O que resultaria em:
# 
#         112
#         80
# 
# Como $112 \gt 80$, então `"p" > "P"`.
# ```

# In[10]:


"Python" > "python"


# ```{admonition} Resposta
# :class: dropdown
# 
# O contrário do caso anterior. Neste caso, executaríamos:
# 
#         print(ord("P"))
#         print(ord("p"))
# 
# O que resultaria em:
# 
#         80
#         112
# 
# Como $80 \lt 112$, então `"P" < "p"`.
# ```

# ## Questão 7

# Avalie cada expressão abaixo e explique seu resultado.

# In[11]:


"texto um" and "texto dois"


# ```{admonition} Resposta
# :class: dropdown
# Operador `and`. A resposta é `"texto dois"`, pois o operando esquerdo é verdadeiro, então o operando direito é avaliado e retornado.
# ```

# In[12]:


444 and 333


# ```{admonition} Resposta
# :class: dropdown
# Operador `and`. A resposta é `333`, pois o operando esquerdo é verdadeiro, então o operando direito é avaliado e retornado.
# ```

# In[13]:


(120 - 60 * 2) and (41 / 3)


# ```{admonition} Resposta
# :class: dropdown
# Operador `and`. A resposta é `0`, pois o operando esquerdo é zero, portanto, falso. Assim, este resultado é retornado e o operando direito não é avaliado.
# ```

# In[14]:


" " or "texto"


# ```{admonition} Resposta
# :class: dropdown
# Operador `or`. A resposta é `" "`, pois o operando esquerdo é verdadeiro, então este resultado é retornado e o operando direito não é avaliado.
# 
# > **Nota**: uma string com espaço em branco (*whitespace*) não é vazia, consequentemente, é verdadeira.
# ```

# In[15]:


0.0 or ""


# ```{admonition} Resposta
# :class: dropdown
# Operador `or`. A resposta é `""` (string vazia), pois o operando esquerdo é falso, então o operando direito é avaliado e retornado.
# ```

# In[16]:


(45 % 5) or (8**2 * 5.2)


# ```{admonition} Resposta
# :class: dropdown
# Operador `or`. A resposta é `332.8`, pois o operando esquerdo é zero (falso), então o operando direito é avaliado e retornado.
# ```

# In[17]:


not (1 < 2)


# ```{admonition} Resposta
# :class: dropdown
# A resposta é `False`, pois a expressão `1 < 2` tem precedência maior (expressão entre parênteses) e é avaliada, resultando em `True`. O operador `not` nega este resultado, transformando-o em `False`.
# ```

# In[18]:


not (1 > 3 and 5 == 5)


# ```{admonition} Resposta
# :class: dropdown
# A resposta é `True`. A expressão `(1 > 3 and 5 == 5)` é avaliada primeiro (maior precedência). Nesta expressão, o operando esquerdo é falso, então o operando direito não é avaliado e o resultado desta expressão é `False`. O operador `not` nega e transforma em `True`.
# ```

# In[19]:


(5.0 != 5) and (not (8 % 7))


# ```{admonition} Resposta
# :class: dropdown
# A resposta é `False`. Temos três expressões em parênteses, mas, de acordo com o operador `and`, temos duas expressões (uma em cada operando). O resultado do operando esquerdo é `False`, pois `5.0` é igual a `5`. Assim, este resultado é retornado e o operando direito não é avaliado.
# ```

# In[20]:


not --684


# ```{admonition} Resposta
# :class: dropdown
# A resposta é `False`. Aqui novamente temos um caso de negação da negação com o operador unário `-`. O resultado é `684`, que é verdadeiro. O operador `not` nega e transforma em `False`.
# ```

# In[21]:


not ((-8 * 4) or (True and False))


# ```{admonition} Resposta
# :class: dropdown
# A resposta é `False`. Pela precedência, executamos primeiro a expressão `(-8 * 4) or (True and False)`. Nesta, temos que `-8 * 4` é verdadeiro (`-32`), então seu resultado é retornado e o operando direito não é avaliado. Segue que `not -32` é `False`.
# ```

# In[22]:


(not (-8 * -4)) or (True or False)


# ```{admonition} Resposta
# :class: dropdown
# A resposta é `True`. O operando esquerdo é avaliado e resulta em `False`. Assim, o operando direito é avaliado e seu resultado é retornado.
# ```

# ## Questão 8

# Avalie as questões abaixo.
# 
# ```{note}
# 1. Comparações de valores com booleanos como feitos nestas questões não devem ser feitas. Este tipo de dado deve ser comparado de [outras formas](oper_ident). O propósito deste exercício é mostrar que é possível fazer e explicar o que está acontecendo.
# ```

# In[23]:


12 <= 56


# ```{admonition} Resposta
# :class: dropdown
# 12 é menor ou igual a 56, então `True`.
# ```

# In[24]:


2 > 2


# ```{admonition} Resposta
# :class: dropdown
# 2 não é maior que 2, então `False`.
# ```

# In[25]:


5.0 == 5


# ```{admonition} Resposta
# :class: dropdown
# Apesar dos tipos numéricos diferentes, 5 é igual a 5.
# ```

# In[26]:


-8 >= -9


# ```{admonition} Resposta
# :class: dropdown
# -8 é maior ou igual a -9, então `True`.
# ```

# In[27]:


"a" != "A"


# ```{admonition} Resposta
# :class: dropdown
# Pelos códigos Unicode, $97 \neq 65$, então o resultado é `True`. Veja a resposta da [questão anterior](str_comp_perg) para mais detalhes.
# ```

# In[28]:


1 == True


# ```{admonition} Resposta
# :class: dropdown
# O número `1` representa o valor booleano `True`. Assim, o resultado é `True`.
# 
# Tecnicamente, os tipos booleanos são inteiros. Assim, o número inteiro `1`, de fato, é o valor `True`. Releia a seção de [números inteiros](dados_inteiros) para mais referências.
# ```

# In[29]:


-684 == True


# ```{admonition} Resposta
# :class: dropdown
# Apesar do valor booleano de `-684` ser verdadeiro, este número não representa o valor booleano `True`. Assim, o resultado é `False`.
# ```

# In[30]:


True > 5


# ```{admonition} Resposta
# :class: dropdown
# Python não está convertendo o booleano `True` para um inteiro. Lembre-se que booleanos **são** inteiros. Assim, apenas o está usando como tal. Desta forma, $1 \gt 5$ é `False`.
# ```

# In[31]:


0 != False


# ```{admonition} Resposta
# :class: dropdown
# O número zero representa o valor booleano `False`. Por isto, o resultado é `False`, uma vez que é o operador de diferença.
# 
# Tecnicamente, os tipos booleanos são inteiros. Assim, o número inteiro `0`, de fato, é o valor `False`. Releia a seção de [números inteiros](dados_inteiros) para mais referências.
# ```

# In[32]:


resto = 10 % 3

resto == 0


# ```{admonition} Resposta
# :class: dropdown
# A expressão `10 % 3` resulta em `1` e é guardada na variável `resto`. Comparações com variáveis funcionam do mesmo modo, pois o valor da variável é seu elemento, neste caso, é `1`. Assim, `1 == 0` resulta em `False`.
# ```

# In[33]:


-8 + 26 < 2**2 > 16 // 3


# ```{admonition} Resposta
# :class: dropdown
# Pela precedência, a ordem de avaliação é:
# 
# 1. `2**2` (resultado: `4`)
# 1. `-8`
# 1. `16 // 3` (resultado: `5`)
# 1. `-8 + 26` (resultado: `18`)
# 
# O que torna a expressão em:
# 
#         18 < 4 > 5
# 
# Em Python, o encadeamento de expressões comparativas é feito com o operador `and`. Desta forma, nossa expressão acima ficará:
# 
#         18 < 4 and 4 < 5
# 
# Segue:
# 
# 1. `18 < 4` (resultado: `False`)
# 1. Como estamos lidando com o operador `and`, o resultado `False` é retornado e o operando direito não é avaliado.
# ```

# In[34]:


((12 % 4) <= (25 / 10**2)) == 8 != 8.0


# ```{admonition} Resposta
# :class: dropdown
# Pela precedência, a ordem de avaliação é:
# 
# 1. `12 % 4` (resultado: `0`)
# 1. `10**2` (resultado: `100`)
# 1. `25 / 100` (resultado: `0.25`)
# 
# O que torna a expressão em:
# 
#         (0 <= 0.25) == 8 != 8.0
# 
# Segue:
# 
# 1. `0 <= 0.25` (resultado: `True`)
# 
# O que torna a expressão em:
# 
#         True == 8 != 8.0
# 
# Segue do encadeamento:
# 
#         True == 8 and 8 != 8.0
# 
# Disto:
# 
# 1. `True == 8` (resultado: `False`)
# 1. Como estamos lidando com o operador `and`, o resultado `False` é retornado e o operando direito não é avaliado.
# ```

# ## Questão 9

# Na expressão abaixo, explique o motivo do resultado ser um `float`, uma vez que toda a operação é feito com `int`'s.

# In[35]:


8 + 2 * 10 / 2


# ```{admonition} Resposta
# :class: dropdown
# Os operadores com maior precedência são `*` e`/`. Assim, a operação é avaliada:
# 
# 1. `8 + 2 * 10 / 2`
# 1. `8 + 20 / 2`
# 1. `8 + 10.0`
# 1. `18.0`
# 
# No passo 2, a operação `20 / 2` resulta em um `float`, pois divisão em Python sempre retorna um `float`, como visto na seção do operador de [divisão](oper_div). A partir deste momento, Python converterá os outros inteiros para `float`, para que as operações sejam possíveis. Por isto, o resultado final é um `float`.
# ```

# ## Questão 10

# Qual é a precedência e associatividade da expressão abaixo?

# In[36]:


8 * 6 - 4**3


# ```{admonition} Resposta
# :class: dropdown
# A precedência é feita na seguinte ordem:
# 
# 1. `4**3` (resultado: `64`)
# 1. `8 * 6` (resultado: `48`)
# 1. `48 - 64`
# 1. `-16`
# 
# Com exceção do operador `**`, o qual avalia da direita para esquerda, todas as outras seguem da esquerda para a direita.
# ```
