#!/usr/bin/env python
# coding: utf-8

# # Acessando Elementos

# Tuplas são objetos ordenados. Portanto, todas operações de tais objetos aplicam-se à tuplas, como indexação e slicing.
# 
# > Tecnicamente, assim como listas e strings, tuplas são [sequências](sequencia_perg)!
# 
# A sintaxe de tais processos em tuplas é a mesma de listas, o que simplifica nosso processo de aprendizado. Como já os sabemos, podemos aplicá-los prontamente.
# 
# Vamos usar nossa tupla de meses do ano:

# In[1]:


meses = (
    "Janeiro", "Fevereiro", "Março", "Abril", "Maio",
    "Junho", "Julho", "Agosto", "Setembro", "Outubro",
    "Novembro", "Dezembro"
)


# ## Indexação

# Para acessar um elemento em específico, basta indexar tal elemento. Vamos acessar o mês de novembro (index `10` na tupla):

# In[2]:


print(meses[10])


# E fazemos isto para qualquer outro elemento:

# In[3]:


# Outubro:
print(meses[9])


# In[4]:


# setembro:
print(meses[8])


# In[5]:


# Maio:
print(meses[4])


# ## Slicing

# Para acessarmos vários elementos de uma única vez, fazemos o slicing normal. Vamos acessar os meses de setembro a novembro:

# In[6]:


print(meses[8:11])


# Percebamos um detalhe: em listas, quando fazíamos o slicing, era retornado uma lista com os elementos selecionados. Aqui, nos é retornado uma tupla.

# Obviamente, em slicing podemos usar os três argumentos. Vamos acessar os mês de fevereiro a outubro com um intervalo de `3`:

# In[7]:


meses[1:10:3]


# E agora acessaremos todos os meses, mas com um intervalo de `2`:

# In[8]:


meses[::2]


# E inverter todos os meses:

# In[9]:


print(meses[::-1])


# ## Modificação

# Vamos tentar modificar uma tupla: trocaremos `dezembro` pelo número `12`.

# In[10]:


meses[11] = 12


# Há um erro:
# 
# > TypeError: 'tuple' object does not support item assignment
# 
# O que significa que um objeto do tipo `tuple` não aceita designação de elementos, ou seja, não podemos modificar nenhum elemento de uma tupla.
# 
# E se tentássemos modificar um slicing?

# In[11]:


meses[8:11] = (9, 10, 11)


# O mesmo erro é retornado. Vamos tentar, agora, apagar um elemento da tupla com `del`:

# In[12]:


del meses[11]


# Há um erro:
# 
# > TypeError: 'tuple' object does not support item deletion
# 
# O que significa que um objeto do tipo `tuple` não aceita deleção dede elementos.
# 
# Não podemos modificar tuplas de forma alguma.
