#!/usr/bin/env python
# coding: utf-8

# # Precedência de Operadores

# Apredemos sobre dois novos tipos de comparadores. Vamos atualizar nossa lista de precedência.

# |Operador|Precedência|Obs|
# |:--|:--|:--|
# |`(expressão)`|Maior precedência|-|
# |`**`|-|-|
# |`-X`|-|Operador `-` unário|
# |`*`, `/`, `//`, `%`|-|-|
# |`+`, `-`|-|Operadores aritméticos `+` e `-` binários|
# |`in`, `not in`, `is`, `is not`, `<`, `<=`, `>`, `>=`, `!=`, `==`|-|Comparações possuem mesma precedência|
# |`not`|-|-|
# |`and`|-|-|
# |`or`|Menor precedência|-|
