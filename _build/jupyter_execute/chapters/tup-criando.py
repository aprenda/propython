#!/usr/bin/env python
# coding: utf-8

# # Criando Tuplas

# Há algumas formas de se criar tuplas:
# 
# 1. Sintaxe literal (*literal syntax*) com o operador `()`:
#       - `(elemento1, elemento2, elemento3, ...)`
# 1. Função construtora `tuple()`, na qual passamos um iterável:
#       - `tuple(iterável)`
# 1. Separando objetos por vírgulas:
#     - `elemento1, elemento2, elemento3, ...`
# 
# Como é perceptível, é bem similar às listas. Vamos criar algumas tuplas.

# ## Tuplas Vazias

# Primeiramente, criaremos tuplas vazias, ou seja, com nenhum elemento:

# In[1]:


# Criando uma tupla vazia com a sintaxe literal:
tup1 = ()

print(tup1)


# In[2]:


# Criando uma tupla vazia com a função construtora:
tup2 = tuple()

print(tup2)


# Nos referiremos a uma tupla genérica como `tup`.
# 
# Ambas tuplas foram criadas retornam somente o operador `()` sem elemento algum, significando que a tupla está vazia. Podemos verificar o tamanho das tuplas:

# In[3]:


# Tamanho da tupla 1:
print(len(tup1))


# In[4]:


# Tamanho da tupla 2:
print(len(tup2))


# E seus valores booleanos são `False`, pois são objetos vazios:

# In[5]:


print(bool(tup1))


# In[6]:


print(bool(tup2))


# ## Tuplas com Elementos

# ### Sintaxe `()`

# Tuplas podem conter diferentes tipos de elementos. Vamos criar uma tupla com a sintaxe literal:

# In[7]:


tup = (1.025, "string", True, 4)

print(tup)


# Os elementos desta tupla são: um `float`, uma string, um valor booleano e um `int`.
# 
# Verificando que estão de fato em um único objeto — a tupla:

# In[8]:


print(type(tup))


# Mas tuplas, assim como listas, são muito utilziadas para agrupar elementos homogêneos. Por exemplo, poderíamos criar uma tupla de strings contendo os meses do ano:

# In[9]:


meses = (
    "Janeiro", "Fevereiro", "Março", "Abril", "Maio",
    "Junho", "Julho", "Agosto", "Setembro", "Outubro",
    "Novembro", "Dezembro"
)

print(meses)


# Ou uma tupla de usuários de alguma rede social:

# In[10]:


# ID's de usuários:
tup_id = (
    "joaoaugusto", "maria_beltrao22",
    "pricardoso", "rlopes"
)

print(tup_id)


# Contudo, como são tuplas, estes elementos nunca mudarão, pois tuplas são imutáveis.

# ### Função `tuple()`

# Podemos criar a função `tuple()`. A diferença é que fornecemos um iterável para a função. Vamos criar a mesma tupla anterior com esta função.

# In[11]:


# ID's de usuários:
tup_id = tuple(
    ["joaoaugusto", "maria_beltrao22",
    "pricardoso", "rlopes"]
)

print(tup_id)


# Perceba que, agora, as id's dos usuários estão dentro de um iterável — uma lista. Mas podemos utilizar qualquer outro objeto iterável, como abaixo:

# In[12]:


# ID's de usuários:
tup_id = tuple(
    ("joaoaugusto", "maria_beltrao22",
    "pricardoso", "rlopes")
)

print(tup_id)


# As id's estão agora dentro dos operadores `()`. Sim, tuplas são objetos iteráveis também! Podemos usá-las como tal sempre que necessário.
# 
# E se não fornecemos os elementos em um iterável?

# In[13]:


# ID's de usuários:
tup_id = tuple(
    "joaoaugusto", "maria_beltrao22",
    "pricardoso", "rlopes"
)

print(tup_id)


# Um erro ocorre. Vejamos:
# 
# > TypeError: tuple expected at most 1 argument, got 4
# 
# O erro nos diz que fornecemos 4 argumentos, mas o tipo de objeto `tuple` só aceita um único argumento. Ou seja, temos de fornecer este único argumento: o iterável.

# (tup_virg)=
# ### Vírgulas

# Uma outra forma de se criar tuplas é simplesmente separando os elementos por vírgulas. Como em:

# In[14]:


# ID's de usuários:
tup_id = "joaoaugusto", "maria_beltrao22", "pricardoso", "rlopes"

print(tup_id)


# Isto se dá porque, tecnicamente, o que cria uma tupla são as vírgulas, não os parênteses.
# 
# Agora que sabemos que tal sintaxe cria uma tupla (e sabemos o que tuplas são), podemos voltar a falar sobre um ponto que tocamos na seção de [variáveis](mult-desig): *tuple unpacking*.

# (tup-pack)=
# ## *Tuple Packing* e *Unpacking*

# Quando falamos de *packing* e *unpacking*, significa que que temos uma designação de variáveis e, com esta, ou agrupamos vários valores em uma variável; ou distribuímos os elementos de uma tupla para várias outras variáveis.
# 
# Quando falamos de [múltiplas designações](mult-desig), criamos três variáveis de uma única vez:

# In[15]:


tres, variaveis, diferentes = 5, 10, 15

print(tres, variaveis, diferentes)


# O que está acontecendo de fato: primeiramente, o operando direito do operador de designação `=` constituí-se por três elementos separados por uma vírgula. Como acabamos de ver, esta sintaxe cria uma tupla. Podemos facilmente comprovar:

# In[16]:


numeros = 5, 10, 15

print(f"{numeros} é: {type(numeros)}")


# Quando fazemos este tipo de operação, onde o operando direito são elementos separados por vírgulas e o operando esquerdo é um nome de variável, esta operação é denominada **tuple packing**, pois todos os elementos do operando direito são agrupados (*packed*) em uma única tupla. É exatamente isto que fazemos acima para criar a variável `numeros`.
# 
# Por outro lado, no código:

# In[17]:


tres, variaveis, diferentes = 5, 10, 15


# O operando esquerdo do operador `=` constituí-se por três nomes diferentes. Com esta sintaxe, Python ligará os valores do operando direito, respectivamente, aos nomes do operando esquerdo.
# 
# Este mesmo código pode ser visto como:

# In[18]:


tres, variaveis, diferentes = (5, 10, 15)


# Então temos três valores no operando esquerdo e uma tupla no operando direito. Quando temos uma situação desta, chamamos de **tuple unpacking**, pois será feita uma distribuição ordenada dos elementos da tupla para os valores do operando esquerdo. Especificamente, na ordem:
# 
# - O elemento `5` será ligado ao nome `tres`.
# - O elemento `10` será ligado ao nome `variaveis`.
# - O elemento `15` será ligado ao nome `diferentes`.
# 
# E o resultado final são três variáveis criadas:

# In[19]:


print(tres)
print(variaveis)
print(diferentes)


# Então, tecnicamente, a múltipla designação de variáveis é uma junção de *packing* e *unpacking*.
