#!/usr/bin/env python
# coding: utf-8

# # Tipos de Dados

# Antes de aprendermos como criar variáveis, devemos conhecer quais tipos de dados são suportados em Python. Um tipo de dado especifica que tipo de valor a variável em questão possuirá, como, por exemplo: números inteiros, texto ou booleanos. Em outras palavras, os tipos de dados são as categorias de dados que Python reconhece e sabe como guardar/manipular.
# 
# Veremos aqui os tipos de dados fundamentais em Python — os chamados *built-in types* (pois são os tipos própios de Python) — e veremos as estruturas de dados implementadas em Python em um próximo capítulo.

# ## Booleanos
# 
# Valores booleanos são valores que possuem somente duas opções: `True` e `False`, que em também podem ser representados em programação e lógica como os números `1` e `0`, respectivamente. Chamam-se valores booleanos pois estes são resultantes da álgebra booleana, criada no século XIX pelo matemático [George Boole](refBoole).
# 
# Em Python, os valores booleanos são escritos em inglês e com a primeira letra maiúscula. Podemos ver o tipo de um objeto (sua classe) com a função `type()`:

# In[1]:


# Printando o valor booleano True:
print(True)


# In[2]:


# Printando o valor booleano False:
print(False)


# In[3]:


# Printando a qual tipo de objeto o valor True pertence:
print(type(True))


# In[4]:


# Printando a qual tipo de objeto o valor False pertence:
print(type(False))


# Qualquer outra forma de escrita é inválida e resultará em erro, pois Python reconhecerá qualquer outra forma de escrita como o nome de uma variável existente, e não, de uma palavra-chave imbuída em Python.
# 
# É exatamente isto que o erro `NameError` significa:

# In[5]:


print(true)


# In[8]:


print(TRUE)


# Podemos utilizar a função `bool()` para sabermos o valor booleano de um objeto em Python:

# In[9]:


# Valor booleano de True: 
bool(True)


# In[10]:


# Valor booleano do número 1: 
bool(1)


# In[11]:


# Valor booleano de False: 
bool(False)


# In[12]:


# Valor booleano do número zero:
bool(0)


# Em Python, o valor booleano de qualquer objeto é considerado verdadeiro a não ser que o objeto seja vazio ou seja implementado como falso. Veremos vários exemplos nas seções que seguem e quando chegarmos nas estruturas de dados.

# ## Tipos Númericos

# Python reconhece três tipos numéricos: `int`, `float` e `complex`.
# 
# Números em Python podem ser criados por **constantes literais**, ou seja, valores inseridos pelo programador diretamente no 
# código. São constantes pois não podem ser mudadas de forma dinâmica: o programador tem de mudá-las manualmente, uma a uma. 
# Normalmente são chamadas apenas de **literais** e, neste caso, seriam **literais numéricas**, pois são números inseridos 
# diretamente no código-fonte.
# 
# Outra forma de se criar números em Python é através do retorno de funções e como resultado da utilização de operadores — ambos 
# casos que veremos adiante.

# ### Int

# São os números pertencentes ao conjunto dos números inteiros. Em outras palavras, os valores inteiros — tipo `int` — são os valores sem casas decimais.
# 
# Exemplos:
# 
# > 1    
# > 102547    
# > -50    
# > -215487
# 
# Podemos ver que os mesmos pertencem à classe `int`:

# In[13]:


print(type(1))


# In[14]:


print(type(2))


# In[15]:


print(type(-50))


# In[16]:


print(type(-100))


# ```{admonition}Observação
# Os tipos booleanos (`True` e `False`) tecnicamente são um subtipo dos números inteiros, se comportando na maioria dos casos 
# como `1` e `0`. Leia mais sobre isto nas [referências](refSub).
# ```

# ### Float

# São os números pertencentes ao conjunto dos números reais. De forma geral, são números com casas decimais. Todos números que conterem pontos e números decimais serão floats.
# 
# Exemplos:
# 
# > 12.02    
# > 10.0    
# > -567.5
# > -4.684321354
# 
# Números na forma de notação científica — escritos com a letra `e` — também são floats. Vemos isto no próximo exemplo, o qual demonstra a notação científica para o número 4000 $\left(4 \times 10^{3} \right)$:

# In[27]:


4e3


# Podemos ver que estes números pertencem à classe `float`:

# In[22]:


print(type(12.02))


# In[23]:


print(type(10.0))


# In[24]:


print(type(-567.5))


# In[25]:


print(type(-4.684321354))


# In[26]:


print(type(4e3))


# ### Complex

# São os números pertencentes ao conjunto dos números complexos. Números complexos possuem duas partes: imaginária e real. Matematicamente, são representados na forma:
# 
# $a+ib$
# 
# Onde:
# 
# - $a, b$ = números reais $\left(\mathbb{R} \right)$
# - $i$ = número imaginário, sendo seu valor: $\sqrt{-1}$
# 
# Geralmente, números imaginários são representados pela letra $i$ ou $j$. Em Python, adicionamos a letra $j$ (minúscula ou maiúscula) a uma literal numérica para criar um número imaginário e somamos a um número real para termos um número complexo. 
# 
# Exemplos:

# In[28]:


4 + 3j


# In[29]:


5.6 + 0J


# Outra forma de se criar números complexos é com a função `complex()`, na qual colocamos os valores real e imaginário, respectivamente:

# In[30]:


complex(4, 3)


# In[31]:


complex(5.6, 0)


# In[32]:


# O default do número imaginário é zero:
complex(5.6)


# Podemos ver que todos pertencem à classe `complex`:

# In[33]:


print(type(4 + 3j))


# In[34]:


print(type(5.6 + 0J))


# ### Valores Booleanos de Tipos Numéricos

# Qualquer número diferente de zero possuirá valor booleano verdadeiro:

# In[35]:


print(bool(2))


# In[36]:


print(bool(-14))


# In[37]:


print(bool(-5.689))


# In[38]:


print(bool(-0.01))


# In[39]:


print(bool(0.01))


# O número zero sempre será falso:

# In[40]:


print(bool(0))


# In[41]:


print(bool(0.0))


# ## Strings

# Tecnicamente, uma string é uma sequência de caracteres, mas basicamente, strings são textos. Enquanto em outras linguagens há diferenciação entre caracteres únicos e strings, como em C++: 
# 
# ```{admonition}Texto em C++
# Caracteres: 'a', 'b', 'c', '1', '2', '3'
# 
# String: "Alfabeto", "0123456789"
# ```
# 
# Em Python, não há esta diferenciação. Tudo que estiver entre aspas será uma string: um objeto da classe `str`.
# 
# Strings podem ser declaradas com aspas duplas ou simples, indiferentemente:

# In[42]:


print('Eu sou uma string com aspas simples!')


# In[43]:


print("Eu sou uma string com aspas duplas!")


# Até mesmo por aspas triplas:

# In[51]:


print("""Eu sou uma string com aspas triplas!""")


# E todas pertencem à classe `str`:

# In[48]:


print(type('Eu sou uma string com aspas simples!'))


# In[49]:


print(type("Eu sou uma string com aspas duplas!"))


# In[50]:


print(type("""Eu sou uma string com aspas triplas!"""))


# Aspas triplas geralmente são utilizadas para [docstrings](#docstrings) e comentários em várias linhas. Abaixo um exemplo disto: fazemos um simples print de $5+5$, mas antes há um grande texto entre aspas triplas.

# In[54]:


"""
Esta
é
uma
string
em
várias
linhas
que
também
serve
como
comentário,
ou
seja,
não
será
executado
pelo
Python.
"""

print(5 + 5)


# Contudo, por questões de legibilidade, é bom evitar comentários em várias linhas.
# 
# A única coisa importante a se lembrar é consistência: durante todo seu programa, você deve usar um único modo de declaração de strings. Não misture no programa aspas duplas com únicas.
# 
# Utilize ambas na mesma string somente quando sua string tiver de possui-las.
# 
# Exemplificando:

# In[ ]:


print("Ciclano não ouviu Beltrano gritando: "PARE!"")


# In[ ]:


print('Ciclano não ouviu Beltrano gritando: 'PARE!'')


# Em ambas frases acima, há um erro de sintaxe: Python não entende que `"PARE!"` e `'PARE!'` ainda são parte da string. Ele pensa que a string acabou nas aspas anteriores à palavra e outra string iniciou-se após a palavra.
# 
# Para utilizarmos corretamente os dois tipos de aspas dentro de uma mesma string, há dois modos: i) usar um caracter de escape; ou ii) usar outro tipo de aspas.

# ### Caracter de Escape

# Caracteres de escape são iniciados com a barra invertida (`\`) e indicam ao computador que algo deve ser avaliado diferentemente do normal.
# 
# Já vimos um destes caracteres: a nova linha (`\n`). Quando utilizamos  barra invertida com a letra `n`, indicamos ao computador que naquele local do código deve ser posta uma quebra de linha. Sem a barra invertida (sem o escape), a letra `n` é apenas uma string.
# 
# Nossos caracteres de escape de interesse, quando se tratando de strings, são, principalmente, aspas duplas (`"`) e simples (`'`).
# 
# Ambas indicarão ao programa que as aspas antecedidas pela barra não são o término nem o início da string, mas sim, que fazem parte dela. Deste modo, o Python não irá terminar a avaliação da string erroneamente. Façamos o mesmo exemplo de antes com ambos tipos de strings:

# In[55]:


print("Ciclano não ouviu Beltrano gritando: \"PARE!\"")


# In[56]:


print('Ciclano não ouviu Beltrano gritando: \'PARE!\'')


# Agora não há mais erros.

# ### Alternando os Tipos de Aspas

# Um outro modo, mais simples e compreensível, de se fazer isto é alternar de aspas dentro da string. Assim, Python entenderá que a string não terminou.
# 
# Se utilizarmos aspas duplas na string, dentro dela devemos utilizar aspas simples:

# In[57]:


print("Ciclano não ouviu Beltrano gritando: 'PARE!'")


# E vice-versa:

# In[58]:


print('Ciclano não ouviu Beltrano gritando: "PARE!"')


# ### Valores Booleanos de Strings

# Qualquer string não-vazia possuirá valor booleano verdadeiro. Strings vazias são aquelas que não possuem nenhum caracter dentro das aspas. Podemos ver o tamanho de um objeto em Python com a função `len()`, que, no caso de strings, nos retorna a quantidade de caracteres da string:

# In[59]:


print(len("Python"))


# In[60]:


print(len(""))


# Acima, a string `"Python"` possui 6 caracteres, por isso não é vazia.
# 
# Diferentemente, a segunda string possui zero caracteres, sendo uma string vazia. Vejamos seus valores booleanos:

# In[ ]:


print(bool("Python"))
 
print(bool(""))


# Strings vazias sempre serão falsas. Importante ressaltar: espaços em branco (*whitespaces*) são um caracter, por isso, strings com estes espaços não são vazias:

# In[62]:


print(type(" "))


# In[63]:


print(bool(" "))


# ## Conversão de Tipos

# Em Python há conversão explícita de dados: convertemos dados através de funções específicas para cada tipo.
# 
# No capítulo passado vimos uma conversão implícita dos dados para strings em [f-strings](#variaveis).

# ### Conversão para Inteiros

# Função de conversão para inteiros: `int()`

# In[64]:


# Convertendo float para int:
int(12.56)


# ### Conversão para float

# Função de conversão para float: `float()`

# In[65]:


# Convertendo int para float:
float(12)


# ### Conversão para Booleano

# Já estamos utilizando esta função desde o início do capítulo. Esta é: `bool()`

# In[66]:


# Convertendo int para bool:
bool(12)


# ### Conversão para strings

# Função de conversão para str: `str()`

# In[67]:


# Convertendo int para str:
str(12)


# ### Conversão entre Dados

# O tipo de dado tem de ser coercível para o outro tipo desejado. Podemos converter todos números para strings, por exemplo:

# In[68]:


# Convertendo números para string:
str(12)


# In[69]:


# Convertendo números para string:
str(-52.0006)


# In[70]:


# Convertendo strings para números:
int("12")


# In[71]:


# Convertendo strings para números:
float("12")


# Mas nem todas strings são coercíveis para números:

# In[72]:


# Tentando converter esta string para número:
int("Texto não é conversível para númerais.")

# Tentando converter esta string para número:
float("Texto não é conversível para númerais.")


# ## Leia Mais

# (refBoole)=
# ### George Boole

# - Site comemorativo do bicentenário do matemático, com diversas informações sobre suas obras:
#     - [George Boole 200](https://www.georgeboole.com)

# (refSub)=
# ### Relação Entre Booleanos e Inteiros

# - [PEP 285](https://www.python.org/dev/peps/pep-0285/)
# 
# - [Documentação Oficial](https://docs.python.org/3.9/library/stdtypes.html#numeric-types-int-float-complex)
