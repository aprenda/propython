#!/usr/bin/env python
# coding: utf-8

# # Slicing

# *Slicing* é um anglicismo que refere-se ao ato de cortar/separar/dividir os elementos de um objeto e retornar um agrupamento específico destes elementos.
# 
# A sintaxe de slicing é a mesma de slicing de strings: com o operador `[]`:

# In[1]:


nome_lista[inicio:fim:passo]


# Onde os parâmetros são:
# 
# - `inicio`: index por onde começar; incluso no agrupamento final.
# - `fim`: index por onde terminar; excluído do agrupamento final.
# - `passo`: intervalo de agrupamento.
# 
# O operador `:` é fundamental na sintaxe. Se não o colocarmos, Python entenderá que queremos acessar elementos individuais, como na seção anterior.
# 
# Se colocarmos somente o operador `:` significa que acessamos a lista inteira. Vamos criar uma lista numérica para exemplificar:

# In[33]:


numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# Agora façamos o slicing desta lista utilizando somente o operador `:`.

# In[35]:


print(numeros[:])


# Como visto, é o mesmo que:

# In[37]:


print(numeros)


# É interessante notar uma característica que já falamos no capítulo sobre variáveis: os locais da memória.
# 
# Com esta sintaxe de slicing total vista logo acima (acessando toda a lista), podemos nos referir a todos os objetos da lista e criar uma cópia independente. Criemos uma nova variável com esta sintaxe:

# In[39]:


copia_numeros = numeros[:]


# Agora vejamos ambas simultaneamente:

# In[41]:


print(numeros)
print(copia_numeros)


# Verifiquemos ambas. Primeiro, uma comparação de valores:

# In[44]:


numeros == copia_numeros


# Python nos retornou o valor `True`, uma vez que os elementos de ambas listas são, de fato, iguais. Agora façamos uma comparação de identidade:

# In[45]:


numeros is copia_numeros


# Nesta comparação nos é retornado o valor `False`, pois as duas listas não são a mesma: elas estão em locais diferentes da memória, portanto são variáveis independentes. Se mudarmos uma destas, a outra não será afetada:

# In[46]:


# Originais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[48]:


# Modificando a lista original:
numeros[0] = -1000


# In[49]:


# Listas finais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# Lembrando: quando utilizamos uma designação normal, estamos criando duas referências para o mesmo local da memória:

# In[50]:


copia_numeros = numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# Comparemos:

# In[51]:


print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[52]:


numeros == copia_numeros


# In[53]:


numeros is copia_numeros


# Agora, ambas listas estão no mesmo local da memória do computador; são referências para este mesmo local. Se mudarmos uma, a outra será modificada:

# In[46]:


# Originais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[54]:


# Modificando a lista original:
numeros[0] = -1000


# In[55]:


# Listas finais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# Parênteses fechado, voltemos ao slicing, com nossa lista original.

# In[57]:


numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# Utilizando o primeiro parâmetro — `inicio`:

# In[58]:


# Acessando todos os elementos até o fim da lista, iniciando pelo index 0:

print(numeros[0:])


# In[59]:


# Acessando todos os elementos até o fim da lista, iniciando pelo index 4:

print(numeros[4:])


# In[61]:


# Acessando todos os elementos até o fim da lista, iniciando pelo index 8:

print(numeros[8:])


# Se fizermos um slicing de um index inexistente, teremos uma lista vazia:

# In[62]:


print(numeros[10:])


# O slice começa no index 10, mas este não existe. Portanto, uma lsita vazia é retornada.

# Assim como indexação, podemos fazer o slicing com index negativos e dividir a lista iniciando pelos últimos elementos:

# In[79]:


# Inicia no index -1 e pega todos elementos até o fim da lista:
print(numeros[-1:])


# In[80]:


# Inicia no index -5 e pega todos elementos até o fim da lista:
print(numeros[-5:])


# In[83]:


# Inicia no index -8 e pega todos elementos até o fim da lista:
print(numeros[-8:])


# In[84]:


# Inicia no index -10 e pega todos elementos até o fim da lista:
print(numeros[-10:])


# Adicionando o segundo parâmetro — `fim`: com este, indicamos até qual index queremos acessar. Este parâmetro funciona de forma exclusiva, ou seja, o index em si não é incluído no slicing. O slicing retornado inclui somente até o index anterior. Desta forma, se fazemos um slicing com o index final de `6`, nos será retornado todos os elementos incluindo até o index `5`.
# 
# Vejamos exemplos:

# In[85]:


# Inicia no index 0 e termina no index 1:
print(numeros[:2])


# In[86]:


# Inicia no index 0 e termina no index 5:
print(numeros[:6])


# In[87]:


# Inicia no index 0 e termina no index 9:
print(numeros[:10])


# E com índices negativos (também de forma exclusiva):
# 
# ```{admonition}Observação
# Perceba que, já que estamos utilizando índices negativos, aumentamos o index ao contabilizar qual é o elemento anterior.
# ```

# In[90]:


# Inicia no index 0 e termina no index -2:
print(numeros[:-1])


# In[100]:


# Inicia no index 0 e termina no index -7:
print(numeros[:-6])


# Podemos, logicamente, utilizar ambos parâmetros conjuntamente:

# In[102]:


# Slicing iniciando do index 1 até o index 6:

print(numeros[1:7])


# In[103]:


# Slicing iniciando do index 3 até o index 8:

print(numeros[3:9])


# In[104]:


# Slicing iniciando do index 1 até o index -5:

print(numeros[1:-4])


# Por fim, o terceiro parâmetro: `passo`. Neste, lidamos com o intervalo do slicing; quantos índices pular ao acessar os elementos selecionados.
# 
# ```{admonition}Observação
# Temos de utilizar o operador `::` para simbolizar que estamos nos referindo ao último parâmetro. Nesta forma, pegaremos todos os elementos da lista, por default.
# ```

# In[115]:


# Acessando todos os elementos da lista, com intervalo de um índice:

print(numeros[::1])


# In[118]:


# Acessando todos os elementos da lista, com intervalo de dois índices:

print(numeros[::2])


# Podemos utilizar intervalos negativos também. Neste caso, a lista será invertida e o intervalo especificado será aplicado:

# In[117]:


# Acessando todos os elementos da lista, com intervalo de dois índices negativos:

print(numeros[::-2])


# Finalmente, podemos utilizar todos os parâmetros juntos:

# In[120]:


# Iniciando no index 2, terminando no index 8, com intervalo de dois índices:

print(numeros[2:9:2])


# In[122]:


# Iniciando no index 2, terminando no index 8, com intervalo de três índices:

print(numeros[2:9:3])


# Notemos neste último exemplo acima que ainda há um último elemento na lista: o elemento `900` no index `9`, mas este não é incluído, pois o próximo número que seria incluído nesta sequẽncia em particular seria o número `1100` no index `11`. Contudo, este elemento não existe nesta lista.
