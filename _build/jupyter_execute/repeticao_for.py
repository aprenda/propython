#!/usr/bin/env python
# coding: utf-8

# # For Loop

# A estrutura `for` é uma implementação extremamente comum de loops controlados por contagem, sendo esta a palavra-chave em Python para realizar este tipo de loop.
# 
# Antes de entrar propriamente em Python, vejamos esta estrutura com dois exemplos em outra linguagem: C++.

# ## Dois Exemplos de `for` Loop

# ### Exemplo 1

# In[1]:


for(int var {1}; var <= 6; ++var)
    std::cout << var << std::end;


# Neste exemplo, ocorre o seguinte:
# 
# - `int var {1}`: criamos uma variável chamada `var` contendo o valor inteiro `1`.
# - `var <= 6`: designamos que o loop terminará no valor `6`.
# - `++var`: somamos `1` ao valor da variável `var` a cada iteração feita.
# - `std::cout << var << std::endl`: printamos a variável `var`.

# Vemos então que há três partes essenciais: i) uma inicialização da variável `var`; ii) uma condição de término do loop, no caso, o loop parará em `6`; e iii) atualização da variável `var` a cada iteração.
# 
# O resultado deste loop é:

# In[ ]:


1
2
3
4
5
6


# ### Exemplo 2

# In[ ]:


std::string nome {"Python"};
   
for(int var {0}; var < nome.size(); ++var)
    std::cout << nome[var] << std::endl;


# Este exemplo é similar ao anterior, com exceção de:
# 
# - `std::string nome {"Python"};`: criamos uma string chamada `nome` contendo `Python`.
# - `int var {0}`: a variável `var` tem o valor inicial de zero.
# - `var < nome.size()`: a quantidade de iterações será igual ao tamanho da string `nome`.
# - `std::cout << nome[var] << std::endl`: a cada iteração, printamos uma letra da string `nome`.

# Aqui, também temos as três partes, mas a grande diferença é que, ao invés de printarmos a sequência numérica em si, estamos utilizando esta sequência como índices para acessarmos e printarmos cada elemento da string `nome`.
# 
# O resultado é:

# In[ ]:


P
y
t
h
o
n


# ### Sintaxe

# De uma forma geral, poderíamos dizer que o `for` em C++ possui a seguinte sintaxe:

# In[ ]:


for(inicialização; condição_de_término; atualização)
    código_do_loop


# E funciona na seguinte ordem:
# 
# 1. Primeiramente, a inicialização é feita e uma variável é criada.
# 2. A cada iteração, a condição é avaliada e, se seu valor booleano for verdadeiro, o `código_do_loop` é executado. Se for falso, o loop é finalizado.
# 3. Após o `código_do_loop` ser executado, a `atualização` da variável é feita e a execução retorna para avaliação em `condição_de_término` novamente.

# ```{admonition}Observação
# C++ possui uma estrutura específica para iterações ao longo de elementos de um objeto, os chamados *range-based for-loop* (ou for-each loops), mas aqui o interesse era demonstrar a estrutura mais básica para comparação.
# ```

# ## Python `for` Loop

# Os `for` loops em Python funcionam de forma diferente: não temos uma estrutura com três partes, pois em Python não especificamos uma condição de término do loop explicitamente ou especificamos como será a atualização da variável em questão. Em Python, 
# 
# Sua sintaxe é a seguinte:

# In[ ]:


for nome_variavel in iteravel:
    codigo


# Onde:
# 
# - `iteravel`: qualquer objeto iterável.
# - `nome_variavel`: variável que guarda o valor da iteração atual. Esta variável pode ser nomeada da forma que bem desejar.
# - `codigo`: todo o código que deseja repetir a cada iteração.
# 
# Como `for` funciona:
# 
# 1. O `for` será iniciado na criação da variável `nome_variavel`.
# 2. e executará todo o código designado ao loop. Ao chegar ao final do código, Python
# avaliará se 'var' chegou ao último valor de 'iteravel'. Se chegou,
# o loop será terminado, se não, Python voltará à primeira linha do
# código dentro do loop e executará tudo novamente.
# 
# Importante:
# Um lembrete importante é a identação em Python: como não há chaves
# ou pontos-e-vírgulas em Python, todo o código é determinado pela
# identação. Assim, todo o código identado dentro do 'for', por exemplo,
# pertencerá ao código de 'for'.
# 
# 
# Façamos um exemplo de 'for' e resolvamos nosso problema inicial: uma
# sequência de 1 a 10:
# 
# for x in range(11):
#     print(x)
# 
# Temos como iterável um 'range' de no intervalo [0, 10] e nomeamos
# nossa variável 'x'. Aqui, sabemos a priori quantas iterações serão 
# feitas pois temos um intervalo definido: os números de 0 a 10, ou seja,
# onze números (onze iterações). Em cada iteração, 'x' guardará o valor
# da iteração atual, começando pelo primeiro valor do objeto iterável,
# neste caso, 'range(11)'. Portanto:
# 
# Iteração 1: x = 0
# Iteração 2: x = 1
# Iteração 3: x = 2
# Iteração 4: x = 3
# Iteração 5: x = 4
# Iteração 6: x = 5
# Iteração 7: x = 6
# Iteração 8: x = 7
# Iteração 9: x = 8
# Iteração 10: x = 9
# Iteração 11: x = 10
# 
# Em cada iteração, será repetido todo o código dentro do 'for'. A única
# coisa que pedimos foi para fazer um print do valor atual de 'x'. Por
# isto, em cada iteração, faremos um print de 'x' e, ao fim, teremos 
# toda a sequência de números.
# 
# Com 'for' ficou muito mais simples e automático de se alterar o código
# e obtermos outras sequências:
# 
# for y in range(101): Uma sequência de 0 a 100
#     print(y)
# 
# for nome_qualquer in range(100, -101, -1): Uma sequência de 100 a -100
#     print(nome_qualquer)
# 
# Como vimos, podemos nomear a variável da forma que quisermos. Ela é
# somente uma variável para guardar o valor atual da iteração. Ela
# ainda existirá após o fim do loop:
# 
# print(nome_qualquer)
# 
# Como o último valor do loop foi '-100', a variável 'nome_qualquer'
# tem o valor de '-100'
# 
# Uma certa convenção existe quando queremos representar uma variável
# que não será utilizada em loops: utilizamos o underscore.
# 
# Exemplo: queremos somente printar uma sequência de números e não
# utilizaremos o valor da iteração atual em nenhum momento, então não
# necessitamos necessariamente nomear uma variável para o loop, pois
# não a utilizaremos. Deste modo, usamos um underscore como nome da
# variável para dizer apenas que faremos um loop, sem usar os valores
# do iterável em questão.
# 
# for _ in range(10):
#     print("Printando esta mensagem 10 vezes.")
# 
# Todos que verem este código deverão entender a convenção de que, ao
# usar o underscore como variável, o loop será feito um determinado
# número de vezes (10, no caso) e só. Mesmo assim, esta variável ainda
# terá o último valor:
# 
# print(_)
# 
# 
# Exemplifiquemos agora o 'for' com outros tipos de dados:
# 
# for x in "exemplo":
#     print(x)
# 
# Como sabemos, uma string é um objeto iterável, de modo que podemos
# utilizá-la. No exemplo acima, a cada iteração, 'x' terá o valor de
# cada letra e printará este valor.
# 
# vogais = ["a", "e", "i", "o", "u"]
# 
# for x in vogais:
#     print(x)
# 
# Acima, criamos uma lista de strings com os valores das vogais e usamos
# esta lista como iterável no loop. Objetivamente, é o mesmo que:
# 
# for x in "aeiou":
#     print(x)
# 
# Mas listas podem ter diferentes tipos de dados:
# 
# lista = ["Z", "", True, 1.54, 0, False]
# 
# for x in lista:
#     print(bool(x))
# 
# Acima, temos uma lista com diferentes tipos de dados e pedimos dentro
# do loop para printar o valor booleano de cada um destes.
# 
# 
# Outros exemplos de 'for':
# 
# for x in range(1, 1001):
#     print(f"x: {x}, x**2: {x**2}, x**3: {x**3}")
# 
# for letra in "café":
#     print(letra * 10)
# 
# for num in range(1, 11):
#     print("*" * num)
