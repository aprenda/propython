#!/usr/bin/env python
# coding: utf-8

# # Listas Aninhadas

# Uma vez que listas aceitam qualquer tipo de estrutura em Python, podemos ter então listas dentro de listas — as chamadas **listas aninhadas**.
# 
# Vamos criar algumas:

# In[1]:


aninhado = [
    [0, 1, 2, 3], # Lista 1
    [True, False], # Lista 2
    ["str1", "str2", "str3"], # Lista 3
]

print(aninhado)


# Na lista `aninhado` encontram-se quatro outras listas, cada uma contendo seus próprios dados.
# 
# Como podemos acessar estes elementos? Da mesma forma que acessamos listas simples: com o operador '[]' e utilizando os índeces dos elementos desejados.
# 
# Primeiramente vamos acessar a primeira lista aninhada — a lista de números inteiros. Esta lista está no index zero:

# In[2]:


print(aninhado[0])


# Acessamos a lista corretamente. Podemos acessar todas as outras listas aninhadas da mesma forma:

# In[3]:


# Acessando a segunda lista aninhada - booleanos (index 1):
print(aninhado[1])


# In[4]:


# Acessando a terceira lista aninhada - strings (index 2):
print(aninhado[2])


# Podemos acessar os elementos próprios de cada lista aninhada adicionando mais um operador '[]' para indicar o aninhamento de index.
# 
# Vamos acessar os elementos da lista de inteiros:

# In[5]:


# Primeiro elemento da lista de inteiros:
print(aninhado[0][0])


# In[6]:


# Segundo elemento da lista de inteiros:
print(aninhado[0][1])


# In[7]:


# Terceiro elemento da lista de inteiros:
print(aninhado[0][2])


# In[8]:


# Quarto elemento da lista de inteiros:
print(aninhado[0][3])


# Da mesma forma, acessamos os elementos das outras listas aninhadas:

# In[9]:


# Acessando os elementos da lista de booleanos:

print(aninhado[1][0])
print(aninhado[1][1])


# In[10]:


# Acessando os elementos da lista de strings:

print(aninhado[2][0])
print(aninhado[2][1])
print(aninhado[2][2])

