#!/usr/bin/env python
# coding: utf-8

# # Listas

# Listas são uma das estruturas de dados fundamentais de Python. Definindo especificamente: são **estruturas que agrupam um conjunto de diferentes elementos em um único objeto ordenado**.
# 
# Podemos criar listas através de:
# 
# 1. Sintaxe literal (*literal syntax*) com o operador `[]`:
#       - `[elemento1, elemento2, elemento3, ...]`
# 2. Função construtora `list()`, na qual passamos um iterável:
#       - `list(iteravel)`
# 
# Primeiramente, criemos duas listas vazias, ou seja, com zero elementos:

# In[1]:


# Criando uma lista vazia com a sintaxe literal:
lst1 = []

print(lst1)


# In[2]:


# Criando uma lista vazia com a função construtora:
lst2 = list()

print(lst2)


# É de praxe nos referirmos a uma lista com o nome `lst`, por isto o fizemos acima e o faremos no restante do livro.
# 
# Ambas listas foram criadas e no print nos é retornado somente o operador `[]` sem elemento algum, significando que a lista está vazia.
# 
# Podemos verificar o tamanho das listas:

# In[3]:


# Tamanho da lista 1:
print(len(lst1))


# In[4]:


# Tamanho da lista 2:
print(len(lst2))


# Como já vimos, todos objetos vazios possuem valor booleano falso e listas não são exceções:

# In[5]:


print(bool(lst1))


# In[6]:


print(bool(lst2))


# Listas aceitam diferentes tipos de objetos:

# In[7]:


lst3 = [1, "string", False, ("isto", "é", "uma", "tupla")]

print(lst3)


# Temos como elementos na lista acima um número inteiro, uma string, um valor booleano e uma tupla.
# 
# Verificando que estão de fato em um único objeto — a lista:

# In[8]:


print(type(lst3))


# Listas são úteis para agrupar elementos independentes em um único lugar, mas se quisermos agrupar elementos que possuem alguma relação, provavelemente uma lista não é a melhor estrutura de dados.
# 
# Observe:

# In[9]:


lst_individuo = [
    "Nome", "Zander",
    "Idade", 28,
    "CNH", False
]

print(lst_individuo)


# Esta lista acima não faz sentido algum se quiséssemos utilizar estes  dados. O que estes dados são? O que representam? A melhor estrutura de dados neste caso seriam dicionários — uma estrutura de dados ideal para mapeamentos, a qual veremos no próximo capítulo.

# Uma outra característica extremamente importante de listas é que elas são estrutura mutáveis, ou seja, após a criação de uma lista, é  possível modificá-la.
# 
# Vejamos:

# In[10]:


# Lista original:
print(lst_individuo)


# In[11]:


# Modificando:
lst_individuo[1] = 38


# In[12]:


# Lista final:
print(lst_individuo)


# ## Indexação de Listas

# ### Indexação Normal

# Um índice (*index*) de um objeto é a posição de um dado elemento guardado neste objeto. Em outras palavras: **índices são as posições dos elementos no objeto**. Referir-se à estas posições como *index* também é extremamente comum, então utilizaremos ambas formas.
# 
# Assim como é de praxe em programação, todos os índices de um objeto em Python começam em zero. Por causa disto, a contagem dos seus elementos encontra-se no intervalo $[0, n-1]$, ou seja, se uma dada lista possuir 10 elementos, o index do primeiro elemento será zero e o index do último será 9.
# 
# ```{admonition}Importante
# Uma grande propriedade de listas é que todos seus elementos são ordenados. Uma lista, ao ser criada, guardará cada elemento na ordem na qual o programador os colocou. Esta é a ordenação de uma lista, que definirá os índices de seus elementos.
# ```
# 
# Quando acessamos um único ou vários elementos de um objeto através de suas posições, diz-se que estamos **indexando** este objeto. Acessamos os elementos específicos de uma lista com o operador `[]`, da seguinte forma:

# In[13]:


nome_da_lista[index_do_elemento]


# Exemplo:

# In[13]:


# Criando uma lista nova:
compras = ["Arroz", "Feijão", "Leite", "Carne"]


# In[20]:


# Acessando o primeiro elemento (index zero):
print(compras[0])


# In[21]:


# Acessando o segundo elemento (index 1):
print(compras[1])


# In[22]:


# Acessando o terceiro elemento (index dois):
print(compras[2])


# In[23]:


# Acessando o quarto elemento (index três):
print(compras[3])


# Se tentarmos acessar um index não existente, um erro será retornado:

# In[24]:


# Acessando o quinto elemento (index 4 — inexistente):
print(compras[4])


# O erro acima diz que há um erro de index (*IndexError*): Python tentou acessar o elemento no index especificado mas a lista em questão não possui tal posição. Assim, o index requisitado está fora do intervalo de elementos existentes (*list index out of range*).
# 
# Por isto fique atento: o último elemento de uma lista é a quantidade de elementos menos 1: $\left( n - 1 \right)$.

# Podemos também criar novas variáveis com estes elementos indexados:

# In[25]:


# Criando uma nova variável chamada 'var':
var = compras[3]

print(var)


# Podemos verificar que não é uma lista, mas sim, uma string, pois estamos acessando este único elemento em si:

# In[26]:


print(type(var))


# E não há alteração alguma na lista original:

# In[27]:


print(compras)


# ### Indexação Negativa

# Outra forma de acessarmos os elementos de uma lista é através de índices negativos:

# In[ ]:


nome_da_lista[-index_do_elemento]


# Aqui, os elementos serão acessados em sua ordem inversa: o primeiro index acessará o último elemento da lista e o último index acessará o primeiro elemento da lista.
# 
# Índices negativos não começam por zero, mas sim, por -1 e terminam na quantidade negativa total de elementos. Assim, em index negativo, os elementos são acessados no intervalo $\left[ -1, -n \right]$:

# In[28]:


# Acessando o último elemento (index -1):
print(compras[-1])


# In[29]:


# Acessando o penúltimo elemento (index -2):
print(compras[-2])


# In[30]:


# Acessando o antepenúltimo elemento (index -3):
print(compras[-3])


# In[31]:


# Acessando o primeiro elemento (index -4):
print(compras[-4])


# Recebemos o mesmo erro ao tentarmos selecionar um index negativo não existente:

# In[32]:


compras[-5]


# Há somente quatro elementos na lista `compras`, portanto não existe um index `-5`.

# ## Slicing

# *Slicing* é um anglicismo que refere-se ao ato de cortar/separar/dividir os elementos de um objeto e retornar um agrupamento específico destes elementos.
# 
# A sintaxe de slicing é a mesma de slicing de strings: com o operador `[]`:

# In[ ]:


nome_lista[inicio:fim:passo]


# Onde os parâmetros são:
# 
# - `inicio`: index por onde começar; incluso no agrupamento final.
# - `fim`: index por onde terminar; excluído do agrupamento final.
# - `passo`: intervalo de agrupamento.
# 
# O operador `:` é fundamental na sintaxe. Se não o colocarmos, Python entenderá que queremos acessar elementos individuais, como na seção anterior.
# 
# Se colocarmos somente o operador `:` significa que acessamos a lista inteira. Vamos criar uma lista numérica para exemplificar:

# In[33]:


numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# Agora façamos o slicing desta lista utilizando somente o operador `:`.

# In[35]:


print(numeros[:])


# Como visto, é o mesmo que:

# In[37]:


print(numeros)


# É interessante notar uma característica que já falamos no capítulo sobre variáveis: os locais da memória.
# 
# Com esta sintaxe de slicing total vista logo acima (acessando toda a lista), podemos nos referir a todos os objetos da lista e criar uma cópia independente. Criemos uma nova variável com esta sintaxe:

# In[39]:


copia_numeros = numeros[:]


# Agora vejamos ambas simultaneamente:

# In[41]:


print(numeros)
print(copia_numeros)


# Verifiquemos ambas. Primeiro, uma comparação de valores:

# In[44]:


numeros == copia_numeros


# Python nos retornou o valor `True`, uma vez que os elementos de ambas listas são, de fato, iguais. Agora façamos uma comparação de identidade:

# In[45]:


numeros is copia_numeros


# Nesta comparação nos é retornado o valor `False`, pois as duas listas não são a mesma: elas estão em locais diferentes da memória, portanto são variáveis independentes. Se mudarmos uma destas, a outra não será afetada:

# In[46]:


# Originais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[48]:


# Modificando a lista original:
numeros[0] = -1000


# In[49]:


# Listas finais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# Lembrando: quando utilizamos uma designação normal, estamos criando duas referências para o mesmo local da memória:

# In[50]:


copia_numeros = numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# Comparemos:

# In[51]:


print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[52]:


numeros == copia_numeros


# In[53]:


numeros is copia_numeros


# Agora, ambas listas estão no mesmo local da memória do computador; são referências para este mesmo local. Se mudarmos uma, a outra será modificada:

# In[46]:


# Originais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[54]:


# Modificando a lista original:
numeros[0] = -1000


# In[55]:


# Listas finais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# Parênteses fechado, voltemos ao slicing, com nossa lista original.

# In[57]:


numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# Utilizando o primeiro parâmetro — `inicio`:

# In[58]:


# Acessando todos os elementos até o fim da lista, iniciando pelo index 0:

print(numeros[0:])


# In[59]:


# Acessando todos os elementos até o fim da lista, iniciando pelo index 4:

print(numeros[4:])


# In[61]:


# Acessando todos os elementos até o fim da lista, iniciando pelo index 8:

print(numeros[8:])


# Se fizermos um slicing de um index inexistente, teremos uma lista vazia:

# In[62]:


print(numeros[10:])


# O slice começa no index 10, mas este não existe. Portanto, uma lsita vazia é retornada.

# Assim como indexação, podemos fazer o slicing com index negativos e dividir a lista iniciando pelos últimos elementos:

# In[79]:


# Inicia no index -1 e pega todos elementos até o fim da lista:
print(numeros[-1:])


# In[80]:


# Inicia no index -5 e pega todos elementos até o fim da lista:
print(numeros[-5:])


# In[83]:


# Inicia no index -8 e pega todos elementos até o fim da lista:
print(numeros[-8:])


# In[84]:


# Inicia no index -10 e pega todos elementos até o fim da lista:
print(numeros[-10:])


# Adicionando o segundo parâmetro — `fim`: com este, indicamos até qual index queremos acessar. Este parâmetro funciona de forma exclusiva, ou seja, o index em si não é incluído no slicing. O slicing retornado inclui somente até o index anterior. Desta forma, se fazemos um slicing com o index final de `6`, nos será retornado todos os elementos incluindo até o index `5`.
# 
# Vejamos exemplos:

# In[85]:


# Inicia no index 0 e termina no index 1:
print(numeros[:2])


# In[86]:


# Inicia no index 0 e termina no index 5:
print(numeros[:6])


# In[87]:


# Inicia no index 0 e termina no index 9:
print(numeros[:10])


# E com índices negativos (também de forma exclusiva):
# 
# ```{admonition}Observação
# Perceba que, já que estamos utilizando índices negativos, aumentamos o index ao contabilizar qual é o elemento anterior.
# ```

# In[90]:


# Inicia no index 0 e termina no index -2:
print(numeros[:-1])


# In[100]:


# Inicia no index 0 e termina no index -7:
print(numeros[:-6])


# Podemos, logicamente, utilizar ambos parâmetros conjuntamente:

# In[102]:


# Slicing iniciando do index 1 até o index 6:

print(numeros[1:7])


# In[103]:


# Slicing iniciando do index 3 até o index 8:

print(numeros[3:9])


# In[104]:


# Slicing iniciando do index 1 até o index -5:

print(numeros[1:-4])


# Por fim, o terceiro parâmetro: `passo`. Neste, lidamos com o intervalo do slicing; quantos índices pular ao acessar os elementos selecionados.
# 
# ```{admonition}Observação
# Temos de utilizar o operador `::` para simbolizar que estamos nos referindo ao último parâmetro. Nesta forma, pegaremos todos os elementos da lista, por default.
# ```

# In[115]:


# Acessando todos os elementos da lista, com intervalo de um índice:

print(numeros[::1])


# In[118]:


# Acessando todos os elementos da lista, com intervalo de dois índices:

print(numeros[::2])


# Podemos utilizar intervalos negativos também. Neste caso, a lista será invertida e o intervalo especificado será aplicado:

# In[117]:


# Acessando todos os elementos da lista, com intervalo de dois índices negativos:

print(numeros[::-2])


# Finalmente, podemos utilizar todos os parâmetros juntos:

# In[120]:


# Iniciando no index 2, terminando no index 8, com intervalo de dois índices:

print(numeros[2:9:2])


# In[122]:


# Iniciando no index 2, terminando no index 8, com intervalo de três índices:

print(numeros[2:9:3])


# Notemos neste último exemplo acima que ainda há um último elemento na lista: o elemento `900` no index `9`, mas este não é incluído, pois o próximo número que seria incluído nesta sequẽncia em particular seria o número `1100` no index `11`. Contudo, este elemento não existe nesta lista.

# ## Listas Aninhadas

# Uma vez que listas aceitam qualquer tipo de estrutura em Python, podemos ter então listas dentro de listas — as chamadas **listas aninhadas**.
# 
# Vamos criar algumas:

# In[128]:


aninhado = [
    [0, 1, 2, 3], # Lista 1
    [True, False], # Lista 2
    ["str1", "str2", "str3"], # Lista 3
]

print(aninhado)


# Na lista `aninhado` encontram-se quatro outras listas, cada uma contendo seus próprios dados.
# 
# Como podemos acessar estes elementos? Da mesma forma que acessamos listas simples: com o operador '[]' e utilizando os índeces dos elementos desejados.
# 
# Primeiramente vamos acessar a primeira lista aninhada — a lista de números inteiros. Esta lista está no index zero:

# In[125]:


print(aninhado[0])


# Acessamos a lista corretamente. Podemos acessar todas as outras listas aninhadas da mesma forma:

# In[126]:


# Acessando a segunda lista aninhada - booleanos (index 1):
print(aninhado[1])


# In[129]:


# Acessando a terceira lista aninhada - strings (index 2):
print(aninhado[2])


# Podemos acessar os elementos próprios de cada lista aninhada adicionando mais um operador '[]' para indicar o aninhamento de index.
# 
# Vamos acessar os elementos da lista de inteiros:

# In[130]:


# Primeiro elemento da lista de inteiros:
print(aninhado[0][0])


# In[131]:


# Segundo elemento da lista de inteiros:
print(aninhado[0][1])


# In[132]:


# Terceiro elemento da lista de inteiros:
print(aninhado[0][2])


# In[133]:


# Quarto elemento da lista de inteiros:
print(aninhado[0][3])


# Da mesma forma, acessamos os elementos das outras listas aninhadas:

# In[137]:


# Acessando os elementos da lista de booleanos:

print(aninhado[1][0])
print(aninhado[1][1])


# In[136]:


# Acessando os elementos da lista de strings:

print(aninhado[2][0])
print(aninhado[2][1])
print(aninhado[2][2])


# ## Métodos de Listas

# Há diversos métodos próprios de listas:
# 
# dir(list)

# ### Método `append()`

# Com 'append()', colocamos um novo elemento no fim de uma lista
# 
# l = [1, 2]
# 
# print(l)
# 
# l.append("risos")
# 
# print(l)
# 
# 
# Exemplo com 'for' (e um exemplo mais grosseiro que poderia ser feito
# com compreensão de listas):
# 
# l = []
# 
# print(l)
# 
# for numero in range(10):
#     l.append(numero)
# 
# print(l)

# ### Método `clear()`

# Com 'clear()', excluímos todos os elementos da lista:
# 
# l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# 
# print(l)
# 
# l.clear()
# 
# print(l)

# ### Método `pop()`

# Com 'pop()', removemos um elemento em um dado index e o retornamos.
# Útil para criar novas variáveis. Por default, o último elemento é 
# removido.
# 
# l = [1, 2, 3, 4, 5, 6, 7, 8, 9]
# 
# print(l)
# 
# l.pop() Removendo o último elemento
# 
# print(l)
# 
# l.pop(6) Removendo o elemento no index 6
# 
# print(l)

# ### Método `remove()`

# Com 'remove()', removemos (SEM RETORNAR!) a primeira ocorrência de
# um elemento
# 
# l = [1, 1, 1, 1, 2, 3, 4, 4, 4, 5, 6]
# 
# print(l)
# 
# l.remove(1)
# 
# print(l)
# 
# l.remove(4)
# 
# print(l)
# 
# l.remove(5)
# 
# print(l)
