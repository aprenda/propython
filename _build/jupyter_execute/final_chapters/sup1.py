#!/usr/bin/env python
# coding: utf-8

# # Suplemento

# Agora que vimos as importantes características ordenação e mutabilidade, faremos aqui uma pequena pausa nas estruturas de dados de vários objetos em Python: . Com estes conceitos, podemos revisitar alguns pontos já vistos e complementar nossos conhecimentos anteriores.

# ## Relação Objetos-Memória

# Primeiramente, voltaremos a um tópico que já falamos no capítulo sobre variáveis: os locais da memória. Comecemos com nossa lista `numeros`:

# In[1]:


numeros = [
    0, 100, 200, 300, 400,
    500, 600, 700, 800, 900
]


# Agora façamos seu slicing total:

# In[2]:


print(numeros[:])


# Como visto, é o mesmo que:

# In[3]:


print(numeros)


# Com esta sintaxe de slicing total vista logo acima (acessando toda a lista), podemos nos referir a todos os objetos da lista e criar uma cópia independente. Criemos uma nova variável com esta sintaxe:

# In[4]:


copia_numeros = numeros[:]


# Agora vejamos ambas simultaneamente:

# In[5]:


print(numeros)
print(copia_numeros)


# Verifiquemos ambas. Para tal, utilizaremos alguns operadores sobre os quais falaremos especificamente em outro capítulo, mas os vejamos em ação antecipadamente.
# 
# Primeiro, o operador booleano de comparação de valores — o operador `==`: ele compara os valores entre dois objetos e retorna `True` se ambos objetos possuírem os mesmos valores e `False` se não possuírem.

# In[6]:


numeros == copia_numeros


# Python nos retornou o valor `True`, uma vez que os elementos de ambas listas são, de fato, iguais. Agora façamos uma comparação de identidade com o operador `is`: ele nos retorna `True` se os dois objetos forem os mesmos, isto é, se ambos estiverem no mesmo local da memória, e retorna `False` se não forem.

# In[7]:


numeros is copia_numeros


# Nesta comparação nos é retornado o valor `False`, pois as duas listas são diferentes: elas estão em locais diferentes da memória, portanto são variáveis independentes. Se mudarmos uma destas, a outra não será afetada:

# In[8]:


# Originais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[9]:


# Modificando a lista original:
numeros[0] = -1000


# In[10]:


# Listas finais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# Lembrando: quando utilizamos uma designação normal, estamos criando duas referências para o mesmo local da memória:

# In[11]:


copia_numeros = numeros = [
    0, 100, 200, 300, 400,
    500, 600, 700, 800, 900
]


# Comparemos:

# In[12]:


print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[13]:


numeros == copia_numeros


# In[14]:


numeros is copia_numeros


# Agora, ambas listas estão no mesmo local da memória do computador; são referências para este mesmo local. Se mudarmos uma, a outra será modificada:

# In[15]:


# Originais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# In[16]:


# Modificando a lista original:
numeros[0] = -1000


# In[17]:


# Listas finais:
print(f"numeros: {numeros}")
print(f"copia_numeros: {copia_numeros}")


# Uai?

# In[18]:


var1 = var2 = 6


# In[19]:


print(f"var1: {var1}\nvar2: {var2}")


# In[20]:


var1 = 8


# In[21]:


print(f"var1: {var1}\nvar2: {var2}")


# (string_comp)=
# ## Strings

# Falamos de strings anteriormente, mas deixamos propositalmente duas características para depois que tivéssemos visto indexação e modificação.

# ### Indexação

# Primeira característica: **strings podem ser indexadas.**

# ### Mutabilidade

# **Strings podem ser indexadas.**
# 
# aefd
# 
# - **Strings são imutáveis.**

# ## Questão 20

# Crie uma variável com a string `"martelo"`, inverta a ordem de seus caracteres (escreva a palavra de trás para a frente) e guarde em uma variável chamada `"inverso"`. Por fim, printe `"inverso"`.

# In[22]:


var = "martelo"

inverso = var[::-1]

print(inverso)

