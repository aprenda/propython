#!/usr/bin/env python
# coding: utf-8

# # Métodos de Listas

# Nesta seção falaremos sobre métodos de listas. O que são métodos?
# 
# Métodos são funções ligadas à objetos. Neste caso, métodos de listas são funções associadas à listas. Funciona da seguinte maneira: temos nosso objeto — a lista — e, a partir dele, executaremos uma função — o método. A sintaxe de um método qualquer é descrita como:

# In[1]:


objeto.função()


# A ligação entre o objeto e seu método é operador `.` — o ponto final. Ao colocarmos o operador `.` após o objeto em questão e, logo em seguida, executarmos o método, queremos dizer que a função é parte do objeto. Vejamos na prática, com listas.
# 
# Podemos ver todos os métodos que uma lista — tecnicamente, um objeto do tipo `list` — possui. Com a função `dir()`, podemos saber todos os atributos e métodos de um objeto:

# In[2]:


dir(list)


# Um objeto `list`, então, possui diversos métodos diferentes. Varios dos métodos retornados possuem dois underscores no nome como `__add__`. Estes são métodos especiais e falaremos deles quando falarmos de classes e OOP. Para nós, agora, alguns métodos são mais importantes. Falaremos destes agora. Primeiramente, vamos criar uma lista para trabalharmos com os vários métodos de listas:

# In[3]:


capitais = [
    "Rio Branco", "Boa Vista", "Macapá", "Manaus", "Boa Vista",
    "Belém", "Boa Vista", "Porto Velho", "Boa Vista"
]


# In[4]:


print(capitais)


# Nossa lista `capitais` contém as capitais da região norte do Brasil. Agora estamos prontos para seus métodos.

# ## Método `copy()`

# Comecemos com um método que faz uma ação que aprendemos há pouco: uma cópia. O método `copy()` faz uma cópia *shallow* da lista em questão. Sua sintaxe é:

# In[5]:


list.copy()


# Abaixo exemplificamos:

# In[6]:


# Copiando a lista 'capitais':
copia_capitais = capitais.copy()


# In[7]:


print(capitais)


# In[8]:


print(copia_capitais)


# Uma última informação: sempre podemos ver, através do Python, o texto de ajuda de qualquer função utilizando `help()`. Por exemplo, vejamos a ajuda da função `id()`:

# In[9]:


print(help(id))


# No caso de métodos, temos de especificar **de qual tipo de objeto** o método faz parte. Por exemplo, vejamos a ajuda mo método `copy()`. Como este método faz parte do objeto lista (*list*), então sua especificação é dada por:
# 
#         list.copy
# 
# Abaixo vemos a ajuda deste método:

# In[10]:


print(help(list.copy))


# Deixamos para que o leitor veja a ajuda dos outros métodos.

# ## Método `index()`

# Com o método `index()`, é retornado o index da **primeira ocorrência** de um elemento na lista. Sua sintaxe é:

# In[11]:


list.index(elemento)


# Vamos descobrir a posição de alguns elementos:

# In[12]:


print(capitais.index("Belém"))


# A string `"Belém"` está na posição `5`.
# 
# Nossa lista tem quatro elementos repetidos: `Boa Vista`. Vamos descobrir em qual posição ele encontra-se.

# In[13]:


print(capitais.index("Boa Vista"))


# Há três strings `"Boa Vista"` na lista, mas apenas a posição `1` foi retornada. Isto ocorre porque o método `index()` retorna apenas o index do **primeiro elemento encontrado**. Poderiam haver vinte strings iguais, mas `index()` retornará apenas a posição da primeira encontrada.
# 
# E se procurássemos por um elemento que não está na lista?

# In[14]:


print(capitais.index("Goiânia"))


# Um erro é retornado, pois a string `"Goiânia"` não está na lista `capitais`.

# ## Método `count()`

# Com o método `count()`, podemos contar quantas vezes algum elemento se repete na lista. Sua sintaxe é:

# In[15]:


list.count(elemento)


# Deste modo, fornecemos o elemento que queremos contar e nos é retornado a quantidade de repetições. Vamos contar quantas vezes Boa Vista repete-se em nossa lista:

# In[16]:


print(capitais.count("Boa Vista"))


# Sabemos que, de fato, há quatro strings `"Boa Vista"` na lista `capitais`. E quanto a Manaus?

# In[17]:


print(capitais.count("Manaus"))


# Há apenas uma string `"Manaus"` na lista. E se colocássemos um elemento inexistente?

# In[18]:


print(capitais.count("Cuiabá"))


# Não nos é retornado erro algum, apenas que não há nenhum elemento `"Cuiabá"` na lista. Por isto, esteja atento!

# ## Método `pop()`

# Com o método `pop()`, removemos um elemento específico da lista e o retornamos. A sintaxe deste método é:

# In[19]:


list.pop(index)


# Como visto, precisamos fornecer o index do elemento que queremos retirar. Nossa lista possui três elementos repetidos:

# In[20]:


print(capitais)


# E estes elementos estão nas posições `1`, `4`, `6` e `8`. Antes de removê-los, vamos nos atentar a um detalhe importante: o método `pop()` remove o elemento da lista **e o retorna**, ou seja, podemos utilizar este método quando não somente queremos remover algo de uma lista, mas ainda queremos utilizar este elemento removido. Abaixo, removemos a string `"Boa Vista"` do index `1` e a guardamos em uma variável nova:

# In[21]:


capital_roraima = capitais.pop(1)


# Podemos ver agora que a variável `capital_roraima` possui a string `"Boa Vista"`:

# In[22]:


print(capital_roraima)


# E que a lista `capitais` possui um elemento a menos, pois o retiramos:

# In[23]:


print(capitais)


# Por default (sem colocar index algum), este método remove o último elemento da lista:

# In[24]:


capital_roraima2 = capitais.pop()


# In[25]:


print(capital_roraima2)


# E, de fato, o último elemento da lista foi removido:

# In[26]:


print(capitais)


# ## Método `remove()`

# Com o método `remove()`, removemos a primeira ocorrência de um elemento. Sua sintaxe é:

# In[27]:


list.remove(elemento)


# Fornecemos, então, o elemento que desejamos retirar da lista. Contudo, acabamos de ver o método `pop()`, o qual também remove elementos de uma lsita. Qual é a diferença?
# 
# O método `pop()` remove um elemento **e o retorna**. O método `remove()` apenas remove um elemento; **não há retorno algum**.
# 
# Em nossa lista, a string `"Boa Vista"` ainda possui duplicata. Vamos remover esta repetição da lista com `remove()`: basta fornecer à função qual elemento queremos retirar.

# In[28]:


capitais.remove("Boa Vista")


# E agora vejamos nossa lista:

# In[29]:


print(capitais)


# Não possui nenhuma duplicata.
# 
# ```{note}
# Fique sempre atento: o método `remove()` retira **a primeira ocorrência de um elemento!**
# ```

# ## Método `append()`

# Com o método `append()`, colocamos um novo elemento na lista, na última posição.Sua sintaxe é:

# In[30]:


list.append(elemento)


# Nossa lista está incompleta: falta a capital Palmas. Vamos, assim, adicioná-la com `append()`:

# In[31]:


capitais.append("Palmas")


# Agora vejamos as capitais:

# In[32]:


print(capitais)


# Adicionamos a capital do Tocantins à última posição da lista e completamos as capitais da região norte do país.

# ## Método `insert()`

# O método `insert()` adiciona um elemento à lista em uma posição específica. Sua sintaxe é:

# In[33]:


list.insert(index, elemento)


# Vamos adicionar as capitais da região centro-oeste à lista. Para simplificar o exemplo, as colocaremos nas três primeiras posições (`0`, `1` e `2`). Primeiro Goiânia:

# In[34]:


capitais.insert(0, "Goiânia")


# In[35]:


print(capitais)


# Agora Campo Grande:

# In[36]:


capitais.insert(1, "Campo Grande")


# In[37]:


print(capitais)


# Por fim, Cuiabá:

# In[38]:


capitais.insert(2, "Cuiabá")


# In[39]:


print(capitais)


# ```{note}
# Atente-se à forma com a qual os elementos são inseridos: se adicionamos `"Cuiabá"` na posição `2`, significa que ele será adicionado à frente do elemento atual na posição `2`, no caso, `"Rio Branco"`.
# ```

# ## Método `extend()`

# O método `extend()` é parecido com o método `insert()`: ele também adiciona elementos à lista. A diferença é que ele adiciona vários elementos de uma única vez e os coloca no fim da lista. Sua sintaxe é:

# In[40]:


list.extend(iterável)


# Fornecemos, então, um iterável para o método. Sabemos que uma lista é um iterável, então significa que podemos adicionar todos os elementos de uma lista em outra lista. Façamos e=isto: vamos adicionar as capitais da região sul à lista `capitais`. Primeiro, vamos criá-la:

# In[41]:


sul = ["Porto Alegre", "Florianópolis", "Curitiba"]


# Agora vamos utilizar o método `extend()` para adicionar estas cidades.

# In[42]:


# Lista antes do método:
print(capitais)


# In[43]:


capitais.extend(sul)


# In[44]:


# Lista após o método:
print(capitais)


# ## Método `reverse()`

# O método `reverse()` inverte a posição de todos os elementos de uma lista, ou seja, os últimos elementos serão os primeiros e os primeiros serão os últimos. Sua sintaxe é:

# In[45]:


list.reverse()


# Façamos isto com nossa lista:

# In[46]:


# Lista antes do método:
print(capitais)


# In[47]:


capitais.reverse()


# In[48]:


# Lista após o método:
print(capitais)


# ## Método `clear()`

# O método `clear()` apaga permanentemente todos os elementos da lista. Sua sintaxe é:

# In[49]:


list.clear()


# Façamos isto com nossa lista: vamos torná-la uma lista vazia.

# In[50]:


# Lista antes do método:
print(capitais)


# In[51]:


capitais.clear()


# In[52]:


# Lista após o método:
print(capitais)


# ```{admonition} Questão
# Há um método que não falamos aqui: o método `sort()`. Veja esta [questão](sort_perg) e aprenda mais um método de Python.
# ```
