#!/usr/bin/env python
# coding: utf-8

# # Slicing

# *Slicing* é um termo que refere-se ao ato de cortar/separar/dividir os elementos de um objeto e retornar um agrupamento específico destes elementos. Portanto, usamos o slicing para acessar vários elementos de uma única vez.
# 
# Fazemos um slicing de um objeto com o operador `[]` — o mesmo operador de listas. A sintaxe de slicing é:

# ```python
# nome_lista[inicio:fim:passo]
# ```

# Onde os parâmetros são:
# 
# - `inicio`: index por onde começar; incluso no agrupamento final. Default: `0`.
# - `fim`: index por onde terminar; excluído do agrupamento final.
# - `passo`: intervalo de agrupamento. Default: `1`

# Agora que vimos a sintaxe de slicing, é válido comentarmos aqui, antes de continuarmos, a diferença no uso deste operador.
# 
# Ao criarmos uma lista com o operador `[]`, o utilizamos sozinho, e dentro dele colocamos os elementos desejados:
# 
# ```python
# # Criando uma lista numérica:
# [3.14, 2.71, 6.022]
# ```
# 
# E podemos ligá-la à variável `numeros`:

# In[1]:


# Exemplo de criação de uma lista numérica:
numeros = [3.14, 2.71, 6.02]


# A diferença para um slicing é:
# 
# - Aplicamos o operador `[]` a um objeto já existente;
# - Dentro dos colchetes, ao invés de elementos, colocamos os parâmetros de slicing.
# 
# Façamos um slicing simples da nossa lista `numeros`:

# In[2]:


numeros[0:2]


# Veja o que estamos fazendo:
# 
# 1. Escrevemos o nome do objeto no qual queremos fazer o slicing: `numeros`
# 1. Quando colocamos o operador `[]` ao fim da lista, estamos aplicando-o à lista (para fazer slicing): `numeros[]`
# 1. Dentro dos colchetes, colocamos os parâmetros de slicing. Neste caso, os parâmetros são os índices zero (parâmetro de início) e dois (parâmetro de fim): `numeros[0:2]`
# 
# Com esta sintaxe, estamos dizendo ao Python: "retorne para mim os elementos da lista `numeros` que estão nas posições zero e um".
# 
# ```{admonition} Atenção!
# Por que pegamos somente as duas primeiras posições no slicing acima? Não deveriam ser retornados os elementos nos índices de 0 até 2?
# 
# Não, o slicing está correto. Veja novamente a descrição do parâmetro `fim` acima. Este parâmetro indica onde terminamos o slicing, mas ele é **excludente**, isto é, a posição indicada neste parâmetro não é incluída no agrupamento final (o slicing retornado). Por isto, efetivamente, quando fazemos o slicing, estamos pegando os elementos nas posições de início até a posição anterior do parâmetro `fim`. Em nosso exemplo, pegamos então apenas os índices `0` e `1`, pois `1` é o índice anterior ao índice `2`.
# ```

# (slic-total)=
# ## Slicing Total

# Há duas formas de retornamos uma lista completa (com todos seus elementos). Fazendo um print direto da lista em questão:

# In[3]:


print(numeros)


# Ou através do slicing somente com um operador `:` — o que chamaremos de *slicing total*, uma vez que é retornada a lista inteira através da sintaxe de slicing. Exemplo:

# In[4]:


# Acessando todos os elementos da lista:
print(numeros[:])


# Mas o resultado também é o mesmo se usarmos os dois operadores `:` (como especificado na definição de slicing):

# In[5]:


# Acessando todos os elementos da lista:
print(numeros[::])


# Esta forma acima é redundante à anterior, então, quando fizermos um slicing total, utilizaremos apenas um operador `:`.
# 
# O que significa usarmos o slicing somente com o operador `:`? Veja novamente a definição de slicing no começo desta seção. Perceba que há argumentos default nos parâmetros `inicio` e `passo`, respectivamente, `0` e `1`. Argumentos default são utilizados quando nenhum valor é especificado, assim, os valores default são utilizados. Desta forma, em nosso exemplo, como não especificamos nenhum valor ao executarmos o código:
# 
# ```python
# numeros[:]
# ```
# 
# Python utilizará os valores default destes parâmetros, que, no caso, é o mesmo que:
# 
# ```python
# numeros[0:3:1]
# ```
# 
# Onde:
# 
# - `0` é o primeiro índice;
# - `3` é o último índice (exclusivo, lembre-se!);
# - `1` é o intervalo.
# 
# Por fim, O operador `:` é fundamental na sintaxe de slicing. Ele que diferenciará slicing de indexação. Se não o colocarmos, Python entenderá que queremos acessar elementos individuais, como na [seção anterior](lista-index).

# ## Manipulando o Slicing

# Vamos manipular especificamente cada parâmetro do slicing. Comecemos aumentando nossa lista:

# In[6]:


numeros = [0, 100, 200, 300, 400, 500, 600, 700, 800, 900]


# ### `inicio`

# Utilizando o primeiro parâmetro — `inicio` —, vamos acessar todos os elementos até o fim da lista, iniciando pelo index `0`:

# In[7]:


print(numeros[0:])


# Agora vamos acessar todos os elementos até o fim da lista, iniciando pelo index `4`:

# In[8]:


print(numeros[4:])


# E o mesmo com o index `8`:

# In[9]:


print(numeros[8:])


# Se fizermos um slicing de um index inexistente, teremos uma lista vazia. Nenhum erro é retornado:

# In[10]:


print(numeros[10:])


# O slice começa no index 10, mas este não existe. Portanto, uma lista vazia é retornada.
# 
# Assim como indexação, podemos fazer o slicing com index negativos e dividir a lista iniciando pelos últimos elementos.
# 
# Abaixo, iniciamos no index `-1` e pegamos todos elementos até o fim da lista:

# In[11]:


print(numeros[-1:])


# Como iniciamos pelo último elemento (index `-1`), nossa lista tem apenas um único elemento. Façamos o mesmo, mas agora começando pelo index `-5`:

# In[12]:


print(numeros[-5:])


# E index `-8`:

# In[13]:


print(numeros[-8:])


# E o index `-10`:

# In[14]:


# Inicia no index -10 e pega todos elementos até o fim da lista:
print(numeros[-10:])


# Neste último, como começamos pelo primeiro elemento (index `-10`), nos é retornado a lista inteira.

# ### `fim`

# Adicionando o segundo parâmetro — `fim`: com este, indicamos até qual index queremos acessar. Este parâmetro funciona de forma exclusiva, ou seja, o index em si não é incluído no slicing. O slicing retornado inclui somente até o index anterior. Desta forma, se fazemos um slicing com o index final de `6`, nos será retornado todos os elementos incluindo até o index `5`. Vejamos exemplos abaixo.
# 
# Vamos fazer um slicing do index `0` ao index `1`:

# In[15]:


print(numeros[:2])


# Relembrando, perceba que não fornecemos nenhum valor para o parâmetro `inicio`, então a sintaxe `[:2]` é o mesmo que `[0:2]`. Assim, no comando acima pegamos os dois primeiros elementos da lista, pois, uma vez que o valor do parâmetro `fim` é `2`, significa que o slicing será feito dos índices zero até 1.
# 
# Vamos aumentar nosso slicing: começaremos no index `0` e vamos pegar até a quinta posição, ou seja, o valor do parâmetro `fim` será `6`.

# In[16]:


print(numeros[:6])


# Agora todos os elementos até o index `9`:

# In[17]:


print(numeros[:10])


# Como a lista possui 10 elementos, o último index é o `9`, retornando assim, a lista inteira.

# Também é possível utilizarmos índices negativos neste parâmetro (também de forma exclusiva). Vamos iniciar o slicing no index `0` e terminar no index `-2`:

# In[18]:


print(numeros[:-1])


# Agora, vamos do index `0` até o index `-7`:

# In[19]:


print(numeros[:-6])


# ```{admonition} Observação
# Perceba que, já que estamos utilizando índices negativos, aumentamos o index ao contabilizar qual é o elemento anterior.
# ```

# Podemos, logicamente, utilizar ambos parâmetros `inicio` e `fim` conjuntamente.

# In[20]:


# Slicing do index 1 até o index 6:
print(numeros[1:7])


# In[21]:


# Slicing do index 3 até o index 8:
print(numeros[3:9])


# In[22]:


# Slicing do index 1 até o index -5:
print(numeros[1:-4])


# ### `passo`

# Por fim, o terceiro parâmetro: `passo`. Neste, lidamos com o intervalo do slicing; quantos índices pular ao acessar os elementos selecionados.
# 
# ```{admonition} Observação
# Temos de utilizar o operador `::` para simbolizar que estamos nos referindo ao último parâmetro. Nesta forma, pegaremos todos os elementos da lista, por default.
# ```
# 
# Num primeiro exemplo, vamos acessar todos os elementos da lista, com intervalo de `1`:

# In[23]:


print(numeros[::1])


# Claramente pegamos todos os elementos da lista. Isto ocorre pois o intervalo `1` é o valor default deste parâmetro, então não pulamos nenhum elemento ao especificar um intervalo de `1`.
# 
# Vamos agora acessar todos os elementos da lista, com intervalo de dois índices, isto é, pulando um elemento:

# In[24]:


print(numeros[::2])


# E se colocássemos uma intervalo igual a `12`?

# In[25]:


print(numeros[::12])


# Nos é retornado apenas o primeiro elemento, pois nossa lista possui 10 elementos. Python pega o primeiro elemento `0` e tenta pegar o 12°, mas este não existe, então retorna apenas a lsita com o elemento existente.
# 
# Podemos utilizar intervalos negativos também. Neste caso, a lista será invertida e o intervalo especificado será aplicado. Como exemplo, vamos acessar todos os elementos da lista, com intervalo de dois índices negativos:

# In[26]:


print(numeros[::-2])


# Finalmente, podemos utilizar todos os parâmetros juntos.
# 
# Abaixo, iniciamos no index `2`, terminamos no index `8`, com intervalo de duas posições:

# In[27]:


print(numeros[2:9:2])


# No próximo exemplo, iniciamos no index `2`, terminamos no index `8`, com intervalo de três posições:

# In[28]:


print(numeros[2:9:3])


# Notemos neste último exemplo acima que ainda há um último elemento na lista: o elemento `900` no index `9`, mas este não é incluído, pois o próximo número que seria incluído nesta sequência em particular seria o número `1100` no index `11`. Contudo, este elemento não existe nesta lista.
