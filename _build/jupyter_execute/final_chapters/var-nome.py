#!/usr/bin/env python
# coding: utf-8

# # Nomeando Variáveis

# Para haver uma padronização nos códigos de Python, a comunidade possui uma convenção como um guia sobre como nomear variáveis. Esta convenção é parte do [PEP 8](https://pep8.org/#naming-conventions), onde você pode ver a resolução completa.

# ## Restrições em Nomeação

# Em Python, há algumas restrições de nomeação de variáveis:

# 1. Variáveis têm de começar com uma letra ou underscore:
# 
# - Forma correta:
# 
# > gatos = 2    
# > _gatos = 2
# 
# - Forma errada:
# 
# > 8gatos = 2    
# > .gatos = 2
# 
# 2. O restante do nome tem de consistir de letras, números ou underscores:
# 
# - Forma correta:
# 
# > gatos8 = 2
# 
# - Forma errada:
# 
# > ola@voce = 7    
# > nome+2=9
# 
# Neste último exemplo:
# 
# > nome+2=9
# 
# Python acha que estamos tentando designar o número `9` ao operador `+`. Entretanto, este operador já possui outros significados em Python, por isto o erro.
# 
# 3. Nomes são *case-sensitive*, ou seja, há diferenciação entre letras maiúsculas e minúsculas.
# 
# Uma variável com o nome `PAMONHA` é diferente da variável `Pamonha` e da variável `pamonha`.

# ## Convenções de Nomeação (Boas Práticas de Nomeação)

# 1. Use underscores para separar palavras:
# 
# Há projetos nos quais os programadores utilizam o chamado *camelCase* para separar as palavras e outros que utilizam underscores. Você deve seguir o formato já estipulado, se o houver. Se não, dê preferência ao
# underscore:
# 
# Em *camelCase*, inicia-se a variável com uma letra minúscula e diferenciam-se as próximas com maiúsculas.
# 
# Veja comparações destas nomeações na tabela abaixo:
# 
# | Underscore         | camelCase        |
# | :----------------: | :--------------: |
# | torta_morango      | tortaMorango     |
# | tv_plasma          | tvPlasma         |
# | uma_longa_variavel | umaLongaVariavel |
# 
# 2. As variáveis devem ser escritas em letras minúsculas, com exceções:
# 
# Variáveis normais devem ser escritas totalmente em letras minúsculas. Variáveis em letras maiúsculas são aplicadas normalmente somente para indicar constantes - valores que não mudarão durante o programa
# 
# Nomes escritos com letras maiúsculas e minúsculas são aplicados somente para indicar classes — um tipo de objeto mais avançado em Python.
# 
# A tabela abaixo apresenta exemplos destas nomeações:
# 
# | Constante | Classes           | Variáveis Normais |
# | :-------: | :---------------: | :---------------: |
# | PI        | FabricaAutomotiva | numero_carros     |
# | EULER     | AplicativoWeb     | qtd_cartas        |
# | FPS       | Calendario        | tempo_final       |
# 
# 
# 3. Não inicie variáveis com underscores ou underscores duplos:
# 
# Python possui uma filosofia diferente de outras linguagens de programação: não há nada que impeça alguém de ver e/ou modificar seu código. Outras linguagens de programação como C, C++ e Java possuem variáveis e métodos (funções) privados, significando que a própria linguagemd e programação impede que ninguém além do programador possa vê-los ou alterá-los. Em Python, a filosofia é de um acordo consensual implícito entre os desenvolvedores: se há uma variável ou método indicado como privado, ela não deve ser mexida, mas a linguagem Python não impedirá isto.
# 
# Em Python indicamos que algo é privado colocando um underscore antes do nome da variável. Assim, outros programadores saberão que aquele objeto não deve ser modificado.
# 
# Em Python, também há métodos dentro de classes nos quais underscores duplos são utilizados antes e depois dos nomes. Estes são chamados de métodos dunder (**D**ouble **UNDER**scores). Também não devem ser mexidos.
# 
# Portanto, variáveis normais não devem ser comumente iniciadas com underscores, a não ser que queira indicar que são privadas.
# 
# A tabela abaixo apresenta exemplos de nomes com underscores:
# 
# | Métodos Dunder | Variáveis/funções Privadas | Variáveis/funções Normais |
# | :------------: | :------------------------: | :-----------------------: |
# | \_\_init\_\_() | _qtd_inicial               | qtd_inicial               |
# | \_\_len\_\_()  | _lista                     | lista                     |
# | \_\_repr\_\_() | _ordenar_por_tamanho()     | ordenar_elementos()       |
# 
# 4. Os nomes das variáveis devem ser descritivos:
# 
# Como dissemos anteriormente, nomes de variáveis (ou funções, classes, ...) devem ser explícitos. Variáveis devem dizer claramente sua função.
# 
# Ao invés de:
# 
# > usuario = 190
# 
# Use:
# 
# > id_usuario = 190
# 
# 5. Evite acentuação ao nomear as variáveis:
# 
# Acentos podem causar erros aos programas, mesmo que forem aceitos. Evite sempre acentuações.

# ## Palavras Reservadas

# Em Python, há diversas palavras que não podemos utilizar como nomes para variáveis, uma vez que são próprias da linguagem. Se tentarmos, será retornado um erro. Algumas destas são:
# 
# - for
# - while
# - not
# - is
# - None
# - True
# - False
# - if
# - elif
# - else
# - return
# 
# Exemplo de erro:

# In[1]:


return = 2

