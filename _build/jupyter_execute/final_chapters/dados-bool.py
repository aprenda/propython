#!/usr/bin/env python
# coding: utf-8

# # Booleanos

# Valores booleanos são valores que possuem somente duas opções: `True` e `False`, que em também podem ser representados em programação e lógica como os números `1` e `0`, respectivamente. Chamam-se valores booleanos pois estes são resultantes da álgebra booleana, criada no século XIX pelo matemático [George Boole](dados-leia).
# 
# Em Python, os valores booleanos são escritos em inglês e com a primeira letra maiúscula. Usamos a função `print()` para "printar", isto é, ver o valor de algum objeto:

# In[1]:


# Printando o valor booleano True:
print(True)


# In[2]:


# Printando o valor booleano False:
print(False)


# ```{admonition} Uso do print
# Dependendo de qual software estiver utilizando para executar os códigos, a função `print()` não se faz necessária. Por exemplo, em cadernos (Jupyter/Colab) os comandos são executados e retornados diretamente da célula, sem necessitar o comando explícito de print.
# 
# Isto deve estar claro para o leitor, mas continuaremos usando a função `print()` explicitamente no restante do livro. Falaremos especificamente sobre esta função [no próximo capítulo](var-print).
# ```

# Podemos também ver o tipo de um objeto (sua classe) com a função `type()`:

# In[3]:


# Printando a qual tipo de objeto o valor True pertence:
print(type(True))


# In[4]:


# Printando a qual tipo de objeto o valor False pertence:
print(type(False))


# Qualquer outra forma de escrita é inválida e resultará em erro, pois Python reconhecerá qualquer outra forma de escrita como o nome de uma variável existente, e não, de uma palavra-chave imbuída em Python.
# 
# É exatamente isto que o erro `NameError` significa:

# In[5]:


print(true)


# In[6]:


print(TRUE)


# Podemos utilizar a função `bool()` para sabermos o valor booleano de um objeto em Python:

# In[7]:


# Valor booleano de True: 
bool(True)


# In[8]:


# Valor booleano do número 1: 
bool(1)


# In[9]:


# Valor booleano de False: 
bool(False)


# In[10]:


# Valor booleano do número zero:
bool(0)


# Em Python, o valor booleano de qualquer objeto é considerado verdadeiro a não ser que o objeto seja vazio ou seja implementado como falso. Veremos vários exemplos nas seções que seguem e quando chegarmos nas estruturas de dados.
