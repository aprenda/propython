#!/usr/bin/env python
# coding: utf-8

# # Métodos de Strings

# Uma seção muito importante, agora veremos os métodos mais importantes de strings.

# ## Método `count()`

# In[1]:


"AaabB".count("a")


# ## Método `startswith()`

# ## Método `endswith()`

# In[2]:


"AaabB".endswith("B")


# ## Método `format()`

# ```{note}
# Este método está aqui somente para fins de completude, você talvez possa ver esta forma de formatação em vários códigos mais antigos. Atualmente, se você quiser formatar strings, use [f-strings](f-string).
# ```

# In[3]:


"AaabB {}".format("nada")


# In[4]:


"AaabB {} kkk {}".format(1+2, "eita")


# ## Método `isalpha()`

# In[5]:


"AaabB".isalpha()


# ## Método `isnumeric()`

# In[6]:


"AaabB".isnumeric()


# In[7]:


"AaabB12".isnumeric()


# In[8]:


"12".isnumeric()


# ## Método `isalnum()`

# O método `isalnum()` retornará `True` se qualquer um de `isalpha()` ou `isnumeric()` retornar `True`.

# In[9]:


"AaabB".isalnum()


# In[10]:


"AaabB12".isalnum()


# In[11]:


"12".isalnum()


# ## Método `islower()`

# In[12]:


"AaabB".islower()


# In[13]:


"aab".islower()


# ## Método `isupper()`

# In[14]:


'BANANA'.isupper()


# In[15]:


'banana'.isupper()


# In[16]:


'baNana'.isupper()


# In[17]:


' '.isupper()


# ## Método `istitle()`

# In[18]:


"AaabB".istitle()


# In[19]:


"aab".istitle()


# In[20]:


"Aab".istitle()


# In[21]:


"Aab a".istitle()


# ## Método `lower()`

# ## Método `upper()`

# ## Método `capitalize()`

# ## Método `title()`

# ## Método `swapcase()`

# ## Método `strip()`

# ## Método `join()`

# ## Método `split()`

# ## Método `replace()`
