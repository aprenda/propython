#!/usr/bin/env python
# coding: utf-8

# (string_comp)=
# # Suplemento: Strings

# No capítulo sobre [listas](lista-intro), fomos introduzidos aos conceitos de mutabilidade, ordenação, indexação e métodos de objetos. Contudo, não só apenas as estruturas de dados possuem estas características. Neste capítulo veremos que as strings também são indexáveis e possuem métodos. Com estes conceitos, iremos revisitar alguns pontos já vistos e complementar nossos conhecimentos anteriores sobre strings.
# 
# ```{note}
# Os tipos de dados `int` e `float` também possuem métodos! Entretanto, seus métodos basicamente lidam com bits, bytes e suas representações. Por isto, não falaremos deles. Para nós, indexações e métodos de strings são muito mais utilizadas e importantes.
# ```
