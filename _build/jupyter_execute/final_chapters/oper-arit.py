#!/usr/bin/env python
# coding: utf-8

# # Operadores Aritméticos

# Operandos:
# 
# Unário:
# 
# Binário:
# 
# A não ser quando explicitamente especificado, todos os operadores debatidos aqui são binários.
# 
# ```{note}
# Vários operadores possuem mais de uma funcionalidade em Python. Nesta seção, apresentaremos apenas seu significado matemático. Os outros usos dos operadores serão debatidos em capítulos posteriores, à medida com que forem necessários.
# ```

# ## Operador `+`

# The + (addition) operator yields the sum of its arguments. The arguments must either both be numbers or both be sequences of the same type. In the former case, the numbers are converted to a common type and then added together. In the latter case, the sequences are concatenated.

# ## Operador `-`

# The - (subtraction) operator yields the difference of its arguments. The numeric arguments are first converted to a common type.

# ## Unário

# The unary - (minus) operator yields the negation of its numeric argument; the operation can be overridden with the __neg__() special method.

# ## Operador `*`

# The * (multiplication) operator yields the product of its arguments. The arguments must either both be numbers, or one argument must be an integer and the other must be a sequence. In the former case, the numbers are converted to a common type and then multiplied together. In the latter case, sequence repetition is performed; a negative repetition factor yields an empty sequence.

# ## Operador `**`

# O operador de exponenciação `**` (*power operator*)
# 
# in an unparenthesized sequence of power and unary operators, the operators are evaluated from right to left (this does not constrain the evaluation order for the operands): -1**2 results in -1.

# ## Operador `/`

# The / (division) and // (floor division) operators yield the quotient of their arguments. The numeric arguments are first converted to a common type. Division of integers yields a float, while floor division of integers results in an integer; the result is that of mathematical division with the ‘floor’ function applied to the result. Division by zero raises the ZeroDivisionError exception.
# 
# 

# ## Operador `//`

# Divisão de Inteiros (*floor division*)

# ## Operador `%`

# The % (modulo) operator yields the remainder from the division of the first argument by the second. The numeric arguments are first converted to a common type. A zero right argument raises the ZeroDivisionError exception. The arguments may be floating point numbers, e.g., 3.14%0.7 equals 0.34 (since 3.14 equals 4*0.7 + 0.34.) The modulo operator always yields a result with the same sign as its second operand (or zero); the absolute value of the result is strictly smaller than the absolute value of the second operand
