#!/usr/bin/env python
# coding: utf-8

# # Indexação de Listas

# Sabemos que listas são objetos ordenados, portanto, seus elementos possuem ordem; cada um possui uma posição específica dentro da lista. Tal ordenação de elementos é imprescindível na manipulação das estruturas ordenadas: conseguimos acessar e manipular elementos especificando sua posição individual dentro de um objeto — um processo chamado **indexação**.
# 
# Abaixo, conheceremos agora duas formas de indexação em Python: uma utilizando as posições normais de cada elemento e outra utilizando "posições negativas".

# ## Indexação Normal

# Um índice (ou *index*) de um objeto é a posição de um dado elemento guardado neste objeto. Em outras palavras: **índices são as posições dos elementos no objeto**, posições estas que são definidas pela ordem na qual foram inseridas no objeto. Referir-se à estas posições como *index* também é extremamente comum, então utilizaremos ambas formas.
# 
# Assim como é de praxe em programação, todos os índices de um objeto em Python começam em zero. Por causa disto, a contagem dos seus elementos encontra-se no intervalo $[0, n-1]$, ou seja, se uma dada lista possuir 10 elementos, o index do primeiro elemento será zero e o index do último será 9.
# 
# Quando acessamos um único ou vários elementos de um objeto através de suas posições, diz-se que estamos **indexando** este objeto. Acessamos os elementos específicos de uma lista com o operador `[]`, da seguinte forma:

# ```python
# nome_da_lista[index_do_elemento]
# ```

# Exemplo:

# In[1]:


# Criando uma lista nova:
compras = ["Arroz", "Feijão", "Leite", "Carne"]


# In[2]:


# Acessando o primeiro elemento (index zero):
print(compras[0])


# In[3]:


# Acessando o segundo elemento (index 1):
print(compras[1])


# In[4]:


# Acessando o terceiro elemento (index 2):
print(compras[2])


# In[5]:


# Acessando o quarto elemento (index 3):
print(compras[3])


# Se tentarmos acessar um index não existente, um erro será retornado:

# In[6]:


# Acessando o quinto elemento (index 4 — inexistente):
print(compras[4])


# O erro acima diz que há um erro de index (*IndexError*): Python tentou acessar o elemento no index especificado mas a lista em questão não possui tal posição. Assim, o index requisitado está fora do intervalo de elementos existentes (*list index out of range*).
# 
# Por isto, fique atento: o último elemento de uma lista é a quantidade de elementos menos um:
# 
# $\text{Total de elementos} = \left( n - 1 \right)$.
# 
# Podemos também criar novas variáveis com estes elementos indexados:

# In[7]:


# Criando uma nova variável chamada 'var':
var = compras[3]

print(var)


# Podemos verificar que não é uma lista, mas sim, uma string, pois estamos acessando este único elemento em si:

# In[8]:


print(type(var))


# E não há alteração alguma na lista original:

# In[9]:


print(compras)


# ## Indexação Negativa

# Outra forma de acessarmos os elementos de uma lista é através de índices negativos:

# In[10]:


nome_da_lista[-index_do_elemento]


# Aqui, os elementos serão acessados em sua ordem inversa: o primeiro index acessará o último elemento da lista e o último index acessará o primeiro elemento da lista.
# 
# Índices negativos não começam por zero, mas sim, por `-1` e terminam na quantidade negativa total de elementos. Assim, em index negativo, os elementos são acessados no intervalo $\left[ -1, -n \right]$

# In[28]:


# Acessando o último elemento (index -1):
print(compras[-1])


# In[29]:


# Acessando o penúltimo elemento (index -2):
print(compras[-2])


# In[30]:


# Acessando o antepenúltimo elemento (index -3):
print(compras[-3])


# In[31]:


# Acessando o primeiro elemento (index -4):
print(compras[-4])


# Assim, podemos fazer uma comparação entre as posições nas duas formas de indexação (os números representam os índices dos elementos):
# 
# - Normal:
# 
# $[0,1,2,3]$
# 
# - Negativa:
# 
# $[-4,-3,-2,-1]$
# 
# Assim como na indexação normal, recebemos o mesmo erro ao tentarmos selecionar um index negativo não existente:

# In[32]:


compras[-5]


# Há somente quatro elementos na lista `compras`, portanto não existe um index `-5`.
