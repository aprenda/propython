#!/usr/bin/env python
# coding: utf-8

# # Comparação de Identidade

# ## Operador `is`

# x is y is true if and only if x and y are the same object. An Object’s identity is determined using the id() function.

# ## Operador `is not`

# x is not y yields the inverse truth value. 
