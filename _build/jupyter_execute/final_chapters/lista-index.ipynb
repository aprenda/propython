{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "55522d55-f4d8-4514-9f83-1ea8e9a86523",
   "metadata": {},
   "source": [
    "# Indexação de Listas"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88908cbb-c558-459e-90bf-2c27ef0f5925",
   "metadata": {},
   "source": [
    "Sabemos que listas são objetos ordenados, portanto, seus elementos possuem ordem; cada um possui uma posição específica dentro da lista. Tal ordenação de elementos é imprescindível na manipulação das estruturas ordenadas: conseguimos acessar e manipular elementos especificando sua posição individual dentro de um objeto — um processo chamado **indexação**.\n",
    "\n",
    "Abaixo, conheceremos agora duas formas de indexação em Python: uma utilizando as posições normais de cada elemento e outra utilizando \"posições negativas\"."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "754820ed-d916-42eb-b43a-99afa81bf7ee",
   "metadata": {},
   "source": [
    "## Indexação Normal"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e85ae3b-3fe2-4c51-9fdd-9c4e314021c8",
   "metadata": {},
   "source": [
    "Um índice (ou *index*) de um objeto é a posição de um dado elemento guardado neste objeto. Em outras palavras: **índices são as posições dos elementos no objeto**, posições estas que são definidas pela ordem na qual foram inseridas no objeto. Referir-se à estas posições como *index* também é extremamente comum, então utilizaremos ambas formas.\n",
    "\n",
    "Assim como é de praxe em programação, todos os índices de um objeto em Python começam em zero. Por causa disto, a contagem dos seus elementos encontra-se no intervalo $[0, n-1]$, ou seja, se uma dada lista possuir 10 elementos, o index do primeiro elemento será zero e o index do último será 9.\n",
    "\n",
    "Quando acessamos um único ou vários elementos de um objeto através de suas posições, diz-se que estamos **indexando** este objeto. Acessamos os elementos específicos de uma lista com o operador `[]`, da seguinte forma:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "671f3871-92cf-4309-bdc7-3920ff2cdeaa",
   "metadata": {
    "tags": [
     "remove-ouput",
     "raises-exception"
    ]
   },
   "source": [
    "```python\n",
    "nome_da_lista[index_do_elemento]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2fb5cb6-9090-4a3c-aed4-5347b0fd86b7",
   "metadata": {},
   "source": [
    "Exemplo:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "a534581e-a1c2-426c-9488-d0cd2cd6a1c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Criando uma lista nova:\n",
    "compras = [\"Arroz\", \"Feijão\", \"Leite\", \"Carne\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "b7ddfa02-8934-4527-8e48-429fcd94b023",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Arroz\n"
     ]
    }
   ],
   "source": [
    "# Acessando o primeiro elemento (index zero):\n",
    "print(compras[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "8a2548a1-0cff-40df-a7ea-3ca1cbd0be4a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Feijão\n"
     ]
    }
   ],
   "source": [
    "# Acessando o segundo elemento (index 1):\n",
    "print(compras[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "a378d175-0a44-47f6-9720-04e6bd1c9091",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Leite\n"
     ]
    }
   ],
   "source": [
    "# Acessando o terceiro elemento (index 2):\n",
    "print(compras[2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "eff01fab-e774-4be1-ae1f-865731a5e8e6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Carne\n"
     ]
    }
   ],
   "source": [
    "# Acessando o quarto elemento (index 3):\n",
    "print(compras[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d0abf12-6a4d-4576-8711-2158118d2970",
   "metadata": {},
   "source": [
    "Se tentarmos acessar um index não existente, um erro será retornado:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "5c79a6fc-31d0-4499-88a2-9fbc08e9971c",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "IndexError",
     "evalue": "list index out of range",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mIndexError\u001b[0m                                Traceback (most recent call last)",
      "Input \u001b[0;32mIn [6]\u001b[0m, in \u001b[0;36m<cell line: 2>\u001b[0;34m()\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[38;5;66;03m# Acessando o quinto elemento (index 4 — inexistente):\u001b[39;00m\n\u001b[0;32m----> 2\u001b[0m \u001b[38;5;28mprint\u001b[39m(\u001b[43mcompras\u001b[49m\u001b[43m[\u001b[49m\u001b[38;5;241;43m4\u001b[39;49m\u001b[43m]\u001b[49m)\n",
      "\u001b[0;31mIndexError\u001b[0m: list index out of range"
     ]
    }
   ],
   "source": [
    "# Acessando o quinto elemento (index 4 — inexistente):\n",
    "print(compras[4])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bad7d060-57a8-4b52-9e7d-df2d0da61a95",
   "metadata": {},
   "source": [
    "O erro acima diz que há um erro de index (*IndexError*): Python tentou acessar o elemento no index especificado mas a lista em questão não possui tal posição. Assim, o index requisitado está fora do intervalo de elementos existentes (*list index out of range*).\n",
    "\n",
    "Por isto, fique atento: o último elemento de uma lista é a quantidade de elementos menos um:\n",
    "\n",
    "$\\text{Total de elementos} = \\left( n - 1 \\right)$.\n",
    "\n",
    "Podemos também criar novas variáveis com estes elementos indexados:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c29ce91b-451e-478c-8da0-24395634ff29",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Carne\n"
     ]
    }
   ],
   "source": [
    "# Criando uma nova variável chamada 'var':\n",
    "var = compras[3]\n",
    "\n",
    "print(var)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d936e6dd-aa17-41ec-8d85-1c496409bdf4",
   "metadata": {},
   "source": [
    "Podemos verificar que não é uma lista, mas sim, uma string, pois estamos acessando este único elemento em si:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "ceac7985-831a-438a-8da1-a0a7085a0e42",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "<class 'str'>\n"
     ]
    }
   ],
   "source": [
    "print(type(var))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "899d6ae0-99a7-4fad-819e-55e9d39b95a2",
   "metadata": {},
   "source": [
    "E não há alteração alguma na lista original:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "09001c26-7c0f-41e6-bc5c-60beffc9e0d5",
   "metadata": {
    "tags": []
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['Arroz', 'Feijão', 'Leite', 'Carne']\n"
     ]
    }
   ],
   "source": [
    "print(compras)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a6adddc-d3d4-478a-a90a-2718d91c26e5",
   "metadata": {},
   "source": [
    "## Indexação Negativa"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3058d244-8b7a-476e-ba85-9cbe9ff017e3",
   "metadata": {},
   "source": [
    "Outra forma de acessarmos os elementos de uma lista é através de índices negativos:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "247621ae-1dc5-4350-b06c-cc746859f7ed",
   "metadata": {
    "tags": [
     "remove-ouput"
    ]
   },
   "outputs": [
    {
     "ename": "NameError",
     "evalue": "name 'nome_da_lista' is not defined",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mNameError\u001b[0m                                 Traceback (most recent call last)",
      "Input \u001b[0;32mIn [10]\u001b[0m, in \u001b[0;36m<cell line: 1>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0m \u001b[43mnome_da_lista\u001b[49m[\u001b[38;5;241m-\u001b[39mindex_do_elemento]\n",
      "\u001b[0;31mNameError\u001b[0m: name 'nome_da_lista' is not defined"
     ]
    }
   ],
   "source": [
    "nome_da_lista[-index_do_elemento]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d824be3-474c-4ba7-a3a9-4544bd6200c6",
   "metadata": {},
   "source": [
    "Aqui, os elementos serão acessados em sua ordem inversa: o primeiro index acessará o último elemento da lista e o último index acessará o primeiro elemento da lista.\n",
    "\n",
    "Índices negativos não começam por zero, mas sim, por `-1` e terminam na quantidade negativa total de elementos. Assim, em index negativo, os elementos são acessados no intervalo $\\left[ -1, -n \\right]$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "id": "42edd486-bf4c-4e7b-b85e-5b4f3c0b0907",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Carne\n"
     ]
    }
   ],
   "source": [
    "# Acessando o último elemento (index -1):\n",
    "print(compras[-1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "id": "2d59c160-db56-4bf1-b032-7e3f3088f43f",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Leite\n"
     ]
    }
   ],
   "source": [
    "# Acessando o penúltimo elemento (index -2):\n",
    "print(compras[-2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "id": "6b83e3a3-8cdf-49a9-ac74-a40909ddb001",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Feijão\n"
     ]
    }
   ],
   "source": [
    "# Acessando o antepenúltimo elemento (index -3):\n",
    "print(compras[-3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "id": "e8ab2fa0-fa2c-48ca-af2e-be15653ef350",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Arroz\n"
     ]
    }
   ],
   "source": [
    "# Acessando o primeiro elemento (index -4):\n",
    "print(compras[-4])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "749ebdd4-e901-4e93-adeb-993f68fe36d5",
   "metadata": {},
   "source": [
    "Assim, podemos fazer uma comparação entre as posições nas duas formas de indexação (os números representam os índices dos elementos):\n",
    "\n",
    "- Normal:\n",
    "\n",
    "$[0,1,2,3]$\n",
    "\n",
    "- Negativa:\n",
    "\n",
    "$[-4,-3,-2,-1]$\n",
    "\n",
    "Assim como na indexação normal, recebemos o mesmo erro ao tentarmos selecionar um index negativo não existente:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "5acc49d0-395b-4af9-9d85-6ba74490c262",
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "ename": "IndexError",
     "evalue": "list index out of range",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m----------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mIndexError\u001b[0m                     Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-32-f7abc11be24d>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mcompras\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;34m-\u001b[0m\u001b[0;36m5\u001b[0m\u001b[0;34m]\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mIndexError\u001b[0m: list index out of range"
     ]
    }
   ],
   "source": [
    "compras[-5]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "904ac3f8-618c-4fbb-bdb4-3e4fea14c45f",
   "metadata": {},
   "source": [
    "Há somente quatro elementos na lista `compras`, portanto não existe um index `-5`."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}