#!/usr/bin/env python
# coding: utf-8

# # Exercícios

# ## Questão X

# XXXX

# In[1]:


"python" > "Python"


# In[2]:


"Python" > "python"


# ```{admonition} Resposta
# :class: dropdown
# 
# Strings (instances of str) compare lexicographically using the numerical Unicode code points (the result of the built-in function ord()) of their characters.
# ```
