#!/usr/bin/env python
# coding: utf-8

# # Comparação de Filiação

# ## Operador `in`

# x in s evaluates to True if x is a member of s, and False otherwise.

# ## Operador `not in`

# x not in s returns the negation of x in s.
# 
# The operator not in is defined to have the inverse truth value of in.
