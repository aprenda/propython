#!/usr/bin/env python
# coding: utf-8

# # Precedência de Operadores

# The following table summarizes the operator precedence in Python, from highest precedence (most binding) to lowest precedence (least binding). Operators in the same box have the same precedence. Unless the syntax is explicitly given, operators are binary.
# 
# Operators in the same box group left to right (except for exponentiation, which groups from right to left). **<--- ASSOCIATIVIDADE!**
# 
# Note that comparisons, membership tests, and identity tests, all have the same precedence and have a left-to-right chaining feature as described in the Comparisons section.
# 
# ```{note}
# A tabela abaixo não está completa. Ainda há vários operadores que veremos adiante. Atualizaremos à medida com que os encontrarmos.
# ```

# |Operador|Precedência|Obs|
# |:--|:--|:--|
# |`**`|Maior precedência|-|
# |`-X`|-|Operador `-` unário|
# |`*`, `/`, `//`, `%`|-|-|
# |`+`, `-`|-|Operadores aritméticos `+` e `-` binários|
# |`in`, `not in`, `is`, `is not`, `<`, `<=`, `>`, `>=`, `!=`, `==`|-|Comparações possuem mesma precedência|
# |`not`|-|-|
# |`and`|-|-|
# |`or`|Menor precedência|-|
