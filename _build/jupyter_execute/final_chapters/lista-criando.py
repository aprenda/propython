#!/usr/bin/env python
# coding: utf-8

# # Criando Listas

# Podemos criar listas através de duas formas:
# 
# 1. Sintaxe literal (*literal syntax*) com o operador `[]`:
#       - `[elemento1, elemento2, elemento3, ...]`
# 2. Função construtora `list()`, na qual passamos um iterável:
#       - `list(iteravel)`
# 
# Falaremos especificamente sobre objetos iteráveis quando discutirmos [estruturas de repetição](). Até lá, vamos nos ater a uma definição bem simples: iteráveis são objetos que nos permitem acessar todos seus elementos, um por um.

# ## Listas Vazias

# Vamos criar nossas primeiras listas. Primeiramente, criemos duas listas vazias, ou seja, com zero elementos:

# In[1]:


# Criando uma lista vazia com a sintaxe literal:
lst1 = []

print(lst1)


# In[2]:


# Criando uma lista vazia com a função construtora:
lst2 = list()

print(lst2)


# É de praxe nos referirmos a uma lista genérica com o nome `lst`, por isto o fizemos acima e o faremos no restante do livro.
# 
# Ambas listas foram criadas e no print nos é retornado somente o operador `[]` sem elemento algum, significando que a lista está vazia. Podemos verificar o tamanho das listas com a função `len()`, que nos retorna a quantidade de elementos de um objeto:

# In[3]:


# Tamanho da lista 1:
print(len(lst1))


# In[4]:


# Tamanho da lista 2:
print(len(lst2))


# Como já vimos, todos objetos vazios possuem valor booleano falso e listas não são exceções:

# In[5]:


print(bool(lst1))


# In[6]:


print(bool(lst2))


# ## Listas com Elementos

# Com já falamos, listas podem sim conter diferentes tipos de dados. Vamos criar uma lista com diferentes tipos de objetos:

# In[7]:


lst3 = [1, "string", False, 4.44]

print(lst3)


# Temos como elementos na lista acima um número inteiro, uma string, um valor booleano e um número float.
# 
# Verificando que estão de fato em um único objeto — a lista:

# In[8]:


print(type(lst3))


# Mencionamos também na introdução deste capítulo que gerelamente listas são utilizadas para guardar elementos homogêneos, ou seja, de mesmo tipo. Falemos sobre isso mais um pouco.
# 
# Listas são úteis para agrupar elementos independentes em um único lugar, mas se quisermos agrupar elementos que possuem alguma relação, provavelmente uma lista não é a melhor estrutura de dados.
# 
# Observe:

# In[9]:


# Lista para um indivíduo qualquer:
lst_ind = [
    "Nome", "João",
    "Idade", 28,
    "CNH", False
]

print(lst_ind)


# Esta lista acima não faz sentido algum se quiséssemos utilizar estes  dados. O que estes dados são? O que representam? A melhor estrutura de dados neste caso seriam dicionários — uma estrutura de dados ideal para mapeamentos, a qual veremos num [próximo capítulo]().
# 
# Um modo melhor de se utilizar uma lista seria, por exemplo, uma lista de nomes de usuários, por exemplo. Veja abaixo.

# In[10]:


# ID's de usuários:
lst_id = [
    "joaoaugusto", "maria_beltrao22",
    "pricardoso", "rlopes"
]


# Na variável `lst_id`, temos uma lista homogênea (todos elementos são strings) que, hipoteticamente, representa alguns usuários de uma dada rede social.

# ```{admonition} Dica
# Perceba no código acima: abrimos os colchetes em linhas separadas e colocamos os elementos da lista entre eles. Este é um modo eficaz de organizar seu código quando tem-se muitos elementos, pois é claro onde inicia-se a construção da lista; onde ela é finalizada; e todos seus elementos são facilmente visíveis.
# 
# Este modo de escrita pode ser feita em qualquer estrutura de dados, tanto é que a utilizaremos com todas as outras estruturas.
# ```

# E podemos também ver quantos elementos existem em `lst_id` com a função `len()`:

# In[11]:


print(len(lst_id))


# Assim, `lst_id` é uma variável que representa quatro usuários de uma rede social.
