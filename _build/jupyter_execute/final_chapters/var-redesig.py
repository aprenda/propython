#!/usr/bin/env python
# coding: utf-8

# # Re-designando Variáveis

# In[1]:


x = y = z = 4000


# Voltando às variáveis, podemos mudar o valor das mesmas após sua criação. Fazemos isto re-designando o nome da variável a um novo valor:

# In[2]:


print(f"Valor atual de x: {x}")


# Re-designando `x` (mudando o valor de `x`) e printando o novo valor:

# In[3]:


x = 333

print(f"Novo valor de x: {x}")


# Podemos fazer isto a qualquer momento e, até mesmo, designar uma variável a outra variável:

# In[4]:


# Criando uma variável "quantidade de pães":
qtd_paes = 6

print(qtd_paes)


# In[5]:


# Criando uma variável "quantidade de facas":
qtd_facas = qtd_paes

# Printando:
print(qtd_facas)


# Este é o mesmo caso que vimos anteriormente, quando designamos três variáveis ao mesmo vamor ao mesmo tempo. Vejamos se de fato são o mesmo:

# In[6]:


print(f"id(qtd_paes): {id(qtd_paes)}")


# In[7]:


print(f"id(qtd_facas): {id(qtd_facas)}")


# São nomes para o mesmo valor. E se mudarmos uma das variáveis?

# In[8]:


# Mudando o valor de 'qtd_paes':
qtd_paes = 500


# Vejamos seus valores agora:

# In[9]:


print(qtd_paes)


# In[10]:


print(qtd_facas)


# E seus locais na memória:

# In[11]:


print(id(qtd_paes))

print(id(qtd_facas))


# São diferentes. Então, podemos ligar os nomes de variáveis a outros valores, não influenciando as outras variáveis ligados ao mesmo valor anterior.
# 
# Podemos também criar várias variáveis a vários valores diferentes utilizando vírgulas para separá-los:

# In[12]:


tres, variaveis, diferentes = 5, 10, 15

print(tres, variaveis, diferentes)


# O que aconteceu:
# 
# Python ligou os três nomes diferentes (`tres`, `variaveis` e `diferentes`) a três valores diferentes (`5`, `10` e `15`), utilizando a respectiva ordem definida pelo usuário. Assim:
# 
# > tres = 5    
# > variaveis = 10    
# > diferentes = 15
# 
# Na realidade, o que está acontencendo é um *tuple unpacking*, mas falaremos disto em outro capítulo.
