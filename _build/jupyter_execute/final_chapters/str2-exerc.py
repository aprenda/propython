#!/usr/bin/env python
# coding: utf-8

# # Exercícios

# ```{admonition} **Tente Sozinho!**
# Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
# ```

# ## Questão 1

# Crie uma variável com a string `"martelo"`, inverta a ordem de seus caracteres (escreva a palavra de trás para a frente) e guarde em uma variável chamada `"inverso"`. Por fim, printe `"inverso"`.

# In[1]:


var = "martelo"

inverso = var[::-1]

print(inverso)


# ```{admonition} Resposta
# :class: dropdown
# 
# Listas são estruturas que agrupam um conjunto de elementos em um único objeto e caracterizam-se por serem mutáveis e ordenáveis.
# ```
