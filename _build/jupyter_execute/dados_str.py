#!/usr/bin/env python
# coding: utf-8

# ## Strings

# Tecnicamente, uma string é uma sequência de caracteres, mas basicamente, strings são textos. Enquanto em outras linguagens há diferenciação entre caracteres únicos e strings, como em C++: 
# 
# ```{admonition}Texto em C++
# Caracteres: 'a', 'b', 'c', '1', '2', '3'
# 
# String: "Alfabeto", "0123456789"
# ```
# 
# Em Python, não há esta diferenciação. Tudo que estiver entre aspas será uma string: um objeto da classe `str`.
# 
# Strings podem ser declaradas com aspas duplas ou simples, indiferentemente:

# In[1]:


print('Eu sou uma string com aspas simples!')


# In[2]:


print("Eu sou uma string com aspas duplas!")


# Até mesmo por aspas triplas:

# In[3]:


print("""Eu sou uma string com aspas triplas!""")


# E todas pertencem à classe `str`:

# In[4]:


print(type('Eu sou uma string com aspas simples!'))


# In[5]:


print(type("Eu sou uma string com aspas duplas!"))


# In[6]:


print(type("""Eu sou uma string com aspas triplas!"""))


# Aspas triplas geralmente são utilizadas para [docstrings](#docstrings) e comentários em várias linhas. Abaixo um exemplo disto: fazemos um simples print de $5+5$, mas antes há um grande texto entre aspas triplas.

# In[7]:


"""
Esta
é
uma
string
em
várias
linhas
que
também
serve
como
comentário,
ou
seja,
não
será
executado
pelo
Python.
"""

print(5 + 5)


# Contudo, por questões de legibilidade, é bom evitar comentários em várias linhas.
# 
# A única coisa importante a se lembrar é consistência: durante todo seu programa, você deve usar um único modo de declaração de strings. Não misture no programa aspas duplas com únicas.
# 
# Utilize ambas na mesma string somente quando sua string tiver de possui-las.
# 
# Exemplificando:

# In[8]:


print("Ciclano não ouviu Beltrano gritando: "PARE!"")


# In[ ]:


print('Ciclano não ouviu Beltrano gritando: 'PARE!'')


# Em ambas frases acima, há um erro de sintaxe: Python não entende que `"PARE!"` e `'PARE!'` ainda são parte da string. Ele pensa que a string acabou nas aspas anteriores à palavra e outra string iniciou-se após a palavra.
# 
# Para utilizarmos corretamente os dois tipos de aspas dentro de uma mesma string, há dois modos: i) usar um caracter de escape; ou ii) usar outro tipo de aspas.

# ### Caracter de Escape

# Caracteres de escape são iniciados com a barra invertida (`\`) e indicam ao computador que algo deve ser avaliado diferentemente do normal.
# 
# Já vimos um destes caracteres: a nova linha (`\n`). Quando utilizamos  barra invertida com a letra `n`, indicamos ao computador que naquele local do código deve ser posta uma quebra de linha. Sem a barra invertida (sem o escape), a letra `n` é apenas uma string.
# 
# Nossos caracteres de escape de interesse, quando se tratando de strings, são, principalmente, aspas duplas (`"`) e simples (`'`).
# 
# Ambas indicarão ao programa que as aspas antecedidas pela barra não são o término nem o início da string, mas sim, que fazem parte dela. Deste modo, o Python não irá terminar a avaliação da string erroneamente. Façamos o mesmo exemplo de antes com ambos tipos de strings:

# In[55]:


print("Ciclano não ouviu Beltrano gritando: \"PARE!\"")


# In[56]:


print('Ciclano não ouviu Beltrano gritando: \'PARE!\'')


# Agora não há mais erros.

# ### Alternando os Tipos de Aspas

# Um outro modo, mais simples e compreensível, de se fazer isto é alternar de aspas dentro da string. Assim, Python entenderá que a string não terminou.
# 
# Se utilizarmos aspas duplas na string, dentro dela devemos utilizar aspas simples:

# In[57]:


print("Ciclano não ouviu Beltrano gritando: 'PARE!'")


# E vice-versa:

# In[58]:


print('Ciclano não ouviu Beltrano gritando: "PARE!"')


# ### Valores Booleanos de Strings

# Qualquer string não-vazia possuirá valor booleano verdadeiro. Strings vazias são aquelas que não possuem nenhum caracter dentro das aspas. Podemos ver o tamanho de um objeto em Python com a função `len()`, que, no caso de strings, nos retorna a quantidade de caracteres da string:

# In[59]:


print(len("Python"))


# In[60]:


print(len(""))


# Acima, a string `"Python"` possui 6 caracteres, por isso não é vazia.
# 
# Diferentemente, a segunda string possui zero caracteres, sendo uma string vazia. Vejamos seus valores booleanos:

# In[ ]:


print(bool("Python"))
 
print(bool(""))


# Strings vazias sempre serão falsas. Importante ressaltar: espaços em branco (*whitespaces*) são um caracter, por isso, strings com estes espaços não são vazias:

# In[62]:


print(type(" "))


# In[63]:


print(bool(" "))

