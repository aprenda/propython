#!/usr/bin/env python
# coding: utf-8

# # Criando Listas

# Podemos criar listas através de:
# 
# 1. Sintaxe literal (*literal syntax*) com o operador `[]`:
#       - `[elemento1, elemento2, elemento3, ...]`
# 2. Função construtora `list()`, na qual passamos um iterável:
#       - `list(iteravel)`
# 
# Primeiramente, criemos duas listas vazias, ou seja, com zero elementos:

# In[1]:


# Criando uma lista vazia com a sintaxe literal:
lst1 = []

print(lst1)


# In[2]:


# Criando uma lista vazia com a função construtora:
lst2 = list()

print(lst2)


# É de praxe nos referirmos a uma lista com o nome `lst`, por isto o fizemos acima e o faremos no restante do livro.
# 
# Ambas listas foram criadas e no print nos é retornado somente o operador `[]` sem elemento algum, significando que a lista está vazia.
# 
# Podemos verificar o tamanho das listas:

# In[3]:


# Tamanho da lista 1:
print(len(lst1))


# In[4]:


# Tamanho da lista 2:
print(len(lst2))


# Como já vimos, todos objetos vazios possuem valor booleano falso e listas não são exceções:

# In[5]:


print(bool(lst1))


# In[6]:


print(bool(lst2))


# Listas aceitam diferentes tipos de objetos:

# In[7]:


lst3 = [1, "string", False, ("isto", "é", "uma", "tupla")]

print(lst3)


# Temos como elementos na lista acima um número inteiro, uma string, um valor booleano e uma tupla.
# 
# Verificando que estão de fato em um único objeto — a lista:

# In[8]:


print(type(lst3))


# Listas são úteis para agrupar elementos independentes em um único lugar, mas se quisermos agrupar elementos que possuem alguma relação, provavelemente uma lista não é a melhor estrutura de dados.
# 
# Observe:

# In[9]:


lst_individuo = [
    "Nome", "Zander",
    "Idade", 28,
    "CNH", False
]

print(lst_individuo)


# Esta lista acima não faz sentido algum se quiséssemos utilizar estes  dados. O que estes dados são? O que representam? A melhor estrutura de dados neste caso seriam dicionários — uma estrutura de dados ideal para mapeamentos, a qual veremos no próximo capítulo.

# Uma outra característica extremamente importante de listas é que elas são estrutura mutáveis, ou seja, após a criação de uma lista, é  possível modificá-la.
# 
# Vejamos:

# In[10]:


# Lista original:
print(lst_individuo)


# In[11]:


# Modificando:
lst_individuo[1] = 38


# In[12]:


# Lista final:
print(lst_individuo)

