#!/usr/bin/env python
# coding: utf-8

# # Função `print()`

# Também podemos ver o valor de uma variável com a função `print()`. Esta função retorna na tela do usuário o que for colocado como argumento. É comumente utilizado o anglicismo "printar" como verbo quando falamos que estamos utilizando esta função.
# 
# Abaixo vemos os valores das variáveis criadas acima:

# In[1]:


print(x)

print(y)

print(z)


# Vimos que variáveis são somente nomes para um determinado valor na memória. Portanto, acima, quando executamos:
# 
# > x = y = z = 4000
# 
# Não temos diferentes variáveis, mas sim, diferentes nomes para o mesmo valor na memória. Em outras palavras, `x`, `y` e `z` são referenciais para o mesmo local na memória.
# 
# Podemos verificar isto com a função `id()`. Esta função retorna o local na memória de uma variável. Vejamos o local na memória das três variáveis:

# In[6]:


print(id(x))

print(id(y))

print(id(z))


# De fato, os três nomes referem-se ao mesmo local na memória. Portanto, acabamos de ver que podemos ter quantos referenciais quisermos para um mesmo valor.

# Agora vejamos um pouco mais sobre a função `print()`: esta avalia seus argumentos e retorna um resultado único. Assim, podemos realizar operações aritméticas, por exemplo:

# In[4]:


print(12 + 4 + 4)


# "Printar" seria o mesmo que dizer:    
# *"Me dê o valor atual da variável `x` e adicione 8"*.
# 
# Importante! Neste cálculo acima, nunca dissemos para mudar o valor de `x`, apenas que "utilize seu valor atual".
# 
# Pela mesma razão, a função aceita uma mistura de variáveis e **literais** — valores fixos inseridos diretamente no código, como números:

# In[10]:


print(x + 12 * 4)


# Podemos printar várias variáveis de uma vez, com literais (texto) e as variáveis:

# In[11]:


print("x:", x, "y:", y, "z:", z)


# Com o argumento `sep`, podemos definir a separação entre os argumentos.
# 
# Separando com `***`:

# In[12]:


print("x:", x, "y:", y, "z:", z, sep="***")


# ## Printando com f-string

# Entretanto, Python 3 nos permite uma formatação bem melhor nesta função, chamada de `f-string`. Este modo de formatação nos permite uma *interpolação de literais de strings*, ou seja, permite uma mistura de strings com valores de uma única vez, e bem mais aprazível visualmente. Em outras palavras, é uma conversão implícita de tipos de dados para string.
# 
# Para utilizarmos f-strings, temos de colocar a letra `f` (de formatação) antes de aspas, dentro do print.
# 
# Exemplo:

# In[13]:


print(f"")


# Agora, dentro das aspas, colocamos o texto que desejamos, como nos prints acima, mas sem as vírgulas:

# In[5]:


print(f"x: y: z: ")


# Agora interpolamos os valores, ou seja, colocamos os valores na string. Para tal, necessitamos de colocar os valores a serem interpolados dentro de chaves:

# In[14]:


print(f"x: {x} y: {y} z: {z}")


# O que ocorrerá: Python irá printar na tela o texto sem modificação e substituirá o nome de cada variável por seu valor.
# 
# Assim, podemos melhorar ainda mais o print. Coloquemos vírgulas, a designação específica de cada valor e cada valor em uma linha diferente:

# In[15]:


print(f"\nVariável x: {x}\nVariável y: {y} \nVariável z: {z}\n")


# Acima, utilizamos um caractere de escape (*escape character*): `\n`. Ele diz para a função `print()` que deve ser colocado uma nova linha na tela, assim separamos cada variável em uma linha diferente e melhoramos sua visualização.
