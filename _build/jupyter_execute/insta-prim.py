#!/usr/bin/env python
# coding: utf-8

# # Primeiro Contato

# Agora que já instalamos Python, vamos ter nosso primeiro contato com esta linguagem de programação. Como de praxe, o primeiro comando (ou conjunto de comandos) que se faz ao aprender uma nova linguagem é o famigerado "Olá mundo!" (*Hello World!*). Isto se dá, provavelmente, por ser algo simples e que nos dá um vislumbre da sintaxe da linguagem em questão.
# 
# Em Python, este código é extremamente simples. Necessitamos apenas da função `print()` e de colocar algo dentro dos parênteses. No caso, este algo é a frase `"Olá, Mundo!"`, com aspas mesmo. Abaixo executamos o código:

# In[1]:


print("Olá, Mundo!")


# É visivelmente perceptível que Python é uma linguagem enxuta e pouco verborrágica em comparação com outras linguagens, como:

# - C:
# 
# ```c
# #include<stdio.h>
# 
# int main(void) {
#     printf("Olá, Mundo!\n");
#     return 0;
# }
# ```

# - C++:
# 
# ```c++
# #include <iostream>
# 
# using namespace std;
# 
# int main() {
#    cout << "Olá, Mundo!" << endl;
#    return 0;
# }
# ```

# - Java:
# 
# ```java
# public class java {
#     public static void main(String[] args) {
#         System.out.println("Olá, Mundo!");
#     }
# }
# ```
