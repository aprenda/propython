#!/usr/bin/env python
# coding: utf-8

# # Variáveis

# XXXX

# ## Definindo "Variável"

# Variáveis possuem um nome (às vezes chamados de identificador, como em C++) e um valor. Basicamente, variáveis são nomes para locais na memória que possuem um valor.
# 
# Variáveis necessitam ser designadas antes de serem utilizadas. Designação de variáveis também é conhecida como **ligação** (*binding*). Quando fazemos isto, estamos ligando um valor na memória a um determinado nome.
# 
# Ligação feita, a designação da variável está completa e podemos nos referir a ela por seu nome. Ao fazer isto, o computador saberá aonde olhar pela informação na memória, através do nome da variável.

# ## Designação

# A sintaxe da designação de uma variável em Python é:

# In[1]:


nome_var = valor


# Onde:
# 
# - `nome_var`: nome da variável
# - `=`: operador para designação (*assignment operator*)
# - `valor`: valor ligado à variável
# 
# Exemplo:

# In[1]:


x = 100


# Designamos o valor `100` para a variável `x`.
# 
# Em Python, é mais fácil pensarmos em variável como nomes: são nomes que se referem a valores. Assim, no exemplo acima, o nome `x` (a variável `x`) refere-se ao valor `100` na memória do computador.
# 
# Ao nomearmos uma variável, devemos ter uma melhor semântica nas variáveis, ou seja, as variáveis devem ter nomes compreensíveis e claros em seu propósito:

# In[2]:


# Uma variável guardando o número de canetas
numero_canetas = 12


# E podemos ter vários tipos de valores nas variáveis, um tema que veremos em detalhes no próximo [capítulo](#tipos_dados).
# 
# > x = "string"    
# > y = False    
# > z = 12.56
# 
# Também podemos designar várias variáveis de uma única vez:

# In[7]:


x = y = z = 4000


# ## Função `print()`

# Também podemos ver o valor de uma variável com a função `print()`. Esta função retorna na tela do usuário o que for colocado como argumento. É comumente utilizado o anglicismo "printar" como verbo quando falamos que estamos utilizando esta função.
# 
# Abaixo vemos os valores das variáveis criadas acima:

# In[8]:


print(x)

print(y)

print(z)


# Vimos que variáveis são somente nomes para um determinado valor na memória. Portanto, acima, quando executamos:
# 
# > x = y = z = 4000
# 
# Não temos diferentes variáveis, mas sim, diferentes nomes para o mesmo valor na memória. Em outras palavras, `x`, `y` e `z` são referenciais para o mesmo local na memória.
# 
# Podemos verificar isto com a função `id()`. Esta função retorna o local na memória de uma variável. Vejamos o local na memória das três variáveis:

# In[6]:


print(id(x))

print(id(y))

print(id(z))


# De fato, os três nomes referem-se ao mesmo local na memória. Portanto, acabamos de ver que podemos ter quantos referenciais quisermos para um mesmo valor.

# Agora vejamos um pouco mais sobre a função `print()`: esta avalia seus argumentos e retorna um resultado único. Assim, podemos realizar operações aritméticas, por exemplo:

# In[4]:


print(12 + 4 + 4)


# "Printar" seria o mesmo que dizer:    
# *"Me dê o valor atual da variável `x` e adicione 8"*.
# 
# Importante! Neste cálculo acima, nunca dissemos para mudar o valor de `x`, apenas que "utilize seu valor atual".
# 
# Pela mesma razão, a função aceita uma mistura de variáveis e **literais** — valores fixos inseridos diretamente no código, como números:

# In[10]:


print(x + 12 * 4)


# Podemos printar várias variáveis de uma vez, com literais (texto) e as variáveis:

# In[11]:


print("x:", x, "y:", y, "z:", z)


# Com o argumento `sep`, podemos definir a separação entre os argumentos.
# 
# Separando com `***`:

# In[12]:


print("x:", x, "y:", y, "z:", z, sep="***")


# ### Printando com f-string

# Entretanto, Python 3 nos permite uma formatação bem melhor nesta função, chamada de `f-string`. Este modo de formatação nos permite uma *interpolação de literais de strings*, ou seja, permite uma mistura de strings com valores de uma única vez, e bem mais aprazível visualmente. Em outras palavras, é uma conversão implícita de tipos de dados para string.
# 
# Para utilizarmos f-strings, temos de colocar a letra `f` (de formatação) antes de aspas, dentro do print.
# 
# Exemplo:

# In[13]:


print(f"")


# Agora, dentro das aspas, colocamos o texto que desejamos, como nos prints acima, mas sem as vírgulas:

# In[5]:


print(f"x: y: z: ")


# Agora interpolamos os valores, ou seja, colocamos os valores na string. Para tal, necessitamos de colocar os valores a serem interpolados dentro de chaves:

# In[14]:


print(f"x: {x} y: {y} z: {z}")


# O que ocorrerá: Python irá printar na tela o texto sem modificação e substituirá o nome de cada variável por seu valor.
# 
# Assim, podemos melhorar ainda mais o print. Coloquemos vírgulas, a designação específica de cada valor e cada valor em uma linha diferente:

# In[15]:


print(f"\nVariável x: {x}\nVariável y: {y} \nVariável z: {z}\n")


# Acima, utilizamos um caractere de escape (*escape character*): `\n`. Ele diz para a função `print()` que deve ser colocado uma nova linha na tela, assim separamos cada variável em uma linha diferente e melhoramos sua visualização.

# ## Re-designando Variáveis

# Voltando às variáveis, podemos mudar o valor das mesmas após sua criação. Fazemos isto re-designando o nome da variável a um novo valor:

# In[9]:


print(f"Valor atual de x: {x}")


# Re-designando `x` (mudando o valor de `x`) e printando o novo valor:

# In[6]:


x = 333

print(f"Novo valor de x: {x}")


# Podemos fazer isto a qualquer momento e, até mesmo, designar uma variável a outra variável:

# In[7]:


# Criando uma variável "quantidade de pães":
qtd_paes = 6

print(qtd_paes)

# Criando uma variável "quantidade de facas":
qtd_facas = qtd_paes

# Printando:
print(qtd_facas)


# Este é o mesmo caso que vimos anteriormente, quando designamos três variáveis ao mesmo vamor ao mesmo tempo. Vejamos se de fato são o mesmo:

# In[8]:


print(f"id(qtd_paes): {id(qtd_paes)}")

print(f"id(qtd_facas): {id(qtd_facas)}")


# São nomes para o mesmo valor. E se mudarmos uma das variáveis?

# In[10]:


# Mudando o valor de 'qtd_paes':
qtd_paes = 500


# Vejamos seus valores agora:

# In[11]:


print(qtd_paes)

print(qtd_facas)


# E seus locais na memória:

# In[12]:


print(id(qtd_paes))

print(id(qtd_facas))


# São diferentes. Então, podemos ligar os nomes de variáveis a outros valores, não influenciando as outras variáveis ligados ao mesmo valor anterior.
# 
# Podemos também criar várias variáveis a vários valores diferentes utilizando vírgulas para separá-los:

# In[13]:


tres, variaveis, diferentes = 5, 10, 15

print(tres, variaveis, diferentes)


# O que aconteceu:
# 
# Python ligou os três nomes diferentes ('tres', 'variaveis', 'diferentes') a três valores diferentes (5, 10, 15), utilizando a respectiva ordem definida pelo usuário. Assim:
# 
# > tres = 5    
# > variaveis = 10    
# > diferentes = 15
# 
# Na realidade, o que está acontencendo é um *tuple unpacking*, mas falaremos disto em outro capítulo.

# ## Nomeando Variáveis

# ### Restrições em Nomeação

# Em Python, há algumas restrições de nomeação de variáveis:

# 1. Variáveis têm de começar com uma letra ou underscore:
# 
# - Forma correta:
# 
# > gatos = 2    
# > _gatos = 2
# 
# - Forma errada:
# 
# > 8gatos = 2    
# > .gatos = 2

# 2. O restante do nome tem de consistir de letras, números ou underscores:
# 
# - Forma correta:
# 
# > gatos8 = 2
# 
# - Forma errada:
# 
# > ola@voce = 7    
# > nome+2=9
# 
# Neste último exemplo:
# 
# > nome+2=9
# 
# Python acha que estamos tentando designar o número `9` ao operador `+`. Entretanto, este operador já possui outros significados em Python, por isto o erro.

# 3. Nomes são *case-sensitive*, ou seja, há diferenciação entre letras maiúsculas e minúsculas.
# 
# Uma variável com o nome `PAMONHA` é diferente da variável `Pamonha` e da variável `pamonha`.

# ### Convenções de Nomeação (Boas Práticas de Nomeação)

# 1. Use underscores para separar palavras:
# 
# Há projetos nos quais os programadores utilizam o chamado *camelCase* para separar as palavras e outros que utilizam underscores. Você deve seguir o formato já estipulado, se o houver. Se não, dê preferência ao
# underscore:
# 
# Em *camelCase*, inicia-se a variável com uma letra minúscula e diferenciam-se as próximas com maiúsculas.
# 
# Veja comparações destas nomeações na tabela abaixo:
# 
# | Underscore         | camelCase        |
# | :----------------: | :--------------: |
# | torta_morango      | tortaMorango     |
# | tv_plasma          | tvPlasma         |
# | uma_longa_variavel | umaLongaVariavel |
# 

# 2. As variáveis devem ser escritas em letras minúsculas, com exceções:
# 
# Variáveis normais devem ser escritas totalmente em letras minúsculas. Variáveis em letras maiúsculas são aplicadas normalmente somente para indicar constantes - valores que não mudarão durante o programa
# 
# Nomes escritos com letras maiúsculas e minúsculas são aplicados somente para indicar classes — um tipo de objeto mais avançado em Python.
# 
# A tabela abaixo apresenta exemplos destas nomeações:
# 
# | Constante | Classes           | Variáveis Normais |
# | :-------: | :---------------: | :---------------: |
# | PI        | FabricaAutomotiva | numero_carros     |
# | EULER     | AplicativoWeb     | qtd_cartas        |
# | FPS       | Calendario        | tempo_final       |
# 

# 3. Não inicie variáveis com underscores ou underscores duplos:
# 
# Python possui uma filosofia diferente de outras linguagens de programação: não há nada que impeça alguém de ver e/ou modificar seu código. Outras linguagens de programação como C, C++ e Java possuem variáveis e métodos (funções) privados, significando que a própria linguagemd e programação impede que ninguém além do programador possa vê-los ou alterá-los. Em Python, a filosofia é de um acordo consensual implícito entre os desenvolvedores: se há uma variável ou método indicado como privado, ela não deve ser mexida, mas a linguagem Python não impedirá isto.
# 
# Em Python indicamos que algo é privado colocando um underscore antes do nome da variável. Assim, outros programadores saberão que aquele objeto não deve ser modificado.
# 
# Em Python, também há métodos dentro de classes nos quais underscores duplos são utilizados antes e depois dos nomes. Estes são chamados de métodos dunder (**D**ouble **UNDER**scores). Também não devem ser mexidos.
# 
# Portanto, variáveis normais não devem ser comumente iniciadas com underscores, a não ser que queira indicar que são privadas.
# 
# A tabela abaixo apresenta exemplos de nomes com underscores:
# 
# | Métodos Dunder | Variáveis/funções Privadas | Variáveis/funções Normais |
# | :------------: | :------------------------: | :-----------------------: |
# | \_\_init\_\_() | _qtd_inicial               | qtd_inicial               |
# | \_\_len\_\_()  | _lista                     | lista                     |
# | \_\_repr\_\_() | _ordenar_por_tamanho()     | ordenar_elementos()       |

# 4. Os nomes das variáveis devem ser descritivos:
# 
# Como dissemos anteriormente, nomes de variáveis (ou funções, classes, ...) devem ser explícitos. Variáveis devem dizer claramente sua função.
# 
# Ao invés de:
# 
# > usuario = 190
# 
# Use:
# 
# > id_usuario = 190

# 5. Evite acentuação ao nomear as variáveis:
# 
# Acentos podem causar erros aos programas, mesmo que forem aceitos. Evite sempre acentuações.

# ### Palavras Reservadas

# Em Python, há diversas palavras que não podemos utilizar como nomes para variáveis, uma vez que são próprias da linguagem. Se tentarmos, será retornado um erro. Algumas destas são:
# 
# - for
# - while
# - not
# - is
# - None
# - True
# - False
# - if
# - elif
# - else
# - return
# 
# Exemplo de erro:

# In[ ]:


return = 2

