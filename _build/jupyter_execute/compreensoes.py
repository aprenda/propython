#!/usr/bin/env python
# coding: utf-8

# # Compreensões

# ## Compreensão de Listas

# Um outro modo fácil e mais eficiente de se criar listas é utilizando a compreensão de listas: um `for` dentro da sintaxe literal.

# In[1]:


[variavel for variavel in iteravel]


# Vamos criar uma lista simples de `0` a `9`. Ao invés de:

# In[1]:


lst = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


# Podemos automatizar com `for` e uma compreensão de listas:

# In[2]:


lst = [numero for numero in range(10)]

print(lst)


# O `for` nesta sintaxe é o mesmo que:

# In[ ]:


for numero in range(10):
    numero # Este é o primeiro 'numero'


# O primeiro `numero` refere-se aos elementos que serão adicionados na nova lista.
# 
# Podemos fazer compreensões de listas com qualquer iterável:

# In[3]:


lst = [letra for letra in "palavra"]

print(lst)


# ## Compreensão de Dicionários

# ## Compreensão de Conjuntos
