#!/usr/bin/env python
# coding: utf-8

# # While Loop
# 
# Como última lição sobre esta estrutura: 'for' loops são casos 
# específicos de 'while' loops.
# 
# ## WHILE ----------------------------------------------------------------
# 
# # 'while' é uma das implementações de loops controlados por condição em
# # em linguagens de programação. Em Python, esta é a palavra-chave para
# # realizar este tipo de loop.
# 
# # Sintaxe de 'while':
# 
# while expressao:
#     codigo
# 
# # Onde:
# # - expressao: expressão condicional de controle. Enquanto esta expressão
# #              for verdadeira, todo o código será executado continuamente
# # - codigo: código a ser repetido enquanto 'expressao' for verdadeira
# 
# # Como funciona:
# # O 'while' será iniciado na determinação do valor booleano de 'expressao'
# # e, se for verdadeiro, executará todo o código. Ao chegar ao final do 
# # código, Python avaliará se a expressão ainda é verdadeira. Se não for,
# # o loop será terminado; se ainda for verdadeira, Python voltará à 
# # primeira linha do código dentro do loop e executará tudo novamente.
# 
# # Em vários casos, 'while' necessita de uma variável auxiliar para ser
# # utilizada na avaliação da condição. Um exemplo: vamos criar uma 
# # variável com valor booleano falso:
# 
# var = None
# 
# # Agora utilizemos esta variável em uma expressão de comparação:
# 
# var != "Python"
# 
# # Como possuem valores diferentes, esta expressão é avaliada como 'True'.
# # Utilizemos esta variável em 'while':
# 
# while var != "Python":
#     var = input("Digite uma linguagem de programação geral:\n> ")
# 
# 
# # No loop acima, enquanto a expressão 'var != "Python"' for verdadeira,
# # o loop continuará. Dentro do loop, estamos pedindo para que o usuário
# # digite uma linguagem de programação geral. Enquanto o usuário não
# # digitar exatamente a palavra "Python", o loop continuará. Quando
# # o usuário digitar "Python", a expressão se tornará falsa e o loop
# # será terminado.
# 
# # A linha 138 é extremamente importante, pois ela atualiza a variável 
# # 'var'. Sem esta nós teríamos um loop infinito, no qual o loop nunca
# # é finalizado e roda continuamente, até que o computador o pare por
# # falta de memória ou qualquer outro erro ou o usuário o cancele 
# # manualmente.
# 
# # Exemplo de loop infinito:
# 
# var = None
# 
# while var != "Python":
#     outra_var = input("Digite uma linguagem de programação geral:\n> ")
# 
# # É impossível Python sair deste loop, pois a variável 'var' nunca será
# # modificada. Dentro do código de 'while' estamos atualizando a variável
# # 'outra_var' ao invés de 'var'. Esta é uma questão extremamente 
# # importante em loops por condição: lembre-se de atualizar as variáveis
# # da expressão condicional para que não haja loops infinitos!
# 
# # Outros exemplos de while:
# 
# n = 0
# 
# while n < 5:
#     print(f"{n} ** 2 = {n ** 2}")
#     n += 1
# 
# 
# 
# m = 2
# 
# while m >= -2:
#     
#     print("'m' ainda não é -3")
# 
#     m -= 1
# 
# print(f"m = {m}")
# 
# 
# 
# # WHILE LOOP ANINHADO --------------------------------------------------
# 
# # Assim como blocos condicionais, loops também podem ser aninhados uns
# # dentro dos outros. Nestes casos, haverão loops dentro de loops. Um
# # bom método de vermos como funcionam estes loops é printando números
# # ou strings. Aqui exemplificarei while loops aninhados com strings.
# 
# # Vamos criar duas listas de strings:
# 
# lst1 = ["Leite", "Queijo"]
# 
# lst2 = ["Vaca", "Cabra", "Ovelha", "Búfala"]
# 
# 
# # Primeiramente, vamos iterar sobre cada uma das listas e printar seus
# # respectivos elementos.
# 
# # Iterando sobre 'lst1':
# 
# x = 0
# 
# while x < len(lst1):
#     
#     print(lst1[x])
# 
#     x += 1
# 
# 
# 
# # Iterando sobre 'lst2':
# 
# x = 0
# 
# while x < len(lst2):
#     
#     print(lst2[x])
# 
#     x += 1
# 
# 
# 
# # Agora, vamos criar um loop aninhado:
# 
# x = 0
# 
# while x < len(lst1):
# 
#     y = 0
# 
#     while y < len(lst2):
#         pass
# 
# 
# # Antes de printarmos, vamos entender o que está acontecendo:
# # Na linha 322, criamos o primeiro loop - chamado "loop externo", pois é 
# # o loop que está mais perto do escopo global em relação ao loop interno.
# # O loop da linha 326 é o "loop interno", pois está aninhado dentro do
# # loop externo. Na linha 327, o comando 'pass' significa que ainda 
# # implementaremos o código, para que o Python não levante erros. As
# # variáveis 'x' e 'y' monitoram a condição de parada de cada loop de
# # acordo com os tamnhos das listas 'lst1' e 'lst2', respectivamente.
# # A variável 'y' está dentro do loop pois ela tem de ser reiniciada
# # a cada iteração para podermos iterarmos sobre as duas listas sem erros.
# 
# # Vamos agora printar. Nosso objetivo aqui é iterar sobre as duas listas
# # ao mesmo tempo. Loops aninhados servem para iterações simultâneas.
# 
# x = 0
# 
# while x < len(lst1):
# 
#     y = 0
#     
#     while y < len(lst2):
#         
#         print(f"{lst1[x]} de {lst2[y]}")
#         
#         y +=1
# 
#     x += 1
# 
# 
# # Com estes prints, podemos perceber claramente o que está acontecendo:
# # Entramos no loop externo (o primeiro loop) na posição zero da lista,
# # pois seu valor é zero. Esta posição é a string "Leite". Desta forma,
# # enquanto 'x' for zero, printaremos "Leite". 
# 
# # Dentro do primeiro loop, criamos a variável 'y' que monitorará as 
# # posições da segunda lista. Após, criamos um loop de iteração para
# # a segunda lista (o loop interno). Dentro deste loop, atualizamos a
# # variável 'y' para que possamos acessar todos seus elementos e não
# # fique em um loop infinito. Após toda a iteração da segunda lista,
# # atualizamos o valor de 'x' para que acessemos o segundo elemento da
# # primeira lista.
# 
# 
# # Vejamos o que ocorre a cada iteração:
# 
# # Iteração 1: x=0 e y=0
# #             lst1[0] = "Leite" e lst2[0] = "Vaca"
# #             print: "Leite de Vaca"
# 
# # Iteração 2: x=0 e y=1
# #             lst1[0] = "Leite" e lst2[1] = "Cabra"
# #             print: "Leite de Cabra"
# 
# # Iteração 3: x=0 e y=2
# #             lst1[0] = "Leite" e lst2[2] = "Ovelha"
# #             print: "Leite de Ovelha"
# 
# # Iteração 4: x=0 e y=3
# #             lst1[0] = "Leite" e lst2[3] = "Búfala"
# #             print: "Leite de Búfala"
# 
# # Iteração 5: x=1 e y=0
# #             lst1[0] = "Queijo" e lst2[0] = "Vaca"
# #             print: "Queijo de Vaca"
# 
# # Iteração 6: x=1 e y=1
# #             lst1[1] = "Queijo" e lst2[1] = "Cabra"
# #             print: "Queijo de Cabra"
# 
# # Iteração 7: x=1 e y=2
# #             lst1[1] = "Queijo" e lst2[2] = "Ovelha"
# #             print: "Queijo de Ovelha"
# 
# # Iteração 8: x=1 e y=3
# #             lst1[1] = "Queijo" e lst2[3] = "Búfala"
# #             print: "Queijo de Búfala"
# 
# 
# # Ao todo, tivemos 8 iterações, mas, especificamente, o loop exterior
# # iterou apenas duas vezes, enquanto o loop interior iterou oito (quatro
# # vezes para cada iteração do loop exterior).
# 
# 
# 
# # BREAK E CONTINUE -----------------------------------------------------
# 
# # Apesar do que dissemos antes, loops infinitos são muito utilizados em 
# # vários casos. Por exemplo: o caso de querermos garantir que o usuário 
# # digite a senha correta. Enquanto ele não digitar a senha correta, 
# # o loop pedindo a senha continuará a ser executado. Poderíamos 
# # implementar esta situação da seguinte maneira:
# 
# while True:
#     
#     senha = input("\nDigite a senha:\n> ")
# 
#     if senha == "UFG!":
#         break
# 
# 
# # Acima, temos uma palavra-chave nova: 'break' e uma nova atribuição
# # com 'True'.
# 
# # O valor booleano 'True' cria um loop infinito: 'True' é, e sempre 
# # será, verdadeiro. Ao utilizarmos 'True', precisamos de algo que quebre
# # o loop infinito DENTRO do loop. Esta é a função de 'break'. 
# 
# # O comando 'break' para totalmente o loop mais pŕoximo a ele. Após o
# # 'break' parar um loop, o fluxo do programa volta para o loop mais
# # externo (se for um caso de loops aninhados) ou continua com o 
# # código após loop. Exemplificando:
# 
# n = 0
# 
# while n < 11 :
# 
#     if n == 4:
#         break
# 
#     print(n)
# 
#     n += 1
# 
# print("Depois do loop")
# 
# # O código acima printa a variável 'n' toda iteração. Contudo, há um
# # bloco condicional que verifica se 'n' é igual ao número 4. Se sim,
# # o loop é acabado pelo 'break' e o fluxo volta ao normal, printando
# # a linha 453. Se não houvesse a condicional, este loop printaria
# # todos os números de 0 a 10.
# 
# # Exemplificando com um loop aninhado da seção anterior:
# 
# lst1 = ["Leite", "Queijo"]
# 
# lst2 = ["Vaca", "Cabra", "Ovelha", "Búfala"]
# 
# x = 0
# 
# while x < len(lst1):
# 
#     y = 0
#     
#     while y < len(lst2):
# 
#         if lst2[y] == "Ovelha":
#             break
#         
#         print(f"{lst1[x]} de {lst2[y]}")
#         
#         y +=1
# 
#     x += 1
# 
# 
# # Diferentemente do exemplo aninhado da seção anterior, não printamos 
# # tudo aqui. Isto aconteceu por causa do bloco condicional com o 
# # 'break': quando o loop chega na posição da string "Ovelha", o loop
# # interno é terminado pelo 'break' e o fluxo volta diretamente ao loop
# # mais externo a ele. Neste caso, ele volta ao loop da linha 469, 
# # atualiza a variável 'x', volta ao inicíio deste loop, reinicia a 
# # variável 'y' para zero novamente e cria o novo loop interno. Por esta
# # razão, printamos somente duas strings do loop interno.
# 
# 
# # Vimos o comando 'break', agora, vejamos o comando 'continue': este
# # comando não acaba com um loop, mas sim, pula uma iteração do loop
# # ao qual ele está ligado. Exemplo:
# 
# n = 0
# 
# while n < 11 :
# 
#     n += 1
# 
#     if not (n % 2):
#         continue
# 
#     print(n)
# 
# print("Depois do loop")
# 
# 
# # O código acima NÃO printa a variável 'n' toda iteração. Primeiramente,
# # atualizamos a variável 'n' e verificamos a condicional: se 'n' for
# # par, "continuamos" para a próxima iteração, ou seja, a iteração atual
# # é imediatamente pulada para a pŕoxima iteração. É importante entender
# # que não acabamos com o loop! Apenas pulamos para a próxima rodada.
# # Deste modo, este código printa somente os ímpares. 
# 
# # Exemplificando com um loop aninhado da seção anterior:
# 
# lst1 = ["Leite", "Queijo"]
# 
# lst2 = ["Vaca", "Cabra", "Ovelha", "Búfala"]
# 
# x = 0
# 
# while x < len(lst1):
# 
#     y = -1
#     
#     while y < len(lst2):
# 
#         y +=1
#         
#         if lst2[y] == "Ovelha":
#             continue
#         
#         print(f"{lst1[x]} de {lst2[y]}")
# 
#     x += 1
# 
# 
# 
# # Por fim, à título de curiosidade, não necessariamente precisaríamos de 
# # 'True' no primeiro exemplo:
# 
# senha = None
# 
# while senha != "UFG!":
#     senha = input("\nDigite algo:\n> ")
# 
# 
# 
# # EXEMPLO 1 ------------------------------------------------------------
# 
# # Varrer todos os valores de x:
# x = 0
# 
# while x <= 2:
# 
#     # varrer os valores de y, para cada valor de x
#     y = 0
# 
#     while y <= 3:
#         
#         print(f"x = {x}, y = {y}")
#         
#         y = y + 1
# 
#     print("\n-----------------------------------\n")
# 
#     x += 1
# 
# 
# 
# # EXEMPLO 2 ------------------------------------------------------------
# 
# # Inicialização: intervalo da varredura
# x_ini = 0
# x_fim = 2
# 
# y_ini = 0
# y_fim = 3
# 
# # Inicialização: cálculo do ponto de máximo
# x_max = 0
# y_max = 0
# v_max = y_ini + x_ini * y_ini - x_ini * x_ini
# 
# # Varredura dos pontos no intervalo
# x = x_ini
# 
# while x <= x_fim:
#     
#     # varrer os valores de y, para cada valor de x
#     y = y_ini
#     
#     while y <= y_fim:
#         
#         valor = y + x*y - x*x
#         
#         if valor > v_max:
#         
#             v_max = valor
#         
#             x_max = x
#         
#             y_max = y
# 
#         y = y + 1
#     
#     x += 1
# 
# print(f"Valor máximo: {v_max}")
# print(f"Obtido no ponto: ({x_max}, {y_max})")
# 
# 
# 
# ## Comparação entre For e While {-}
# 
# #  Tudo que podemos fazer com 'for' podemos 
# # fazer com  'while'.
# 
# 
# # Exemplo 1:
# 
# # 'for':
# for x in range(11):
#     print(x)
# 
# 
# # 'while':
# 
# x = 0
# 
# while x < 11:
#     print(x)
#     x += 1
# 
# 
# 
# # Exemplo 2:
# 
# # 'for':
# 
# soma = 0
# 
# for x in range(0, 11, 2):
#     soma += x
# 
# 
# # 'while':
# 
# soma = 0
# 
# x = 0
# 
# while x < 11:
#     
#     soma += x
# 
#     x += 2
# 
# 
# 
# # Exemplo 3:
# 
# # 'for:
# 
# for x in range(11):
#     print("*" * x)
# 
# 
# # 'while':
# 
# x = 0
# 
# while x < 11:
#     
#     print("*" * x)
# 
#     x += 1
# 
# 
# # Exemplo 4:
# 
# numeros = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# 
# # 'for':
# 
# for i in numeros:
#     print(i)
# 
# 
# 
# # 'while':
# 
# i = 0
# 
# while i < len(numeros):
#     
#     print(numeros[i])
# 
#     i += 1
