#!/usr/bin/env python
# coding: utf-8

# ## Re-designando Variáveis

# Voltando às variáveis, podemos mudar o valor das mesmas após sua criação. Fazemos isto re-designando o nome da variável a um novo valor:

# In[1]:


print(f"Valor atual de x: {x}")


# Re-designando `x` (mudando o valor de `x`) e printando o novo valor:

# In[6]:


x = 333

print(f"Novo valor de x: {x}")


# Podemos fazer isto a qualquer momento e, até mesmo, designar uma variável a outra variável:

# In[7]:


# Criando uma variável "quantidade de pães":
qtd_paes = 6

print(qtd_paes)

# Criando uma variável "quantidade de facas":
qtd_facas = qtd_paes

# Printando:
print(qtd_facas)


# Este é o mesmo caso que vimos anteriormente, quando designamos três variáveis ao mesmo vamor ao mesmo tempo. Vejamos se de fato são o mesmo:

# In[8]:


print(f"id(qtd_paes): {id(qtd_paes)}")

print(f"id(qtd_facas): {id(qtd_facas)}")


# São nomes para o mesmo valor. E se mudarmos uma das variáveis?

# In[10]:


# Mudando o valor de 'qtd_paes':
qtd_paes = 500


# Vejamos seus valores agora:

# In[11]:


print(qtd_paes)

print(qtd_facas)


# E seus locais na memória:

# In[12]:


print(id(qtd_paes))

print(id(qtd_facas))


# São diferentes. Então, podemos ligar os nomes de variáveis a outros valores, não influenciando as outras variáveis ligados ao mesmo valor anterior.
# 
# Podemos também criar várias variáveis a vários valores diferentes utilizando vírgulas para separá-los:

# In[13]:


tres, variaveis, diferentes = 5, 10, 15

print(tres, variaveis, diferentes)


# O que aconteceu:
# 
# Python ligou os três nomes diferentes ('tres', 'variaveis', 'diferentes') a três valores diferentes (5, 10, 15), utilizando a respectiva ordem definida pelo usuário. Assim:
# 
# > tres = 5    
# > variaveis = 10    
# > diferentes = 15
# 
# Na realidade, o que está acontencendo é um *tuple unpacking*, mas falaremos disto em outro capítulo.
