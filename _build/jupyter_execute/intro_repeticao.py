#!/usr/bin/env python
# coding: utf-8

# # Introdução à Loops

# ## Loops, Iterações e Iteráveis

# Comecemos com um exemplo trivial: queremos printar os números no intervalo de $[1,10]$. Poderíamos fazer isto de algumas formas:
# 
# 1. Vários prints:

# In[1]:


print(1)
print(2)
print(3)
print(4)
print(5)
print(6)
print(7)
print(8)
print(9)
print(10)


# 2. Um único print:

# In[2]:


print(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)


# De qualquer forma, tivemos que fazer um código totalmente inviável, pois, além de termos escrito várias linhas desnecessariamente, este código não é dinâmico: toda vez que quisermos executar este código, teríamos de mudar número por número e remover ou adicionar linhas, de acordo com o necessário.
# 
# Programação é **automatização**; fazer com que uma tarefa seja dinâmica. Exatamente o oposto dos códigos acima. Como fazemos para automatizar esta tarefa? Neste caso, utilizamos loops.
# 
# **Loops** (ou, como alguns traduzem em português, "laços") são outras estruturas de controle de fluxo de um código. Estes são estruturas de repetição e nos permitem repetir uma porção do código. Quando nos referimos a cada repetição realizada, dizemos que é uma **iteração**. Iteração é o processo de mover-se por todos os elementos de um objeto em repetição. Portanto, se um certo código faz com que haja vinte repetições de algo, diz-se que houveram 20 iterações.
# 
# Objetos com os quais podemos realizar iterações são chamados de objetos **iteráveis**. Em Python, todas estruturas de dados são iteráveis: listas, tuplas, dicionários e sets. É possível também iterar sobre tipos de dados, como strings, e sequências de números.
# 
# Há dois tipos de loops: controlados por contagem ou por condição.

# ## Loops Controlados por Contagem

# Loops controlados por contagem (*count-controlled loops*) — também chamados de iterações definidas — diferenciam-se por sabermos o número de iterações a priori, ou seja, sabemos exatamente quantas repetições ocorrerão antes do início do loop.
# 
# Tais loops são controlados por uma variável de contagem (um *counter*) para definir quantas iterações serão feitas. Quando esta variável chegar a uma quantidade específica, o loop será terminado.
# 
# Este tipo de iteração é comumente utilizado com objetos com tamanho definido, como listas, e quando queremos iterar sobre um dado objeto um certo número de vezes. 
# 
# Geralmente há uma implementação específica deste tipo de loop chamada *for* e é esta que utilizaremos em Python, que veremos em no próximo capítulo.

# ## Loops Controlados por Condição

# Loops controlados por condição (*condition-controlled loops*) — também chamados de iterações indefinidas — são loops nos quaais não sabemos o número de iterações a priori, ou seja, não sabemos quantas repetições serão feitas. Pode ser somente uma como podem ser mais de cem mil. 
# 
# Assim como o próprio nome pressupõe, loops controlados por condição são, de fato, controlados por alguma condição especificada pelo programador. Enquanto esta condição for verdadeira, todos os elementos dentro do loop continuarão a serem executados continuamente, até o valor booleano da condição vir a ser falso. Um perigo deste tipo de condição é a criação de loops infinitos, quando a condição nunca poderá ser falsa, fazendo com que o loop nunca termine e o código não siga adiante.
# 
# Estes também podem ser chamados de *sentinel controlled loops*, pois geralmente possuem uma variável que controla a condição do loop. Esta variável é chamada de "valor sentinela" (*sentinel value*).
# 
# Este tipo de loop é comumente utilizado quando queremos nos certificar que um certo objeto ou atributo em específico seja alcançado.
# 
# Há diferentes tipos estruturas de loops controlados por condição nas diferentes linguagens de programação, como por exemplo:
# 
# - while
# - do-while
# - do-until
# 
# Em Python, temos somente `while`, estrutura a qual veremos em detalhes em outro capítulo desta seção.

# ## Range

# Antes de entrarmos na explicação de loops propriamente ditos, precisamos conhecer de um tipo de objeto específico de Python: `range`, principalmente para trabalhar com sequências de números inteiros.
# 
# Um objeto do tipo `range` é uma sequência imutável de números, utilizado em loops. Assim, quando queremos um modo fácil de se gerar uma sequência númerica para loops, utilizamos a função `range()`. Este objeto é comumente utilizado em `for` loops para iterar um número específico de vezes.
# 
# A função `range()` possui a sintaxe:

# In[3]:


range(start, stop, step)


# Onde os parâmetros são:
# 
# - `start`: o número inicial da sequência. Default: 0.
# - `stop`: o valor final da sequência, exclusivo.
# - `step`: o valor de incremento da sequência; o intervalo entre os números. Default: 1.
# 
# É bom lembrar:
# 
# - Como é de praxe em linguagens de programação geral, Python também começa sua contagem (seja de indexação, sequência, etc.) pelo número zero, como já vimos. Assim, se não oferecermos nenhum valor para o argumento `start`, as sequências sempre começarão em zero.
# - O argumento `stop` é exclusivo: seu valor final na verdade é $n - 1$, ou seja, o número definido não é incluso e o valor final de fato será um número menor do que o definido. Deste modo, o intervalo real da função `range()` é: $[start, stop)$.
# 
# Se executarmos esta função com um único número, estaremos definindo o valor do argumento 'stop'. Vejamos abaixo como criar uma sequência de 1 a 10:

# In[3]:


range(10)


# Executando esta função, não conseguimos exatamente o que queríamos. Isto por causa de dois motivos: a função `range()` retorna um objeto do tipo 'range'; e nossos argumentos estão errados.
# 
# Podemos verificar o tipo de objeto com:

# In[4]:


type(range(10))


# Na verdade, é um objeto do tipo `range`. É este o objeto que é utilizado nos loops e em outras funções que aceitam-no. Ele não retorna uma lista completa com todos os números, pois não é para o usuário, mas sim, para o computador utilizá-lo. Este objeto guarda somente três informações: os parâmetros `start`, `stop` e `step`. Não importa o quão grande é a sequência, um objeto `range` sempre guardará somente estas informações. A partir disto, a função `range()` calculará o próximo elemento da sequência. Esta forma de criação de sequência é conhecida como *lazy evaluation* — a expressão só é executada quando necessária, ocupando, então, pouca memória.
# 
# Como fazemos então para vizualizar esta sequência de números? Podemos coagir este objeto do tipo `range` para o tipo `list`, ou seja, transformar este objeto em uma lista. Fazemos isto com a função `list()`:

# In[5]:


list(range(10))


# Ou, analogamente, transformá-lo em uma tupla com 'tuple()'. Só lembrando que, neste caso, será uma sequência imutável, pois tuplas
# são estruturas de dados imutáveis.

# In[7]:


tuple(range(10))


# Até mesmo transformar em um conjunto, mas este caso é muito específico, pois `range()` não gera números repetidos e isto só será viável quando for do interesse do usuário trabalhar com conjuntos.

# In[8]:


set(range(10))


# Há três formas de se executar a função `range()`:
# 
# 1. `range(stop)`: se damos como argumento apenas um número.
# 2. `range(start, stop)`: se damos dois números como argumento.
# 1. `range(start, stop, step)`: se utilizamos os três argumentos.
# 
# Acima, utilizamos apenas a primeira forma, dizendo apenas em qual número parar a sequência. Ademais, também pudemos observar que `stop` é exclusivo, por isso quando dissemos `range(10)`, a sequência foi somente até o número nove.
# 
# Contudo, nossa sequência ainda está errada. Queremos uma sequência como nosso exemplo inicial: de 1 a 10. Então, forneceremos o argumento `start` para iniciarmos a sequência exatamente onde desejamos:

# In[9]:


list(range(1, 11))


# Fornecemos acima o número `1` para o argumento `start`, assim iniciamos a sequência no número `1`. Também fornecemos o valor `11` para o argumento `stop` para nossa sequência terminar em `10`. Agora nossa sequência está correta, utilizando um código automático e fácil de se modificar.
# 
# Como não fornecemos nenhum argumento para `step`, a função gera o incremento default de 1 em 1 número. Podemos mudar isto também. Vamos gerar uma sequência de 2 em 2 valores, apenas de números pares:

# In[10]:


list(range(0, 21, 2))


# Acima, com um incremento de 2 números, criamos uma sequência de números pares de zero a 20. Criemos agora uma sequência no mesmo intervalo, mas agora de ímpares:

# In[11]:


list(range(1, 21, 2))


# Para termos os números ímpares com um incremento de 2, mudamos somente o valor inicial da sequência para `1`.
# 
# Com esta função, também podemos gerar sequências decrescentes e negativas, alterando principalmente o argumento `step`:

# In[12]:


list(range(20, 0, -1))


# Acima, iniciamos a sequência no número `20`, terminamos em `1` e demos o valor de `-1` para o argumento `step`. Com este argumento negativo, Python entende que queremos gerar uma sequência decrescente. Se tentássemos esta mesma sequência sem o argumento de incremento negativo, nos seria retornado apenas uma lista vazia, pois Python entende que queremos criar uma sequência crescente e não sabe como iniciar em `20` e ir até `1` nesta ordem. Compreensível, pois não há como.

# In[14]:


# Lista vazia:
list(range(20, 0))


# In[15]:


# Lista vazia:
list(range(20, 1))


# Da mesma forma, podemos criar uma sequência com variados incrementos negativos, ou seja, ordem decrescente:

# In[20]:


# Números pares de dois em dois, ordem decrescente:
list(range(20, -1, -2))


# In[21]:


# Números ímpares de dois em dois, ordem decrescente:
list(range(19, 0, -2))


# E sequências específicas:

# In[23]:


# 20 a -20, de cinco em cinco:
list(range(20, -21, -5))


# In[25]:


# -10 a -20:
list(range(-10, -21, -1))


# Uma outra característica importante de `range()`: esta função trabalha apenas com números inteiros. Veja o exemplo abaixo, no qual tentamos criar uma sequência no intervalo $\left[2.4, 3.6 \right]$ com incremento de $0.2$:

# In[26]:


range(2.4, 3.7, .2)


# Recebemos um erro do tipo de elemento utilizado (*TypeError*): neste caso, um número decimal não pode ser interpretado como um número inteiro. A função `range()` não trabalha com decimais.
# 
# Se desejar uma sequência de floats, deverá usar de outros meios, como por exemplo, o módulo **NumPy**, que veremos mais à frente.
# 
# Por fim, como qualquer outro objeto em Python, podemos guardar um objeto `range` em uma variável e utilizá-la:

# In[29]:


# Criando e designando à uma variável:

var = range(20)

print(var)


# In[30]:


# Transformando a variável em uma lista:

list(var)

