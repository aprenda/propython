# Instalação

Neste capítulo, é demonstrado como se instalar Python nos sistemas operacionais [](insta-wind.md), [](insta-mac.md). Para sistemas Linux/UNIX, em [](insta-unix.md) há as instruções gerais de onde obter os arquivos *tarball*.
