# Anotações

## Arrumar

- Emoji na seção de IDE's (JupyterLab)
- Em `dados-str`, arrumar o link para docstrings

## Colocar(?)

- Ambiente virtual (venv)
- Anaconda/Conda na seção de IDE's
- Docstrings em funções
- Walrus Operator
    - [Link 1](https://deepsource.io/blog/python-walrus-operator/)
    - [Link 2](https://realpython.com/python-walrus-operator)    
- Iteradores (Iterator)
- Try/Except
    - [Link](https://note.nkmk.me/en/python-try-except-else-finally/)

### Input

Receber dados de entrada para um programa ("input") de um usuário é fundamental. Isto faz com que o código seja dinâmico e evita o famigerado "hand coding", que consiste de código estabelecido pelo programador em vez de se ter usado lógica para tal.

Na versão "legacy" de Python (Python 2), usávamos a função 'raw_input()' para recebermos input do usuário. Em Python 3, por motivos de legibilidade e facilidade de compreensão pelos iniciantes, esta função foi renomeada para apenas 'input()'.

A função 'input()' suspende o fluxo do programa temporariamente, ou seja, a execução do programa é paralisada na linha do 'input()' até que o usuário entre com qualquer valor.

Sua sintaxe é: input(prompt)

O argumento 'prompt' é uma string, a qual, se presente, será enviada como output (aparecerá na tela). Assim, podemos escerver o que de fato desejamos que o usuário coloque. Se não estiver presente, nada será colocado na tela.

Somente ler por ler a entrada do usuário pouco adianta. Para que haja algum efeito no programa, devemos guardar a entrada em uma variável, simplesmente designando 'input', como qualquer outra variável normal.

Seguem alguns exemplos de input:

Input de preço por unidade, guardado na variável 'preco_unidade'
preco_unidade = input("Digite o preço por unidade:")

Input de nome, guardado na variável 'nome' nome = input("Digite seu nome:")

Input de cidadania, guardado na variável 'cidadania': cidadania = input()

Este último exemplo (linha 1037) é um exemplo de um código ruim. O usuário não saberá o que deve ser digitado, a não ser que o programador tenha especificado anteriormente de alguma forma.

O exemplo da linha 1031 também contém um erro: naõ poderemos fazer nenhum cálculo com este valor, pois todos valores entrados por 'input()' são convertidos para string, independente de seu tipo. Exemplo:

valor = input("Digite qualquer número: ")

Tentemos agora fazer um cálculo: print(valor + 4)

O erro acontece pois Python não sabe o que fazer quando o operador '+' recebe dois operandos de diferentes tipos. No nosso caso, o operando esquerdo ('valor') é uma string e o operando direito ('4') é um int.

Verificando o tipo de 'valor': uma string print(type(valor))

O que fazemos neste caso é converter diretamente para o tipo de dado desejado.

Convertendo o input para float: valor = float(input("Digite qualquer número: "))

print(type(valor))

Convertendo o input para int: valor = int(input("Digite qualquer número: "))

print(type(valor))

Uma última dica, quando usando 'input()' é melhorarmos a legibilidade do argumento 'prompt': assim, fazemos com que ela seja facilmente reconhecível e aparazível ao usuário.

Fazemos isto da mesma forma que modificamos uma f-string: 'input()' aceita caracteres de escape normalmente. Particularmente, eu gosto da seguinte maneira:

valor = int(input("\nDigite qualquer número:\n\> "))

### For (exercício):
 
O que está aocntecendo?

a = [x for x in range(1000)]

total = 0

for x in a[::-1]:

    b = np.random.rand()

    quantos += 1
    
    if b < d:
        # print(f"b: {b}, d: {d}")
        total += 1
        a.remove(x)
    else:
        print(f"b: {b}, d: {d}")

### Tuple Looping

We can use a for loop to iterate over a tuple just like a list:

names=("Colt","Blue","Rusty","Lassy")

for k in range(3,-1,-1): # Decreasing
    print(names[k])

Ou:

for k in names: # Increasing
    print(k)


Ou com 'while':

months=('January',
'February',
'March',
'April',
'May',
'June',
'July',
'August',
'September',
'October',
'November',
'December')

i=len(months)-1

while i>=0:
    print(months[i])
    i-=1

Ou:

i=0

while i<12:
    print(months[i])
    i+=1

## Retirado

- Retirei a seção `Importância / Por que Usar` do capítulo 1.
