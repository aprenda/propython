 # O Que São Estruturas de Dados?
 
Este é o primeiro grande tópico do nosso aprendizado de Python: as estruturas de dados. Primeiramente, precisamos saber o que são tais estruturas.

Estruturas de dados referem-se a determinados **modos pré-definidos de como o computador guarda e organiza dados na memória**. Dada uma certa estrutura de dados, o computador pode, de forma mais eficaz, organizar e processar algum conjunto de dados.

Uma decisão crítica ao se criar qualquer programa, qualquer que seja a linguagem, é aquela de se utilizar a estrutura de dados apropriada (e como utilizá-la) em vista do tipo de dado sendo manipulado e do problema em questão.

Python possui várias estruturas de dados nativas:

- Listas
- Tuplas
- Dicionários
- Conjuntos

Há duas grandes diferenças básicas nas estruturas de dados em Python: *ordenação* e *mutabilidade*.

## Ordenação

Um objeto é dito **ordenado** se os elementos contidos nele possuem ordem, isto é, os elementos dentro do objeto estão posicionados ordenadamente. Exemplificamos: na imagem abaixo, o objeto em questão contém seis valores booleanos. Cada um destes elementos possui uma posição no objeto, indicado pelo número acima dele.

```{figure} /_static/img/estru-dados1.png
---
align: center
alt : Dados ordenados
name: Fig. 4
---
Um objeto ordenado.
```

```{admonition} Posição Zero
Perceba que na imagem acima iniciamos a contagem das posições pelo número zero. Isto não é um erro! Na maioria da linguagens de programação, Python incluso, as posições iniciam-se pelo zero e não pelo número um. Assim, sempre que falarmos da primeira posição (ou primeiro elemento) de um objeto, estamos nos referindo à posição zero (ou ao elemento na posição zero).
```

Em estruturas ordenadas, podemos acessar os elementos em cada posição, assim como mudar as posições dos elementos, uma vez que o conceito de ordenação existe nestes objetos. Na imagem abaixo, mostramos como seria uma reorganização dos elementos de nosso objeto hipotético: colocamos os valores de `True` nas três primeiras posições, enquanto as trẽs últimas posições contêm o valor `False`.

```{figure} /_static/img/estru-dados2.png
---
align: center
alt : Dados ordenados reorganizados.
name: Fig. 5
---
Um objeto ordenado com seus elementos reorganizados.
```

Contrariamente, se os elementos de um objeto não puderem ser acessados pela sua posição ou reorganizados, este objeto **não é ordenádo**. Em Python, listas e tuplas são ordenadas, enquanto dicionários e conjuntos não são ordenados.

## Mutabilidade

Suponha que criamos um objeto como no último exemplo e o guardemos na variável `valores_bool` (imagem abaixo). Após criarmos este objeto, mudamos o elemento na posição 2 de `True` para `3.14` — um valor numérico. Fazemos o mesmo para o elemento na posição 4: inicialmente, nesta posição, havia o valor booleano `False` e o mudamos para o número `2.71`.

```{figure} /_static/img/estru-dados3.png
---
align: center
alt : Dados mutáveis.
name: Fig. 6
---
Um exemplo de objeto mutável.
```

Se um objeto permite que seus elementos sejam modificados após sua criação, ele é dito **mutável**. Em Python, listas, dicionários e conjuntos são mutáveis.

Também há os objetos que não permitem nenhuma modificação de seus elementos após sua criação. Tais objetos são ditos **imutáveis**. Em Python, as tuplas são imutáveis: se quisermos modificarmos algo em uma tupla, precisaríamos criar uma nova tupla.

Veremos todos estes detalhes mais à frente, não se preocupe. Basta por hora saber que existem:

- Objetos que podem ser ordenados (e outros que não possuem ordem);
- Objetos que podem ser modificados (e outros que são imutáveis).
