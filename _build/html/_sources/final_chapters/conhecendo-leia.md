# Leia Mais

- Python por Guido van Rossum:
    - [Linha do Tempo](http://python-history.blogspot.com/2009/01/brief-timeline-of-python.html)
    - [Filosofia do Python](http://python-history.blogspot.com/2009/01/pythons-design-philosophy.html)
    - [História - Parte 1](http://python-history.blogspot.com/2009/01/personal-history-part-1-cwi.html)
    - [História - Parte 2](http://python-history.blogspot.com/2009/01/personal-history-part-2-cnri-and-beyond.html)
    - [Design Inicial](http://python-history.blogspot.com/2009/02/early-language-design-and-development.html)
    - [Entrevista](https://www.artima.com/articles/the-making-of-python)
- Informações do site oficial:
    - [Perguntas Frequentes](https://docs.python.org/3/faq/general.html#id2)
