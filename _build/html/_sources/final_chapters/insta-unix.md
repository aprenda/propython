# Linux/UNIX

Geralmente, nestes sistemas, Python já está instalado. Você pode verificar se já possui Python com algum dos seguintes comandos:

```
which python
```

```
which python3
```

O retorno deve ser `/usr/bin/python` ou afim. Se você possuir, pode verificar a versão de seu Python com:

```
python --version
```

ou

```
python3 --version
```

De acordo com sua distribuição, você pode atualizar ou instalar seu Python com seu gerenciador de pacotes específico, como `apt-get` ou `yum`. Se sua distribuição utilizar outro gerenciador, verifique como instalar Python de acordo sua documentação oficial, como por exemplo, em [Arch Linux](https://wiki.archlinux.org/title/Python#Installation).

Você também pode instalar Python manualmente através dos binários, os quais podem ser encontrados no [site oficial](https://www.python.org/downloads/).
