# Leia Mais

## George Boole

- Site comemorativo do bicentenário do matemático, com diversas informações sobre suas obras:
    - [George Boole 200](https://www.georgeboole.com)

## Relação Entre Booleanos e Inteiros

- [PEP 285](https://www.python.org/dev/peps/pep-0285/)

- [Documentação Oficial](https://docs.python.org/3.10/library/stdtypes.html#numeric-types-int-float-complex)