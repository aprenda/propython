- caption: Estruturas de Dados
  chapters:
  - file: lista-intro
    sections:
      - file: lista-criando
      - file: lista-index
      - file: lista-slic
      - file: lista-anin
      - file: lista-metodo
      - file: lista-exerc
      - file: tup-intro
  chapters:
  - file: dic-intro
    sections:
      - file: dicionarios
  chapters:
  - file: conj-intro
    sections:
      - file: conjuntos
- caption: Estruturas Condicionais e de Repetição
  chapters:
  - file: condicoes
  - file: intro_repeticao
  - file: repeticao_while
  - file: repeticao_for
  - file: compreensoes
- caption: Colocar
  chapters:
  - file: colocar