# Tuplas

Nossa próxima estrutura de dados em Python são as tuplas. Uma tupla é uma estrutura muito similar às listas — na forma de se criar e na manipulação. Basicamente, suas diferenças são duas:

- Sintaxe: usamos os parênteses para criar uma tupla;
- Mutabilidade: tuplas são imutáveis.

Formalmente, tuplas são uma coleção (agrupamento) ordenada de objetos. Adicionando o fato de que são estruturas imutáveis, podemos reformular a definição para: **coleção (agrupamento) ordenada e imutável de objetos**.
