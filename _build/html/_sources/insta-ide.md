# IDE's

IDE é um acrônimo do inglês *Integrated Development Environment*, em português, **Ambiente de Desenvolvimento Integrado**. IDE's são softwares que auxiliam os progeamadres no desenvolvimento de seus trabalhos. Através das IDE's é possível escrever seu código; integrar diferentes partes do programa facilmente; testar o código; e fazer seu *debugging*, isto é, verificar erros e corrigi-los. Há dezenas de IDE's disponíveis para Python. Abaixo listamos algumas IDE's e também outras opções mais simples.

(visual-studio)=
## Visual Studio

Visual Studio é uma IDE profissional completa. Esta IDE possui versões pagas, mas a versão `Community` é grátis para download neste [link](https://visualstudio.microsoft.com/pt-br/downloads/). Neste [link](https://visualstudio.microsoft.com/pt-br/vs/features/python/) você pode ler sobre como utilizar Python nesta ferramenta.

## VS Code

Visual Studio Code (VS Code) é uma versão menor e grátis do [Visual Studio](visual-studio). Tecnicamente, não é uma IDE de fato, talvez uma mini-IDE, pois é um ambiente de edição de texto com maiores aplicabilidades, mas ainda há diversas extensões e funções possíveis neste ambiente. Você pode fazer o download no [site oficial](https://code.visualstudio.com) e ver como utilizar Python com VS Code neste [link](https://code.visualstudio.com/docs/languages/python).

## PyCharm

PyCharm é uma IDE profissional da empresa JetBrains que é específica para Python. Há as versões paga e comunidade (grátis), as quais podem ser encontradas no [site oficial](https://www.jetbrains.com/pycharm/). Um tutorial desta IDE pode ser lido neste [link](https://www.jetbrains.com/help/pycharm/quick-start-guide.html).

## JupyterLab

O projeto Jupyter possui dois softwares para desenvolvimento de códigos: *Jupyter Notebook* e *JupyterLab*. Notebooks são cadernos interativos nos quais colocamos nosso código e podemos intercalá-lo com texto em markdown e LaTeX. Assim, são ferramentas excepcionais para criação e apresentação de documentos interativos. Aqui nos referimos apenas ao JupyterLab, que é uma atualização do Notebook. Detalhe: este livro também foi escrito em JupyterLab :smile:. Um porém é, cadernos não são feitos para softwares de fato, mas sim, escrita de programas menores, pois no fim são documentos interativos. Então, se seu objetivo não é somente algum tipo de análise ou afim, talvez uma melhor opção seja alguma outra anterior. Você pode instalá-lo através do [site oficial](https://jupyter.org) e encontrar como utilizá-lo na [documentação oficial](https://jupyterlab.readthedocs.io/en/stable/index.html) ou em tutoriais na internet.

## Google Colaboratory

Talvez a ferramenta mais simples, o Google Colaboratory (Colab), não precisa ao menos ser instalado, pois é uma ferramente totalmente na nuvem. Os únicos pré-requisitos são um navegador e uma conta da Google. Assim como Jupyter, Colab utiliza cadernos interativos, os quais ficam armazenados e ligados à sua conta. Você pode acessá-lo neste [link](https://colab.research.google.com), onde também encontrará como utilizá-lo. Também como o colab, é melhor indicado somente para o desenvolvimento de análises.
