# Variáveis

Variáveis possuem um nome (às vezes chamados de identificador, como em C++) e um valor. Basicamente, variáveis são nomes para locais na memória que possuem um valor.

Variáveis necessitam ser designadas antes de serem utilizadas. Designação de variáveis também é conhecida como **ligação** (*binding*). Quando fazemos isto, estamos ligando um valor na memória a um determinado nome.

Ligação feita, a designação da variável está completa e podemos nos referir a ela por seu nome. Ao fazer isto, o computador saberá aonde olhar pela informação na memória, através do nome da variável.