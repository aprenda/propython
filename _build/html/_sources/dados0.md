# Tipos de Dados

Antes de aprendermos como criar variáveis, devemos conhecer quais tipos de dados são suportados em Python. Um tipo de dado especifica que tipo de valor a variável em questão possuirá, como, por exemplo: números inteiros, texto ou booleanos. Em outras palavras, os tipos de dados são as categorias de dados que Python reconhece e sabe como guardar/manipular.

Veremos aqui os tipos de dados fundamentais em Python — os chamados *built-in types* (pois são os tipos própios de Python) — e veremos as estruturas de dados implementadas em Python em um próximo capítulo.