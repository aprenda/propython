# Principais Características

## Alto Nível

Python é uma linguagem de alto nível. O que isto quer dizer, exatamente? Basicamente, é o quão distante estamos da linguagem da própria máquina; do hardware. Ao falarmos de níveis, nos referimos ao nível de *abstração* da linguagem. Se programamos em uma língua de menor nível (como a linguagem C, por exemplo), temos de nos ater a diversos detalhes e minúcias da arquitetura do computador, pois estamos, de uma certa forma, lidando diretamente com ela. Por outro lado, isto significa um controle muito maior sobre a máquina. Ao programarmos em uma língua de nível maior, não precisamos sequer conhecer tais minúcias e especificações do hardware, pois a própria linguagem lida e toma decisões internamente, ou seja, estas decisões são *abstraídas* do programador. Grosseiramente, em linguagens de alto nível pensamos na *ideia* do que queremos fazer e como fazê-lo sem entrar em detalher; em linguagens de baixo nível, pensamos nos *detalhes* da execução e como o computador irá lidar com nosso programa. Assim, características de linguagens de alto nível, são, geralmente:

1. Comandos mais explícitos e mais fáceis;
1. Maior legibilidade do código;
1. Menos código necessário para completar-se ações;
1. Consequentemente, menores scripts escritos.

Pense na seguinte analogia: queremos fazer uma torrada. Em uma linguagem de alto nível, nossa sequência de comandos seria algo como:

1. Pegue um pão;
1. Insira-o na torradeira;
1. Retire-o quando pronto.

Em uma linguagem de menor nível, teríamos muito mais com o que nos preocuparmos:

1. Pegue um pão;
1. Verifique se o tipo de pão é adequado para a torradeira;
1. Vá até a torradeira;
1. Coloque o pão na posição vertical;
1. Insira-o na torradeira;
1. Verifique qual a voltagem da casa;
1. Verifique se a voltagem da torradeira é compatível com a da casa;
1. Ligue a torradeira na tomada;
1. Aguarde 4 minutos e 37 segundos;
1. Retire a torradeira da tomada;
1. Retire o pão da torradeira.

Um pouco mais trabalhoso, não é?

## Multi-paradigmas

Há os chamados [paradigmas da programação](https://en.wikipedia.org/wiki/Programming_paradigm), os quais definem diferentes modos, ou estilos, de programação. Abaixo, resumimos três paradigmas que nos são mais importantes.

- **Procedural**: A programação procedural baseia em uma série de comandos na ordem do script (de cima para baixo), instruindo a máquina passo-a-passo a realizar uma ação.
- **Funcional**: O principal elemento da programação funcional é são as funções — de onde vem o nome do paradigma. Aqui, focamos em construir funções para fins específicos: cada uma fará, idealmente, apenas uma ação de forma otimizada. Através de vários retornos destas funções, teremos o resultado final do programa.
- **Orientado ao objeto**: Neste paradigma temos *objetos* — entidades criadas a partir de *classes* e embuídas de ações (funções) e características (atributos). No desenvolvimento do programa, um conjunto de objetos realiza as operações necessárias para obtermos o resultado final. Aprenderemos mais sobre este tipo de paradigma mais à frente no livro.

Diversas linguagens são feitas propriamente com um tipo único de paradigma, por exemplo, C, Pascal e BASIC utilizam o paradigma procedural como abordagem padrão. Outras linguagens, como C++ e Python, são ditas **multi-paradigmas**, pois permitem que sejam utilizados diversos paradigmas. Deste modo, se quisermos criar classes e objetos para um software, nós podemos. Da mesma forma, se preferirmos resolver algo somente com funções ou códigos diretos, também podemos. Python geralmente é disseminado como uma grande linguagem orientada ao objeto, o que não está errado, mas o correto de fato seria dizer que Python é uma linguagem multi-paradigma.

## Interpretada

Python é uma linguagem interpretada, o que, em poucas palavras, quer dizer que Python executa o código-fonte (o código que o programador escreveu) linha por linha e busca por erros de sintaxe no momento da execução. O código das linguagens de programação podem ser *compilados* ou *interpretados*, referindo-se a como as linguagens de programação traduzem o código escrito em sua própria sintaxe ao código da máquina. Resumidamente, linguagens compiladas são aquelas nas quais um programa chamado **compilador** lê o código-fonte, verifica erros e faz otimizações, para então, produzir um arquivo executável do seu programa. Ao termos este executável, não precisamos compilar o código novamente, a não ser que este mude. Linguagens interpretadas possuem um programa chamado **interpretador**, o qual simultaneamente executa e verifica cada linha do código sem necessitar de ser compilado. Neste tipo de abordagem, o código sempre terá de ser interpretado cada vez que executarmos o programa. Se quiser entender um pouco mais a fundo sobre o tema, esta resposta do [Stack Overflow](https://stackoverflow.com/a/38491646/11676927) é um bom começo.
