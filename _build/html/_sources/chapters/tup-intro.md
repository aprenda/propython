# Tuplas

```{figure} /_static/img/parenthesis.png
---
width: 10em
align: center
alt : Parenthesis
name:
---

```

Nossa próxima estrutura de dados em Python são as tuplas. Formalmente, tuplas são uma coleção (agrupamento) ordenada de objetos. Adicionando o fato de que são estruturas imutáveis, podemos reformular a definição:

```{admonition} Definição
Tuplas são uma coleção (agrupamento) ordenada e imutável de objetos.
```

Uma tupla é uma estrutura muito similar à lista — na forma de se criar e na manipulação. Basicamente, suas diferenças são:

- Sintaxe: usamos os parênteses para criar uma tupla;
- Mutabilidade: tuplas são imutáveis.

Tuplas são utilizadas para:

1. Agrupar dados heterogêneos (como em funções que retornam diferentes dados);
1. Agrupar dados homogêneos em um objeto imutável.

Algumas vantagens tuplas são:

- Dependendo da operação, sua performance pode ser melhor do que listas;
- Como são imutáveis, tornam o código um pouco mais seguro contra bugs;
- Como são imutáveis, alocam um menor espaço na memória do que listas;
- Vários métodos e funções retornam tuplas para o usuário.

Neste capítulo, estudaremos em detalhes sobre esta estrutura de dados.
