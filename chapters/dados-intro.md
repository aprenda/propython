# Tipos de Dados

Nosso próximo passo é conhecer os elementos mais finos de Python: os tipos de dados. Conheceremos quais tipos de dados estão disponíveis em Python e um pouco sobre funções que utilizamos para manipular tais tipos. Mas antes, vejamos uma definição de tipos de dados.

Em programação, os tipos de dados definem:

- Como uma variável será armazenada na memória;
- Em qual categoria de dados daquela linguagem o valor armazenado na variável se enquadra.

Um tipo de dado especifica que tipo de valor a variável em questão possuirá, como, por exemplo: números inteiros, texto ou booleanos. Em outras palavras, os tipos de dados são as categorias de dados que Python reconhece e sabe como guardar/manipular.

Veremos aqui os tipos de dados fundamentais em Python — os chamados *built-in types* (pois são os tipos própios de Python) — e veremos as estruturas de dados implementadas em Python em um próximo capítulo.
