# Listas

```{figure} /_static/img/squarebraq.png
---
width: 10em
align: center
alt : Square Braquets
name:
---

```

A primeira das estruturas de dados que estudaremos chama-se `lista` (*list*).

```{admonition} Definição
Listas são estruturas que agrupam um conjunto de elementos homogêneos em um único objeto ordenado.
```

Esta definição utiliza o conceito de homogeneidade  de elementos. Expliquemos: com *elementos homogêneos*, quer-se dizer que os elementos dentro de uma lista são do mesmo tipo (ou, pelo menos, muito similares): ou são booleanos; ou são numéricos; ou strings... Contudo, como veremos, é possível sim criar listas com elementos de tipos diferentes. A homogeneidade dos tipos de dados em uma lista, mesmo que não obrigatória, é uma forma de boa prática e organização de seus dados. Veremos exemplos nas próximas seções.

**Listas são estruturas ordenadas e mutáveis.** Seus elementos possuem posições específicas dentro da lista, podendo então, serem acessados através de tais posições. Como também são mutáveis, após criadas, podemos alterar seus elementos.

Concluindo: listas são estruturas (ordenados e mutáveis) que agrupam diversos elementos diferentes um só objeto.
