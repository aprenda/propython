# Conhecendo Python

Sempre que aprendemos algo novo, é interessante saber quais foram suas motivações; as ideias das quais foram originadas ou quais problemas tentam resolver. Com linguagens de programação não é diferente. Neste capítulo, veremos como e por que Python foi criado, assim como suas características principais.
