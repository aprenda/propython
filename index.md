# Sobre o Site

**Projeto em construção!**

Este é um site para que qualquer um possa aprender a programar em Python, sem experiência alguma em linguagens de programação. A partir do zero, aprenderemos os básicos desta linguagem e avançaremos na complexidade de programação em Python.

O intuito é que, ao seguir e terminar o conteúdo deste site, você tenha aprendido grande parte da linguagem e se sinta confortável para realizar projetos independentes e aprender  tópicos avançados sozinho. Em um primeiro momento, este projeto é somente sobre a linguagem Python em si, não incluindo módulos como Matplotlib, NumPy, Pandas, etc.

## Autor

|Autor|Sobre|
| :-: | :-: |
| ![](_static/img/nathan.jpeg) <br> **Zander Augusto**| Bacharel em ciências biológicas; mestre em biodiversidade animal; e doutor em Ecologia e Evolução. <br><br>[ORCid](https://orcid.org/0000-0002-4949-1096)

## Contato

Contacte-me através do e-mail: <zanderaugusto@gmail.com>

## Agradecimentos

JVBC
